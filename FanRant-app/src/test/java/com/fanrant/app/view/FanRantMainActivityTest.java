package com.fanrant.app.view;

import android.os.Bundle;
import com.fanrant.R;
import com.fanrant.app.events.GuestModeEvent;
import com.fanrant.app.model.FanRantUserModel;
import com.fanrant.testutils.FanRantTestRunner;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.shadows.ShadowTabHost;
import roboguice.RoboGuice;
import roboguice.activity.RoboActivity;

import static org.robolectric.Robolectric.shadowOf;

/**
 * Created with IntelliJ IDEA.
 * User: ant
 * Date: 13/04/13
 * Time: 11:12
 * To change this template use File | Settings | File Templates.
 */
@RunWith(FanRantTestRunner.class)
public class FanRantMainActivityTest {

    private FanRantMainActivity activity;
    private ShadowTabHost tabHost;

    @Before
    public void setUp() throws Exception {
        activity = RoboGuice.getInjector(new RoboActivity()).getInstance(FanRantMainActivity.class);
        tabHost = shadowOf(activity.mTabHost);


    }
    @Test 
    public void testLoadAsGuest() throws Exception {
        FanRantUserModel userModel = new FanRantUserModel(null);

        activity.eventManager.fire(new GuestModeEvent(userModel));


        Assert.assertEquals(tabHost.getTabWidget().getTabCount(),5);

        
    }
    @Test
    public void testInit() throws Exception {
        Bundle b = new Bundle();
        b.putString("tab","Tab3");
        activity.init(b);
        Assert.assertEquals(tabHost.getCurrentView().getId(), R.layout.tvschedule);
    }

    @Test
    public void testFixturesTabHit() throws Exception {
        tabHost.setCurrentTab(2);
        Assert.assertEquals(tabHost.getCurrentView().getId(), R.layout.fixtures);
    }


    @Test
    public void testTvScheduleTabHit() throws Exception {
        tabHost.setCurrentTab(3);
        Assert.assertEquals(tabHost.getCurrentView().getId(), R.layout.tvschedule);

    }

    @Test
    public void testLiveScoresTabHit() throws Exception {
        tabHost.setCurrentTab(4);
        Assert.assertEquals(tabHost.getCurrentView().getId(), R.layout.livescores);
    }

    @Test
    public void testProfileTabHit() throws Exception {
        tabHost.setCurrentTab(5);
        Assert.assertEquals(tabHost.getCurrentView().getId(), R.layout.profile);
    }

    public void testLaunchChooser() throws Exception {

    }

}
                                                                                       