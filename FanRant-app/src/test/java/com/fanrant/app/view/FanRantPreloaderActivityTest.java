package com.fanrant.app.view;

import android.content.Context;
import android.content.Intent;
import com.fanrant.app.mock.FanRantTestPreferences;
import com.fanrant.app.model.IFanRantUserModel;
import com.fanrant.app.modules.IPreferences;
import com.fanrant.app.modules.PreferenceConstants;
import com.fanrant.testutils.FanRantTestRunner;
import com.google.inject.Inject;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.shadows.ShadowActivity;
import roboguice.event.EventManager;

import java.util.HashMap;

import static junit.framework.Assert.assertEquals;

import static junit.framework.Assert.assertSame;
import static junit.framework.Assert.assertTrue;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.robolectric.Robolectric.shadowOf;


/**
 * Created with IntelliJ IDEA.
 * User: Antwork
 * Date: 23/03/2013
 * Time: 13:01
 * To change this template use File | Settings | File Templates.
 */
@RunWith(FanRantTestRunner.class)
public class FanRantPreloaderActivityTest {

    private static final int ARSENAL = 3;
    FanRantPreLoaderActivity fanRantPreLoader;

    @Inject
    IPreferences preferences;
    @Inject
    Context context;

    @Inject
    EventManager eventManager;

    @Before
    public void setUp() throws Exception {
        fanRantPreLoader = new FanRantPreLoaderActivity();
        Robolectric.bindShadowClass(FanRantTestPreferences.class);
        Robolectric.runUiThreadTasksIncludingDelayedTasks();
        Robolectric.resetStaticState();
    }


    @Test
    public void testloggedInSharedPref() throws Exception {
        HashMap<String, String> userPref = new HashMap<String, String>();
        userPref.put(PreferenceConstants.USERNAME, "ant2");
        userPref.put(PreferenceConstants.PASSWORD, "000000");
        preferences.addPref(userPref);

        fanRantPreLoader.onStart();
        ShadowActivity shadowActivity = shadowOf(fanRantPreLoader);

        ShadowActivity.IntentForResult startedIntent = shadowActivity.getNextStartedActivityForResult();
        Intent dummyIntent = new Intent();
        dummyIntent.setClassName("com.fanrant", FanRantMainActivity.class.getName());

        assertThat(startedIntent.intent, equalTo(dummyIntent));

        IFanRantUserModel model = fanRantPreLoader.userModel;

        preferences.clear();//reset prefs
    }

    @Test
    public void testGuestMode() throws Exception {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(PreferenceConstants.UNREGUSER, "true");
        map.put(PreferenceConstants.UNREGTEAMID, String.valueOf(ARSENAL));
        map.put(PreferenceConstants.UID, String.valueOf(System.currentTimeMillis()));
        preferences.addPref(map);

        fanRantPreLoader.onStart();
        ShadowActivity shadowActivity = shadowOf(fanRantPreLoader);

        ShadowActivity.IntentForResult startedIntent = shadowActivity.getNextStartedActivityForResult();
        Intent dummyIntent = new Intent();
        dummyIntent.setClassName("com.fanrant", FanRantMainActivity.class.getName());

        assertThat(startedIntent.intent, equalTo(dummyIntent));

        Assert.assertSame(Integer.parseInt(preferences.getPref(PreferenceConstants.UNREGTEAMID)), ARSENAL);
        assertTrue(Boolean.parseBoolean(preferences.getPref(PreferenceConstants.UNREGUSER)));

        preferences.clear(); //reset prefs
    }

    @Test
    public void testWelcomeScreen() throws Exception {
        fanRantPreLoader.onStart();
        ShadowActivity shadowActivity = shadowOf(fanRantPreLoader);

        ShadowActivity.IntentForResult startedIntent = shadowActivity.getNextStartedActivityForResult();
        Intent dummyIntent = new Intent();
        dummyIntent.setClassName("com.fanrant", FanRantWelcomeActivity.class.getName());

        assertThat(startedIntent.intent, equalTo(dummyIntent));

    }

    @After
    public void tearDown() throws Exception {
        fanRantPreLoader = null;

    }
}
