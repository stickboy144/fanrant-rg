package com.fanrant.app.view;

import android.content.Intent;
import android.widget.Button;
import com.fanrant.R;
import com.fanrant.app.events.TeamSelectedEvent;
import com.fanrant.testutils.FanRantTestRunner;
import com.google.inject.Inject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.shadows.ShadowActivity;
import roboguice.RoboGuice;
import roboguice.activity.RoboActivity;
import roboguice.event.EventManager;

import static junit.framework.Assert.assertNotNull;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.robolectric.Robolectric.shadowOf;

/**
 * Created with IntelliJ IDEA.
 * User: Ant Grimmitt
 * Date: 29/03/2013
 * Time: 15:41
 */
@RunWith(FanRantTestRunner.class)
public class FanRantWelcomeActivityTest {

    FanRantWelcomeActivity activity;


    @Inject
    EventManager eventManager;

    @Before
    public void setUp() throws Exception {
        activity = RoboGuice.getInjector(new RoboActivity()).getInstance(FanRantWelcomeActivity.class);

    }

    @Test
    public void shouldShowDialog() throws Exception {
        activity.onCreate(null);
        Button selectTeam = (Button) activity.findViewById(R.id.pickteam);
        selectTeam.performClick();


    }

    @Test
    public void testInitGuestModeTeamId() throws Exception {
        activity.eventManager.fire(new TeamSelectedEvent("7", "8", "Arsenal"));
        ShadowActivity shadowActivity = shadowOf(activity);
        ShadowActivity.IntentForResult startedIntent = shadowActivity.getNextStartedActivityForResult();
        Intent dummyIntent = new Intent();
        dummyIntent.setClassName("com.fanrant", FanRantMainActivity.class.getName());
        dummyIntent.putExtra("guest",true);

        assertThat(startedIntent.intent, equalTo(dummyIntent));


    }

    @Test
    public void testLogin() throws Exception {
        activity.onCreate(null);
//        selectTeam.performClick();
        Button login = (Button) activity.findViewById(R.id.login);

        login.performClick();

        ShadowActivity shadowActivity = shadowOf(activity);
        ShadowActivity.IntentForResult startedIntent = shadowActivity.getNextStartedActivityForResult();
        Intent dummyIntent = new Intent();
        dummyIntent.setClassName("com.fanrant", FanRantLoginActivity.class.getName());

        assertThat(startedIntent.intent, equalTo(dummyIntent));


    }
}