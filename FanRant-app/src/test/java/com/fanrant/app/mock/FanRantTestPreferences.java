package com.fanrant.app.mock;

import android.content.SharedPreferences;
import android.content.res.AssetManager;
import com.fanrant.app.Environments;
import com.fanrant.app.modules.FanRantPreferences;
import com.fanrant.app.modules.IPreferences;
import com.google.inject.Inject;
import org.robolectric.internal.Implements;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Properties;

/**
 * Created with IntelliJ IDEA.
 * User: Ant Grimmitt
 * Date: 24/03/2013
 * Time: 12:04
 */
@Implements(FanRantPreferences.class)
public class FanRantTestPreferences implements IPreferences {
    protected SharedPreferences sharedPreferences;
    private Environments ENVIRONMENT = Environments.TESTING;
    private String url;
    private final AssetManager mAssetManager;

    @Inject
    public FanRantTestPreferences(SharedPreferences sharedPreferences, AssetManager assetManager) {
        this.sharedPreferences = sharedPreferences;
        mAssetManager = assetManager;

        Properties properties = null;
        try {
            InputStream inputStream = mAssetManager.open("url.properties");
            properties = new Properties();
            properties.load(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (properties != null) {

            switch (getEnvironment()) {
                case DEFAULT:
                    setUrl(properties.getProperty("default.url"));
                    break;
                case TESTING:
                case DEV:
                    setUrl(properties.getProperty("dev.url"));
                    break;
                case ANTDEV:
                    setUrl(properties.getProperty("ant.dev.url"));
            }

        }

    }


    @Override
    public String getPref(String key) {
        return sharedPreferences.getString(key,null);  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public boolean isPref(String key) {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void addPref(HashMap<String, String> map) {
        for (String pref : map.keySet()) {
            sharedPreferences.edit().putString(pref, map.get(pref)).commit();
        }
    }

    @Override
    public void setEnvironment(Environments environment) {
        ENVIRONMENT = Environments.TESTING;
    }

    @Override
    public Environments getEnvironment() {
        return ENVIRONMENT;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public String getUrl() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void clear() {
        sharedPreferences.edit().clear().commit();
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
