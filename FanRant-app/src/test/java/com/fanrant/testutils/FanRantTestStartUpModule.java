package com.fanrant.testutils;

import com.fanrant.app.modules.api.client.MockDataClient;
import com.fanrant.app.modules.api.client.IDataClient;
import com.fanrant.app.mock.FanRantTestPreferences;
import com.fanrant.app.modules.FanRantStartUpModule;
import com.fanrant.app.modules.IPreferences;
import com.google.inject.Binder;
import roboguice.inject.SharedPreferencesName;

/**
 * Created with IntelliJ IDEA.
 * User: Ant Grimmitt
 * Date: 23/03/2013
 * Time: 22:23
 */
public class FanRantTestStartUpModule extends FanRantStartUpModule {
    @Override
    public void configure(Binder binder) {


        binder.bind(IPreferences.class).to(FanRantTestPreferences.class);


        binder.bind(IDataClient.class).to(MockDataClient.class);

        binder.bindConstant().annotatedWith(SharedPreferencesName
                .class).to("com.fanrant");
    }
}
