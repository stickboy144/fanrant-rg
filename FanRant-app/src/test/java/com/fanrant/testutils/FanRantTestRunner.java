package com.fanrant.testutils;

import android.app.Application;
import android.os.Build;
import com.actionbarsherlock.ActionBarSherlock;
import com.actionbarsherlock.internal.ActionBarSherlockCompat;
import com.actionbarsherlock.internal.ActionBarSherlockNative;
import com.fanrant.app.mock.actionbarsherlock.ActionBarSherlockRobolectric;
import com.google.inject.Injector;
import com.google.inject.util.Modules;
import org.junit.runners.model.InitializationError;
import org.robolectric.AndroidManifest;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricContext;
import org.robolectric.RobolectricTestRunner;
import roboguice.RoboGuice;

import java.io.File;
import java.lang.reflect.Method;

/**
 * Created with IntelliJ IDEA.
 * User: Antwork
 * Date: 23/03/2013
 * Time: 13:49
 * To change this template use File | Settings | File Templates.
 */
public class FanRantTestRunner extends RobolectricTestRunner {

    public FanRantTestRunner(Class<?> testClass) throws InitializationError {
        super(RobolectricContext.bootstrap(FanRantTestRunner.class, testClass, new RobolectricContext.Factory() {
            @Override
            public RobolectricContext create() {
                return new RobolectricContext() {
                    @Override
                    protected AndroidManifest createAppManifest() {
                        return new AndroidManifest(new File("FanRant-App/"));
                }

                    @Override
                    public boolean useAsm() {
                        return false;
                    }
                };
            }
        }));
//       super(testClass);
        ActionBarSherlock.registerImplementation(ActionBarSherlockRobolectric.class);
        ActionBarSherlock.unregisterImplementation(ActionBarSherlockNative.class);
        ActionBarSherlock.unregisterImplementation(ActionBarSherlockCompat.class);

    }
    @Override
    public void prepareTest(Object test) {

        Application app = Robolectric.application;
        RoboGuice.setBaseApplicationInjector(app, RoboGuice.DEFAULT_STAGE,
                Modules.override(RoboGuice.newDefaultRoboModule(app)).with(new FanRantTestStartUpModule()));

        Injector injector = RoboGuice.getInjector(app);
        injector.injectMembers(test);
    }

//    @Override
//    public void beforeTest(Method method) {
//        resetStaticState();
//        super.beforeTest(method);
//    }
}
