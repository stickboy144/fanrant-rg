package com.fanrant.app.modules.api.responders;

import com.fanrant.app.mod.FanRantGameLatestUpdateModel;
import com.fanrant.app.model.FanRantGameUpdateModel;
import com.fanrant.app.model.FanRantModel;
import com.fanrant.app.modules.api.FanRantAPIResponse;

/**
 * Created with IntelliJ IDEA.
 * User: anthonygrimmitt
 * Date: 21/04/2013
 * Time: 21:00
 * To change this template use File | Settings | File Templates.
 */
public class    FanRantGameUpdateResponse extends FanRantAPIResponse {
    private FanRantGameLatestUpdateModel[] results;
    @Override
    public FanRantGameLatestUpdateModel[] getResults() {
        return results;  //To change body of implemented methods use File | Settings | File Templates.
    }
}
