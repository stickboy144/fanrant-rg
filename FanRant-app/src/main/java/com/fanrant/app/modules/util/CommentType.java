package com.fanrant.app.modules.util;

/**
 * Created with IntelliJ IDEA.
 * User: Antwork
 * Date: 18/11/2012
 * Time: 17:38
 * To change this template use File | Settings | File Templates.
 */
public enum CommentType {
    CLUB,
    NATIONALTEAM,
    DIVISON;

    public static CommentType fromOrd(int i) {
        if (i < 0 || i >= CommentType.values().length) {
            throw new IndexOutOfBoundsException("Invalid ordinal");
        }
        return CommentType.values()[i];
    }
}
