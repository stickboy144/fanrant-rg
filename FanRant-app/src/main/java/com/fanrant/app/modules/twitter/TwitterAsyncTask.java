package com.fanrant.app.modules.twitter;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import com.fanrant.app.model.FanRantDataModel;
import com.fanrant.app.model.FanRantModel;
import com.fanrant.app.model.objects.twitterrss.Item;
import com.fanrant.app.model.objects.twitterrss.Rss;
import com.fanrant.app.modules.api.FanRantAPIParams;
import com.fanrant.app.modules.api.FanRantAsyncTask;
import com.fanrant.app.modules.util.ApplicationMessages;
import com.fanrant.app.modules.util.FeedType;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.http.converter.xml.SimpleXmlHttpMessageConverter;
import org.springframework.web.client.RestTemplate;
import roboguice.util.RoboAsyncTask;

import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: anthonygrimmitt
 * Date: 07/04/2013
 * Time: 19:37
 * To change this template use File | Settings | File Templates.
 */
public class TwitterAsyncTask extends RoboAsyncTask<FanRantDataModel[]> {
    private String url;

    public TwitterAsyncTask(Context context, Handler handler, String url) {
        super(context, handler);
        this.url = url;
    }

    @Override
    public FanRantDataModel[] call() throws Exception {
        // Create a new RestTemplate instance
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new SimpleXmlHttpMessageConverter());

        Rss twitterFeed = restTemplate.getForObject(url, Rss.class);
        final String TWITTER = "EEE',' dd MMM yyyy HH:mm:ss ZZZZZ";
        SimpleDateFormat sf = new SimpleDateFormat(TWITTER);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        List tmp = new ArrayList();

        if (twitterFeed == null) {
            throw new Exception("unable to retrieve twitter feed");
        } else {
            for (Item item : twitterFeed.getChannel().getItem()) {
                FanRantDataModel model = new FanRantDataModel();
                model.username = item.getLink();
                model.comment = item.getTitle();
                model.type = FeedType.TWITTER;
                model.link = item.getLink();
                Date date = null;
                date = sf.parse(item.getPubDate());
                model.post_time = sdf.format(date);
                tmp.add(model);


            }
            return (FanRantDataModel[]) tmp.toArray(new FanRantDataModel[tmp.size()]);

        }
    }

    @Override
    protected void onException(Exception e) throws RuntimeException {
        Message message = handler.obtainMessage(ApplicationMessages.ASYNCTASKFAILED.ordinal(), e);
        handler.sendMessage(message);
    }

    @Override
    protected void onSuccess(FanRantDataModel[] model) throws Exception {
        Message message = handler().obtainMessage(ApplicationMessages.ASYNCTASKSUCESS.ordinal(), model);
        handler.sendMessage(message);
    }


}