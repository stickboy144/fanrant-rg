package com.fanrant.app.modules.services;

import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.os.*;
import android.os.Process;
import android.widget.Toast;
import com.fanrant.app.Environments;
import com.fanrant.app.events.DataLoadedEvent;
import com.fanrant.app.events.FanRantErrorEvent;
import com.fanrant.app.model.FanRantDataModel;
import com.fanrant.app.model.IFanRantUserModel;
import com.fanrant.app.modules.IPreferences;
import com.fanrant.app.modules.api.FanRantAPIParams;
import com.fanrant.app.modules.api.FanRantAsyncTask;
import com.fanrant.app.modules.api.MockAsyncTask;
import com.fanrant.app.modules.api.responders.FanRantDataResponse;
import com.fanrant.app.modules.db.DatabaseManager;
import com.fanrant.app.modules.db.FanRantDBOpenHelper;
import com.fanrant.app.modules.db.FanRantDatabase;
import com.fanrant.app.modules.providers.WallContentProvider;
import com.fanrant.app.modules.rss.RSSAsyncTask;
import com.fanrant.app.modules.twitter.TwitterAsyncTask;
import com.fanrant.app.modules.util.*;
import com.google.inject.Inject;
import com.j256.ormlite.android.AndroidConnectionSource;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import roboguice.RoboGuice;
import roboguice.event.EventManager;
import roboguice.event.Observes;
import roboguice.service.RoboService;
import roboguice.util.RoboAsyncTask;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Ant Grimmitt
 * Date: 30/03/2013
 * Time: 10:54
 */
public class FanRantHomeWallService extends RoboService {
    private static final int START_TIMER = 556;
    private static final int STOP_TIMER = 557;
    private static final long TIMER_PERIOD = 30000; //30 seconds
    private Looper mServiceLooper;
    private ServiceHandler mServiceHandler;

    @Inject
    IPreferences preferences;

    @Inject
    IFanRantUserModel userModel;

    @Inject
    EventManager eventManager;

    private ResultReceiver receiver;
    private Object twitterFeed;
    private boolean populateInit = false;
    private String lastTime;
    private DataLoadedEvent globalDataLoadedEvent;

    public void getRssFeed() {

        Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                if (msg.what == ApplicationMessages.ASYNCTASKSUCESS.ordinal()) {
                    if (isPopulateInit()) {
                        addData((FanRantDataModel[]) msg.obj, FeedType.TEAMNEWS);
                    } else {
                        addData((FanRantDataModel[]) msg.obj, FeedType.TEAMNEWS);
                    }
                } else if (msg.what == ApplicationMessages.ASYNCTASKFAILED.ordinal()) {
                    eventManager.fire(new FanRantErrorEvent((Exception) msg.obj));
                }
                globalDataLoadedEvent.setRssLoaded(true);
                dataLoadedEvent(globalDataLoadedEvent);
                super.handleMessage(msg);    //To change body of overridden methods use File | Settings | File Templates.
            }
        };

        new RSSAsyncTask(getApplicationContext(), handler, userModel.getTeam_rss_feed()).execute();

    }


    public void getTwitterFeed() {


        Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                if (msg.what == ApplicationMessages.ASYNCTASKSUCESS.ordinal()) {
                    if (isPopulateInit()) {
                        addData((FanRantDataModel[]) msg.obj, FeedType.TWITTER);
                    } else {
                        addData((FanRantDataModel[]) msg.obj, FeedType.TWITTER);
                    }
                } else if (msg.what == ApplicationMessages.ASYNCTASKFAILED.ordinal()) {

                    /**
                     * silent twitter errors
                      */
                    Bundle data = new Bundle();
//                    data.putString("error", ((Exception) msg.obj).getMessage());
//                    receiver.send(-1, data);

                }

                globalDataLoadedEvent.setTwitterLoaded(true);
                dataLoadedEvent(globalDataLoadedEvent);

                super.handleMessage(msg);    //To change body of overridden methods use File | Settings | File Templates.
            }
        };

        new TwitterAsyncTask(getApplicationContext(), handler, userModel.getTeam_twitter_feed()).execute();

    }


    // Handler that receives messages from the thread
    private final class ServiceHandler extends CustomTimeNamedHandler {
        public ServiceHandler(Looper looper) {
            super(looper, Timers.HOMEWALL);
        }

        @Override
        public void handleMessage(Message msg) {

            switch (msg.what) {
                case START_TIMER:
                    run();
                    break;
                case STOP_TIMER:
                    stop();
                    stopSelf(msg.arg1);
                    break;
            }
        }
    }

    private void getFanRantWallComments() {
        HashMap<String, String> postData = new HashMap<String, String>();
        postData.put("method", "getLatestComments");
        postData.put("division_id", userModel.getCompetition_id());
        postData.put("club_id", String.valueOf(userModel.getTeam_id()));

        FanRantAPIParams params = new FanRantAPIParams(FanRantAPIParams.ApiService.DATA,
                FanRantDataResponse.class, postData, preferences);

        Handler dataHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                if (msg.what == ApplicationMessages.ASYNCTASKSUCESS.ordinal()) {
                    addData((FanRantDataModel[]) msg.obj, FeedType.COMMENTS);
                } else if (msg.what == ApplicationMessages.ASYNCTASKFAILED.ordinal()) {
                    eventManager.fire(new FanRantErrorEvent((Exception) msg.obj));

                }

                globalDataLoadedEvent.setCommentsLoaded(true);

                dataLoadedEvent(globalDataLoadedEvent);

            }
        };

        RoboAsyncTask t = preferences.getEnvironment() == Environments.TESTING ?
                new MockAsyncTask(getApplicationContext(), params, dataHandler) :
                new FanRantAsyncTask(getApplicationContext(), params, dataHandler);
        t.execute();
    }

    private void addData(FanRantDataModel[] models, FeedType type) {
        FanRantDBOpenHelper databaseHelper = getHelper();
        ConnectionSource connectionSource =
                new AndroidConnectionSource(databaseHelper);
        databaseHelper.onCreate(databaseHelper.getWritableDatabase(), connectionSource);

            try {
                Dao<FanRantDataModel, Integer> dataModelDao = DaoManager.createDao(connectionSource, FanRantDataModel.class);

                for (FanRantDataModel model : models) {
                    HashMap<String,Object> map = new HashMap<String, Object>();
                map.put("comment", model.getComment());
                map.put("post_time", model.getPost_time());

                List<FanRantDataModel> m = dataModelDao.queryForFieldValuesArgs(map);
                if (m.size() <= 0 ) {
                        dataModelDao.createIfNotExists(model);
                } else {
                    FanRantDataModel current = m.get(0);
                    if(current != model) {
                        model.setDataId(current.getDataId());
                        dataModelDao.update(model);
                    }
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } finally {
            connectionSource.closeQuietly();

        }



    }

    @Override
    public void onCreate() {
        HandlerThread thread = new HandlerThread("ServiceStartArguments",
                Process.THREAD_PRIORITY_BACKGROUND);
        thread.start();

        // Get the HandlerThread's Looper and use it for our Handler

        mServiceLooper = thread.getLooper();
        mServiceHandler = new ServiceHandler(mServiceLooper);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        RoboGuice.getInjector(getApplicationContext()).injectMembersWithoutViews(this);
        // For each start request, send a message to start a job and deliver the
        // start ID so we know which request we're stopping when we finish the job
        Message msg = mServiceHandler.obtainMessage();
        msg.arg1 = startId;
        msg.what = START_TIMER;

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                try {
                    getFanRantWallComments();
                    getRssFeed();
                    getTwitterFeed();
                } finally {
//                    mServiceHandler.start(TIMER_PERIOD);
                }
            }
        };

        globalDataLoadedEvent = new DataLoadedEvent();
        mServiceHandler.setTask(runnable);

        mServiceHandler.sendMessage(msg);

        receiver = intent.getParcelableExtra("receiver");


        // If we get killed, after returning from here, restart
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // We don't provide binding, so return null
        return null;
    }

    @Override
    public void onDestroy() {
    }

    public boolean isPopulateInit() {
        return populateInit;
    }

    public void dataLoadedEvent(@Observes DataLoadedEvent dataLoadedEvent) {
        if (dataLoadedEvent.isAllDataStreamsLoaded()) {
            mServiceHandler.start(TIMER_PERIOD);
            globalDataLoadedEvent = new DataLoadedEvent();
           receiver.send(1,null);
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
//        super.onConfigurationChanged(newConfig);    //To change body of overridden methods use File | Settings | File Templates.
    }

    private FanRantDBOpenHelper getHelper() {

        DatabaseManager<FanRantDBOpenHelper> manager = new DatabaseManager<FanRantDBOpenHelper>();
        return manager.getHelper(this);
    }

}