package com.fanrant.app.modules.api;

/**
 * Created with IntelliJ IDEA.
 * User: Ant Grimmitt
 * Date: 27/03/2013
 * Time: 11:50
 */
public interface IFRUrls {
    String getUrl();
}
