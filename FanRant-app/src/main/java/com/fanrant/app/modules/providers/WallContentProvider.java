package com.fanrant.app.modules.providers;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;
import com.fanrant.app.modules.db.FanRantDatabase;

/**
 * Created with IntelliJ IDEA.
 * User: Ant Grimmitt
 * Date: 26/03/2013
 * Time: 21:14
 */
public class WallContentProvider extends ContentProvider {

    public static String AUTHORITY = "com.fanrant.app.modules.providers.WallContentProvider";
    public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/wall");
    public static final Uri CONTENT_LAST_INSERT_URI = Uri.parse("content://" + AUTHORITY + "/walllastid");
    private FanRantDatabase mDb;

    public static final String WALLS_MIME_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE +
            "/vnd.com.fanrant.app.modules.db.wall";


    // UriMatcher stuff
    private static final int WALL = 0;
    private static final int WALLLASTID = 1;
    private static final UriMatcher sURIMatcher = buildUriMatcher();
    private Cursor wall;
    private Cursor lastInsert;

    /**
     * Builds up a UriMatcher for getting walls or a wall
     */
    private static UriMatcher buildUriMatcher() {
        UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
        // to get definitions...
        matcher.addURI(AUTHORITY, "wall", WALL);
        matcher.addURI(AUTHORITY, "walllastid", WALLLASTID);
        // to get suggestions...
        return matcher;
    }

    @Override
    public boolean onCreate() {
        mDb = new FanRantDatabase(getContext());
        return true;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs,
                        String sortOrder) {

        switch (sURIMatcher.match(uri)) {

            case WALL:

                Cursor c = getWall();
                if (c != null) {
                    c.setNotificationUri(getContext().getContentResolver(), WallContentProvider.CONTENT_URI);
                }
                return c;
            case WALLLASTID:
                return getLastInsert();

        }
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }


    /**
     * This method is required in order to query the supported types.
     * It's also useful in our own query() method to determine the type of Uri received.
     */
    @Override
    public String getType(Uri uri) {
        switch (sURIMatcher.match(uri)) {
            case WALL:
                return WALLS_MIME_TYPE;
            default:
                throw new IllegalArgumentException("Unknown URL " + uri);
        }
    }

    @Override
    public Uri insert(Uri uri, ContentValues initialValues) {
        return null;
    }

    @Override
    public int delete(Uri uri, String s, String[] strings) {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public int update(Uri uri, ContentValues contentValues, String s, String[] strings) {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }


    public Cursor getWall() {

        return null;
    }

    public Cursor getLastInsert() {
        return mDb.getLastInsert();
    }
}
