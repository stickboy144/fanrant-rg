package com.fanrant.app.modules.api.client;

import com.fanrant.app.modules.api.FanRantAPIParams;
import com.fanrant.app.modules.api.FanRantAPIResponse;

/**
 * Created with IntelliJ IDEA.
 * User: Antwork
 * Date: 27/10/2012
 * Time: 13:58
 * To change this template use File | Settings | File Templates.
 */
public interface IDataClient {

    FanRantAPIResponse post(FanRantAPIParams params);

    void setForceFail(boolean fail);
}
