package com.fanrant.app.modules.api.responders;

import com.fanrant.app.model.FanRantFixtureModel;
import com.fanrant.app.model.FanRantModel;
import com.fanrant.app.modules.api.FanRantAPIResponse;

/**
 * Created with IntelliJ IDEA.
 * User: anthonygrimmitt
 * Date: 14/04/2013
 * Time: 19:03
 * To change this template use File | Settings | File Templates.
 */
public class FanRantFixtureResponse extends FanRantAPIResponse {
    FanRantFixtureModel[] results;
    @Override
    public FanRantFixtureModel[] getResults() {
        return results;  //To change body of implemented methods use File | Settings | File Templates.
    }
}
