package com.fanrant.app.modules.api.responders;

import com.fanrant.app.model.FanRantDataModel;
import com.fanrant.app.modules.api.FanRantAPIResponse;

/**
 * Created with IntelliJ IDEA.
 * User: Ant Grimmitt
 * Date: 31/03/2013
 * Time: 17:39
 */
public class
        FanRantDataResponse extends FanRantAPIResponse {
    private FanRantDataModel[] results;

    @Override
    public FanRantDataModel[] getResults() {
        return results;  //To change body of implemented methods use File | Settings | File Templates.
    }
}
