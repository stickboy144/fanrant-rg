package com.fanrant.app.modules.util;

/**
 * Created with IntelliJ IDEA.
 * User: Antwork
 * Date: 02/12/2012
 * Time: 21:14
 * To change this template use File | Settings | File Templates.
 */
public enum GameEvent {
    ATTENDANCE("attendance"),
    EXTRAFIRSTHALF("extrafirsthalf"),
    EXTRASECONDHALF("extrasecondhalf"),
    FIRSTHALF("firsthalf"),
    FULLTIME("fulltime"),
    GOAL("goal"),
    HALFTIME("halftime"),
    INJURY("injury"),
    MISSEDPENALTY("missedpenalty"),
    PENALTY("penalty"),
    PENALTYSHOT("penaltyshot"),
    POSTPONEMENT("postponement"),
    PREMATCH("prematch"),
    PREMATCHAWAYLINEUP("prematch-away-lineup"),
    PREMATCHAWAYSUBS("prematch-away-subs"),
    PREMATCHHOMELINEUP("prematch-home-lineup"),
    PREMATCHHOMESUBS("prematch-home-subs"),
    REDCARD("red card"),
    SCORELINE("scoreline"),
    SECONDYELLOWCARD("second yellow card"),
    SECONDHALF("secondhalf"),
    SHOOTOUT("shootout"),
    STATUS("status"),
    SUBSTITUTION("substitution"),
    YELLOWCARD("yellow card");

    private final String event;

    private GameEvent(String s) {
        this.event = s;
    }

    public String getEvent() {
        return this.event;
    }

    public static GameEvent fromString(String text) {
        if (text != null) {
            for (GameEvent gameEvent : GameEvent.values()) {
                if (text.equalsIgnoreCase(gameEvent.getEvent())) {
                    return gameEvent;
                }
            }
        }
        return null;
    }
}


