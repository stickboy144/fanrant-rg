package com.fanrant.app.modules.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;
import com.fanrant.R;
import com.fanrant.app.modules.util.CacheStore;
import com.novoda.imageloader.core.util.DirectLoader;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created with IntelliJ IDEA.
 * User: Antwork
 * Date: 04/11/2012
 * Time: 16:01
 * To change this template use File | Settings | File Templates.
 */
public class AvatarLoaderTask extends AsyncTask<String, Bitmap, Bitmap> {
    private String strurl;
    private final WeakReference<ImageView> imageViewReference;

    private CacheStore cache = CacheStore.getInstance();


    public AvatarLoaderTask(ImageView imageView) {
        imageViewReference = new WeakReference<ImageView>(imageView);
    }


    @Override
    protected Bitmap doInBackground(String... params) {
            strurl = params[0];
            DirectLoader dl = new DirectLoader();
            Bitmap b = dl.download(strurl);
            return b;

    }


    @Override
    protected void onPostExecute(Bitmap result) {
        if (imageViewReference != null) {
            ImageView imageView = imageViewReference.get();
            if (imageView != null) {
                System.out.println("setting image");
                imageView.setImageBitmap(result);
            } else {
                System.err.println("ImageView null");
            }
        }
    }

    @Override
    protected void onPreExecute() {
    }
}

