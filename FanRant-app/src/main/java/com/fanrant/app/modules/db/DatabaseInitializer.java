package com.fanrant.app.modules.db;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseInitializer extends SQLiteOpenHelper{
	
    private static String DB_NAME = "db.sqlite";
 
    private SQLiteDatabase database; 
    private final Context context;
    
    public DatabaseInitializer(Context context) {
    	super(context, DB_NAME, null, 1);
        this.context = context;
    }	
    

    @Override
	public synchronized void close() {
	    if(database != null)
		    database.close();
	    
	    super.close();
	}
 
	@Override
	public void onCreate(SQLiteDatabase db) {
	}
 
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
	}
 
}
