package com.fanrant.app.modules.services;

import android.content.Intent;
import android.os.*;
import android.os.Process;
import android.widget.Toast;
import com.fanrant.app.Environments;
import com.fanrant.app.events.FanRantErrorEvent;
import com.fanrant.app.model.FanRantDataModel;
import com.fanrant.app.model.FanRantFixtureModel;
import com.fanrant.app.model.IFanRantUserModel;
import com.fanrant.app.model.FanRantOfficialsModel;
import com.fanrant.app.model.FanRantPlayerModel;
import com.fanrant.app.modules.IPreferences;
import com.fanrant.app.modules.api.FanRantAPIParams;
import com.fanrant.app.modules.api.FanRantAsyncTask;
import com.fanrant.app.modules.api.MockAsyncTask;
import com.fanrant.app.modules.api.responders.FanRantFixtureResponse;
import com.fanrant.app.modules.db.DatabaseManager;
import com.fanrant.app.modules.db.FanRantDBOpenHelper;
import com.fanrant.app.modules.util.ApplicationMessages;
import com.fanrant.app.modules.util.CustomTimeNamedHandler;
import com.fanrant.app.modules.util.FeedType;
import com.fanrant.app.modules.util.Timers;
import com.google.inject.Inject;
import com.j256.ormlite.android.AndroidConnectionSource;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.support.ConnectionSource;
import roboguice.RoboGuice;
import roboguice.event.EventManager;
import roboguice.service.RoboService;
import roboguice.util.RoboAsyncTask;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Ant Grimmitt
 * Date: 30/03/2013
 * Time: 10:54
 */
public class FanRantLiveFixturesService extends RoboService {
    private static final int START_TIMER = 556;
    private static final int STOP_TIMER = 557;
    private static final long TIMER_PERIOD = 30000; //30 seconds
    private Looper mServiceLooper;
    private ServiceHandler mServiceHandler;

    @Inject
    IPreferences preferences;

    @Inject
    IFanRantUserModel userModel;

    @Inject
    EventManager eventManager;

    private ResultReceiver receiver;
    private boolean populateInit = false;
    private String lastTime;


    // Handler that receives messages from the thread
    private final class ServiceHandler extends CustomTimeNamedHandler {
        public ServiceHandler(Looper looper) {
            super(looper, Timers.HOMEWALL);
        }

        @Override
        public void handleMessage(Message msg) {

            switch (msg.what) {
                case START_TIMER:
                    run();
                    break;
                case STOP_TIMER:
                    stop();
                    stopSelf(msg.arg1);
                    break;
            }
        }
    }

    private void getFixtures() {

//        Cursor cursor = getContentResolver().query(WallContentProvider.CONTENT_URI, null, null, null, null);
//
//        if (cursor == null) {
            populateInit = true;

            HashMap<String, String> postData = new HashMap<String, String>();
            postData.put("method", "search");
            postData.put("live", "true");

            FanRantAPIParams params = new FanRantAPIParams(FanRantAPIParams.ApiService.FIXTURE,
                    FanRantFixtureResponse.class, postData, preferences);

            Handler dataHandler = new Handler() {
                @Override
                public void handleMessage(Message msg) {
                    if (msg.what == ApplicationMessages.ASYNCTASKSUCESS.ordinal()) {
                        addData((FanRantFixtureModel[]) msg.obj);
                    } else if (msg.what == ApplicationMessages.ASYNCTASKFAILED.ordinal()) {
                        eventManager.fire(new FanRantErrorEvent((Exception) msg.obj));

                    }
                }
            };

            RoboAsyncTask t = preferences.getEnvironment() == Environments.TESTING ?
                    new MockAsyncTask(getApplicationContext(), params, dataHandler) :
                    new FanRantAsyncTask(getApplicationContext(), params, dataHandler);
            t.execute();
//        } else {
//            populateInit = false;
//            try {
//                Cursor cursor1 = getContentResolver().query(WallContentProvider.CONTENT_LAST_INSERT_URI, null, null, null, null);
//
//                if (cursor1.moveToFirst()) {
//                    lastTime = cursor1.getString(cursor1.getColumnIndex(FanRantDatabase.WALL_POST_TIME));
//
//
//                    HashMap<String, String> postData = new HashMap<String, String>();
//                    postData.put("method", "search");
//                    postData.put("live", "true");
//
//                    FanRantAPIParams params = new FanRantAPIParams(FanRantAPIParams.ApiService.DATA,
//                            FanRantFixtureResponse.class, postData, preferences);
//
//                    Handler dataHandler = new Handler() {
//                        @Override
//                        public void handleMessage(Message msg) {
//                            if (msg.what == ApplicationMessages.ASYNCTASKSUCESS.ordinal()) {
//                                addData((FanRantFixtureModel[]) msg.obj);
//                            } else if (msg.what == ApplicationMessages.ASYNCTASKFAILED.ordinal()) {
//                                eventManager.fire(new FanRantErrorEvent((Exception) msg.obj));
//
//                            }
//                        }
//                    };
//
//                    RoboAsyncTask t = preferences.getEnvironment() == Environments.TESTING ?
//                            new MockAsyncTask(getApplicationContext(), params, dataHandler) :
//                            new FanRantAsyncTask(getApplicationContext(), params, dataHandler);
//                    t.execute();
//

        }


    private void addData(FanRantFixtureModel[] models) {
        FanRantDBOpenHelper databaseHelper = getHelper();
        ConnectionSource connectionSource =
                new AndroidConnectionSource(databaseHelper);
        databaseHelper.onCreate(databaseHelper.getWritableDatabase(), connectionSource);

        try {
            Dao<FanRantFixtureModel, String> fixtureModelDao = DaoManager.createDao(connectionSource, FanRantFixtureModel.class);
            Dao<FanRantPlayerModel, String> playersDAO = DaoManager.createDao(connectionSource, FanRantPlayerModel.class);
            Dao<FanRantOfficialsModel, String> officialsDAO = DaoManager.createDao(connectionSource, FanRantOfficialsModel.class);
              fixtureModelDao.delete(fixtureModelDao.queryForAll());
            for (FanRantFixtureModel model : models) {
                fixtureModelDao.createOrUpdate(model);
                for (Object player : model.getHome_team_players()) {
                    FanRantPlayerModel p = (FanRantPlayerModel)player;
                    p.setFixtureModel(model);
                    p.setTeam_id(model.getHome_team_id());
                    p.setTeam_name(model.getHome_team_name());
                    playersDAO.createIfNotExists(p);

                }
                for (Object player : model.getAway_team_players()) {
                    FanRantPlayerModel p = (FanRantPlayerModel)player;
                    p.setFixtureModel(model);
                    p.setTeam_id(model.getAway_team_id());
                    p.setTeam_name(model.getAway_team_name());
                    playersDAO.createIfNotExists(p);
                }

                for (Object officials : model.getOfficials()) {
                    FanRantOfficialsModel o = (FanRantOfficialsModel)officials;
                    o.setModel(model);
                    officialsDAO.createIfNotExists(o);

                }


            }

                receiver.send(1, null);

        } catch (SQLException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        mServiceHandler.start(TIMER_PERIOD);
    }

    @Override
    public void onCreate() {
        HandlerThread thread = new HandlerThread("ServiceStartArguments",
                Process.THREAD_PRIORITY_BACKGROUND);
        thread.start();

        // Get the HandlerThread's Looper and use it for our Handler

        mServiceLooper = thread.getLooper();
        mServiceHandler = new ServiceHandler(mServiceLooper);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        RoboGuice.getInjector(getApplicationContext()).injectMembersWithoutViews(this);
        // For each start request, send a message to start a job and deliver the
        // start ID so we know which request we're stopping when we finish the job
        Message msg = mServiceHandler.obtainMessage();
        msg.arg1 = startId;
        msg.what = START_TIMER;

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                try {
                    getFixtures();
                } finally {
                }
            }
        };

        mServiceHandler.setTask(runnable);

        mServiceHandler.sendMessage(msg);

        receiver = intent.getParcelableExtra("receiver");
    return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // We don't provide binding, so return null
        return null;
    }

    @Override
    public void onDestroy() {
    }

    public boolean isPopulateInit() {
        return populateInit;
    }


    private FanRantDBOpenHelper getHelper() {

        DatabaseManager<FanRantDBOpenHelper> manager = new DatabaseManager<FanRantDBOpenHelper>();
        return manager.getHelper(this);
    }


}