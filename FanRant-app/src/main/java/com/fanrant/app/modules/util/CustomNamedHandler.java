package com.fanrant.app.modules.util;

import android.os.Handler;

/**
 * Created with IntelliJ IDEA.
 * User: Antwork
 * Date: 18/11/2012
 * Time: 17:21
 * To change this template use File | Settings | File Templates.
 */
public class CustomNamedHandler extends Handler {
        private Handlers handlerName;

    public CustomNamedHandler(Handlers handlerName) {
        this.handlerName = handlerName;
    }

    public Handlers getHandlerName() {
        return handlerName;
    }
}
