package com.fanrant.app.modules.providers;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;
import android.provider.BaseColumns;
import com.fanrant.app.modules.db.FanRantDatabase;

/**
 * Created with IntelliJ IDEA.
 * User: Ant Grimmitt
 * Date: 26/03/2013
 * Time: 21:14
 */
public class TeamContentProvider extends ContentProvider {

    public static String AUTHORITY = "com.fanrant.app.modules.providers.TeamContentProvider";
    public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/team");
    public static final Uri DIVISIONS_URI = Uri.parse("content://" + AUTHORITY + "/team/divisions");
    public static final Uri DIVISION_NAMES_URI = Uri.parse("content://" + AUTHORITY + "/team/divisions/byname");
    private FanRantDatabase mDb;

    public static final String TEAMS_MIME_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE +
            "/vnd.com.fanrant.app.modules.db.team";


    // UriMatcher stuff
    private static final int TEAM = 0;
    private static final int GET_TEAMS = 1;
    private static final int GET_DIVISIONS = 2;
    private static final int GET_DIVISION_NAMES = 3;
    private static final UriMatcher sURIMatcher = buildUriMatcher();

    /**
     * Builds up a UriMatcher for getting teams or a team
     */
    private static UriMatcher buildUriMatcher() {
        UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
        // to get definitions...
        matcher.addURI(AUTHORITY, "team", TEAM);
        matcher.addURI(AUTHORITY, "team/#", GET_TEAMS);
        matcher.addURI(AUTHORITY, "team/divisions", GET_DIVISIONS);
        matcher.addURI(AUTHORITY, "team/divisions/byname", GET_DIVISION_NAMES);
        // to get suggestions...
        return matcher;
    }

    @Override
    public boolean onCreate() {
       mDb = new FanRantDatabase(getContext());
        return true;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs,
                        String sortOrder) {

        switch (sURIMatcher.match(uri)) {

            case GET_TEAMS:
                if (selectionArgs == null) {
                    throw new IllegalArgumentException(
                            "selectionArgs must be provided for the Uri: " + uri);
                }

                return getDivisions(selectionArgs[0]);
            case GET_DIVISION_NAMES:
                return mDb.getDivisionsNames(selectionArgs[0],projection);
            case GET_DIVISIONS:
                return getDivisions(selectionArgs[0]);

        }
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    private Cursor getDivisions(String selectionArgs) {
        String[] columns = new String[] {
                FanRantDatabase.COMPETITION_NAME,
        };
        Cursor c = mDb.getDivisions(selectionArgs,columns);
        mDb.close();
        return c;
    }


    /**
     * This method is required in order to query the supported types.
     * It's also useful in our own query() method to determine the type of Uri received.
     */
    @Override
    public String getType(Uri uri) {
        switch (sURIMatcher.match(uri)) {
            case TEAM:
                return TEAMS_MIME_TYPE;
            case GET_TEAMS:
                return TEAMS_MIME_TYPE;
            default:
                throw new IllegalArgumentException("Unknown URL " + uri);
        }
    }

    @Override
    public Uri insert(Uri uri, ContentValues initialValues) {
        return null;
    }

    @Override
    public int delete(Uri uri, String s, String[] strings) {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public int update(Uri uri, ContentValues contentValues, String s, String[] strings) {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }


}
