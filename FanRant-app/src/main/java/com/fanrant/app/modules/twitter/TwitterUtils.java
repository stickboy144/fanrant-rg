package com.fanrant.app.modules.twitter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import com.fanrant.app.events.FanRantTwitterAuthEvent;
import com.fanrant.app.modules.IPreferences;
import com.fanrant.app.modules.util.ApplicationMessages;
import com.google.inject.Inject;
import com.google.inject.Provider;
import oauth.signpost.OAuth;
import oauth.signpost.OAuthConsumer;
import oauth.signpost.OAuthProvider;
import oauth.signpost.commonshttp.CommonsHttpOAuthConsumer;
import oauth.signpost.commonshttp.CommonsHttpOAuthProvider;
import roboguice.event.EventManager;
import roboguice.util.RoboAsyncTask;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;

import java.util.HashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * Created with IntelliJ IDEA.
 * User: Antwork              111111111
 * Date: 21/02/2013
 * Time: 20:17
 * To change this template use File | Settings | File Templates.
 */
public class TwitterUtils implements ITwitterUtils {

    private static final String TAG = "Twitter Auth";
    @Inject
    IPreferences preferences;
    @Inject
    EventManager eventManager;
    public final String CALLBACKURL = "app://twitter";
    private Provider<Context> contextProvider;
    private String token;
    private String secret;
    private CommonsHttpOAuthConsumer consumer;
    private CommonsHttpOAuthProvider provider;

    @Inject
    public TwitterUtils(Provider<Context> contextProvider) {
        this.contextProvider = contextProvider;

        try {
            consumer = new CommonsHttpOAuthConsumer(TwitterConstants.CONSUMER_KEY, TwitterConstants.CONSUMER_SECRET);
            provider = new CommonsHttpOAuthProvider(TwitterConstants.REQUEST_URL, TwitterConstants.ACCESS_URL, TwitterConstants.AUTHORIZE_URL);
        } catch (Exception e) {
            Log.e(TAG, "Error creating consumer / provider", e);
        }
    }

    protected Context getContext() {
        return contextProvider.get();

    }


    @Override
    public void authenticate(Handler handler) {
        if (isTwitterPrefsSet()) {
            AccessToken accessToken = new AccessToken(token, secret);
            new TwitterAuthenticationCheckingTask(getContext(), accessToken, handler).execute();
        }

    }


    @Override
    public void sendTweet(String msg, Handler handler) throws Exception {
        if (isTwitterPrefsSet()) {
            AccessToken accessToken = new AccessToken(token, secret);
            new TweetTask(getContext(), accessToken, handler, msg).execute();
        }
    }


    @Override
    public void loginAndAuthenicate( final Handler handler, final AsyncTask asyncTask) {
        if (isTwitterPrefsSet()) {
            authenticate(handler);
        } else {
            eventManager.fire(new FanRantTwitterAuthEvent());

                    }
    }

    @Override
    public String getAuthCallback() {
        return CALLBACKURL;
    }

    @Override
    public void retrieveAccessToken(Handler handler, Uri uri) {
        new RetrieveAccessTokenTask(getContext(), handler).execute(uri);
    }

    @Override
    public CommonsHttpOAuthConsumer getConsumer() {
        return consumer;
    }

    @Override
    public CommonsHttpOAuthProvider getProvider() {
        return provider;
    }

    @Override
    public boolean isTwitterPrefsSet() {
        token = preferences.getPref(OAuth.OAUTH_TOKEN) != null ?
                preferences.getPref(OAuth.OAUTH_TOKEN) : null;
        secret = preferences.getPref(OAuth.OAUTH_TOKEN_SECRET) != null ?
                preferences.getPref(OAuth.OAUTH_TOKEN_SECRET) : null;

        return (token != null || secret != null);
    }


    class TwitterAuthenticationCheckingTask extends RoboAsyncTask<Boolean> {

        private AccessToken aToken;
        private Handler handler;

        TwitterAuthenticationCheckingTask(Context context, AccessToken aToken, Handler handler) {
            super(context);
            this.aToken = aToken;
            this.handler = handler;
        }

        @Override
        public Boolean call() throws Exception {
            Twitter twitter = new TwitterFactory().getInstance();
            twitter.setOAuthConsumer(TwitterConstants.CONSUMER_KEY, TwitterConstants.CONSUMER_SECRET);
            twitter.setOAuthAccessToken(aToken);

            try {
                twitter.getAccountSettings();
                return true;
            } catch (TwitterException e) {
                return false;
            }
        }

        @Override
        protected void onSuccess(Boolean aBoolean) throws Exception {
            if (aBoolean) {
                handler.sendEmptyMessage(ApplicationMessages.TWITTERAUTH);
            }
        }
    }

    class TweetTask extends RoboAsyncTask<Status> {

        private AccessToken aToken;
        private Handler handler;
        private String msg;

        TweetTask(Context context, AccessToken aToken, Handler handler, String msg) {
            super(context);
            this.aToken = aToken;
            this.handler = handler;
            this.msg = msg;
        }

        @Override
        public Status call() throws Exception {
            Twitter twitter = new TwitterFactory().getInstance();
            twitter.setOAuthConsumer(TwitterConstants.CONSUMER_KEY, TwitterConstants.CONSUMER_SECRET);
            twitter.setOAuthAccessToken(aToken);

            try {
                return twitter.updateStatus(msg);
            } catch (TwitterException e) {

                return null;
            }
        }

        @Override
        protected void onSuccess(Status s) throws Exception {
            if (s != null) {
                handler.sendEmptyMessage(ApplicationMessages.TWEETED);
            }
        }
    }

    public class RetrieveAccessTokenTask extends AsyncTask<Uri, Void, Void> {

        private Context context;
        private Handler handler;

        public RetrieveAccessTokenTask(Context context, Handler handler) {
            this.context = context;
            this.handler = handler;
        }


        /**
         * Retrieve the oauth_verifier, and store the oauth and oauth_token_secret
         * for future API calls.
         */
        @Override
        protected Void doInBackground(Uri... params) {
            final Uri uri = params[0];
            final String oauth_verifier = uri.getQueryParameter(OAuth.OAUTH_VERIFIER);

            try {
                provider.retrieveAccessToken(consumer, oauth_verifier);

                HashMap<String, String> map = new HashMap<String, String>();
                map.put(OAuth.OAUTH_TOKEN, consumer.getToken());
                map.put(OAuth.OAUTH_TOKEN_SECRET, consumer.getTokenSecret());


                map.put("twitterEnabled", "true");
                preferences.addPref(map);

                String token = preferences.getPref(OAuth.OAUTH_TOKEN);
                String secret = preferences.getPref(OAuth.OAUTH_TOKEN_SECRET);

                consumer.setTokenWithSecret(token, secret);


                Log.i(TAG, "OAuth - Access Token Retrieved");

                handler.sendEmptyMessage(ApplicationMessages.ASYNCTASKSUCESS.ordinal());


            } catch (Exception e) {
                Log.e(TAG, "OAuth - Access Token Retrieval Error", e);
            }

            return null;
        }


    }

}