package com.fanrant.app.modules.api;

import com.fanrant.app.modules.api.IFanRantAPIResponse;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Created with IntelliJ IDEA.
 * User: Antwork
 * Date: 21/10/2012
 * Time: 12:59
 * To change this template use File | Settings | File Templates.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class FanRantAPIResponse implements IFanRantAPIResponse {
    private int code;
    private String message;

     @JsonIgnore
    private String time;
    private String token;

    public FanRantAPIResponse() {
    }

    public FanRantAPIResponse(int code, String message) {
        this.code = code;
        this.message = message;
     }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
