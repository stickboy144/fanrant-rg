package com.fanrant.app.modules.api;

import android.content.Context;
import android.os.Handler;
import com.fanrant.app.model.FanRantModel;
import com.fanrant.app.model.IFanRantUserModel;
import com.fanrant.app.modules.api.client.IDataClient;
import com.google.inject.Inject;

/**
 * Created with IntelliJ IDEA.
 * User: Antwork
 * Date: 23/03/2013
 * Time: 18:14
 * To change this template use File | Settings | File Templates.
 */
public class MockAsyncTask extends FanRantAsyncTask {
    @Inject
    IFanRantUserModel userModel;
    @Inject
    IDataClient IDataClient;


    private FanRantAPIParams params;
    private Handler loginHandler;

    public MockAsyncTask(Context context, FanRantAPIParams params, Handler handler) {
        super(context, params, handler);
    }



    @Override
    public void execute() {
        try {
            Thread t = new Thread(new Runnable() {
                @Override
                public void run() {

                    try {
                        onSuccess(call());
                    } catch (Exception e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                }
            }, "test login thread");
            t.run();
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    @Override
    protected void onSuccess(FanRantModel[] model) throws Exception {
        super.onSuccess(model);
    }

    //    @Override
//    public FanRantUserModel call() throws Exception {
//        FanRantUserApiResponse result = IDataClient.getProfile(params);
//
//        if (result != null) {
//            if (result.getCode() == 200) {
//                FanRantUserModel user = (FanRantUserModel) result.getResults()[0];
//                /**
//                 * save user and pass
//                 */
//                user.setUsername(params[0]);
//                user.setPassword(params[1]);
//                return user;
//
//            } else {
//                switch (result.getCode()) {
//                    case 401:
////                        context.hideProgessBar();
////                        context.showFanRantDialog(R.string.logindetailserror, false);
//                        break;
//                }
//                return null;
//            }
//        }
//
//        return  null;
//    }


}

