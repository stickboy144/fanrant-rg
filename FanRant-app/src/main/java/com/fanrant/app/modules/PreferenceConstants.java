package com.fanrant.app.modules;

/**
 * Created with IntelliJ IDEA.
 * User: Ant Grimmitt
 * Date: 23/03/2013
 * Time: 20:05
 */
public class PreferenceConstants {
    public static final String UNREGUSER = "unregistered_user";
    public static final String UNREGTEAMID = "unregistered_team_id";
    public static final String USERNAME = "username";
    public static final String PASSWORD = "password";
    public static final String UID = "uid";
    public static final String UNREGCOMPID = "unregistered_comp_id";
    public static final String SAVEDUSERMODEL = "saved_user_model";
}
