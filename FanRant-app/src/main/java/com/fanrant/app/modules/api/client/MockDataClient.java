package com.fanrant.app.modules.api.client;

import com.fanrant.app.FanRantJson;
import com.fanrant.app.modules.api.FanRantAPIParams;
import com.fanrant.app.modules.api.FanRantAPIResponse;
import com.fanrant.app.modules.api.responders.FanRantTeamResponse;
import com.fanrant.app.modules.api.responders.FanRantTeamsByDivisionResponse;
import com.fanrant.app.modules.api.responders.FanRantUserResponse;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;

//import com.fanrant.app.modules.api.FanRantJson;

public class MockDataClient implements IDataClient {
    private boolean forceFail;
    private FanRantAPIParams params;

    //
//    @Override
//    public FanRantUserApiResponse getProfile(String... params) {
//
//        FanRantUserApiResponse user = null;
//
//        String json = null;
//        if (params[0].equals("ant2") && params[1].equals("000000")) {
//            json = FanRantJson.GETPROFILE;
//        } else {
//            json = FanRantJson.GETWRONGLOGINPROFILE;
//        }
//        user = (FanRantUserApiResponse) getApiResponse(json, FanRantUserApiResponse.class);
//        return user;
//    }
//
    public FanRantAPIResponse getApiResponse(String json, Class clazz) {
        ObjectMapper mapper = new ObjectMapper();

        FanRantAPIResponse o = null;
        try {
            o = (FanRantAPIResponse) mapper.readValue(json,
                    clazz);
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return o;
    }
//

    @Override
    public FanRantAPIResponse post(FanRantAPIParams params) {
        this.params = params;

        return getApiResponse(renderTestJson(), params.getResponseClass());  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void setForceFail(boolean fail) {
        this.forceFail = fail;
    }

    public boolean isForceFail() {
        return forceFail;
    }

    private String renderTestJson() {
        if(params.getResponseClass() == FanRantUserResponse.class) {
            return isForceFail() ? FanRantJson.GETWRONGLOGINPROFILE : FanRantJson.GETPROFILE;
        }

        if(params.getResponseClass() == FanRantTeamsByDivisionResponse.class) {
            return FanRantJson.GETTEAMDATAGUESTMODE;
        }
        if(params.getResponseClass() == FanRantTeamResponse.class) {
            return FanRantJson.GETTEAMDATAGUESTMODE;
        }
        return null;
    }
}
