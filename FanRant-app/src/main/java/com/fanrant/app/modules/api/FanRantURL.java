package com.fanrant.app.modules.api;

import android.content.res.AssetManager;
import com.fanrant.app.modules.IPreferences;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created with IntelliJ IDEA.
 * User: Ant Grimmitt
 * Date: 27/03/2013
 * Time: 11:22
 */
@Singleton
public class FanRantURL implements IFRUrls {
//
//    @Inject
//    Provider<AssetManager> assetManagerProvider;

    @Inject
    IPreferences preferences;

    private String url;

    private AssetManager mAssetManager;

    @Inject
    public FanRantURL(AssetManager assetManager) {
        mAssetManager = assetManager;

    }


    public void setup() {

        Properties properties = null;
        try {
            InputStream inputStream = mAssetManager.open("url.properties");
            properties = new Properties();
            properties.load(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }

        if(properties != null) {

            switch (preferences.getEnvironment()) {
                case DEFAULT:
                    setUrl(properties.getProperty("default.url"));
                    break;
                case TESTING:
                case DEV:
                    setUrl(properties.getProperty("dev.url"));
                    break;
                case ANTDEV:
                    setUrl(properties.getProperty("ant.dev.url"));
            }

        }

    }

    public String getUrl() {
        if(url == null) {
            setup();
        }
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
