package com.fanrant.app.modules.api;

import com.fanrant.app.model.FanRantDataModel;
import com.fanrant.app.model.FanRantModel;

/**
 * Created with IntelliJ IDEA.
 * User: Ant Grimmitt
 * Date: 27/03/2013
 * Time: 14:59
 */
public interface IFanRantAPIResponse {
    <T extends FanRantModel> T[] getResults();

    int getCode();

    String getMessage();
}
