package com.fanrant.app.modules.api.responders;

import com.fanrant.app.model.FanRantDataModel;
import com.fanrant.app.model.FanRantTeamsByCompetitonModel;
import com.fanrant.app.modules.api.FanRantAPIResponse;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Created with IntelliJ IDEA.
 * User: Antwork
 * Date: 04/11/2012
 * Time: 15:03
 * To change this template use File | Settings | File Templates.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class FanRantTeamsByDivisionResponse extends FanRantAPIResponse {
    private FanRantTeamsByCompetitonModel[] results;

    public FanRantTeamsByDivisionResponse(int code, String message, FanRantTeamsByCompetitonModel[] results, Object type) {
        super(code, message);
        this.results = results;
    }

    public FanRantTeamsByDivisionResponse() {
    }

    public FanRantTeamsByCompetitonModel[] getResults() {
        return results;
    }



}
