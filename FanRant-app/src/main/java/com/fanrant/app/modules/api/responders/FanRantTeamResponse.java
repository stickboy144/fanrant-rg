package com.fanrant.app.modules.api.responders;

import com.fanrant.app.model.FanRantDataModel;
import com.fanrant.app.model.FanRantModel;
import com.fanrant.app.model.FanRantTeamModel;
import com.fanrant.app.model.TeamModel;
import com.fanrant.app.modules.api.FanRantAPIResponse;

/**
 * Created with IntelliJ IDEA.
 * User: Ant Grimmitt
 * Date: 29/03/2013
 * Time: 22:01
 */
public class FanRantTeamResponse extends FanRantAPIResponse {
    private FanRantTeamModel[] results;
    @Override
    public FanRantModel[] getResults() {
        return results;  //To change body of implemented methods use File | Settings | File Templates.
    }
}
