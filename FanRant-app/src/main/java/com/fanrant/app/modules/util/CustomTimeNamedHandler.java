package com.fanrant.app.modules.util;

import android.os.Handler;
import android.os.Looper;

/**
 * Created with IntelliJ IDEA.
 * User: Antwork
 * Date: 18/11/2012
 * Time: 17:21
 * To change this template use File | Settings | File Templates.
 */
public class CustomTimeNamedHandler extends Handler {
    public static final int TASKCOMPLETE = 100;
    public static final int TASKSTARTED = 101;
    private Timers timerName;

    private Runnable task;

    public CustomTimeNamedHandler(Timers timerName) {
        this.timerName = timerName;
    }

    public CustomTimeNamedHandler(Looper looper,Timers timerName) {
        super(looper);
        this.timerName = timerName;
    }

    public Timers getTimerName() {
        return timerName;
    }

    public Runnable getTask() {
        return task;
    }

    public void setTask(Runnable task) {
        this.task = task;
    }


    public void start(long millis) {
        removeCallbacks(task); //ensure mock are dead
        postDelayed(task,millis);
    }

    public void stop() {
        removeCallbacks(task);
    }


    public void run() {
        removeCallbacks(task);
        post(task);
    }
}
