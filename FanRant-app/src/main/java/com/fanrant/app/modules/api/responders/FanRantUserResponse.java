package com.fanrant.app.modules.api.responders;

import com.fanrant.app.model.FanRantUserModel;
import com.fanrant.app.model.IFanRantUserModel;
import com.fanrant.app.modules.api.FanRantAPIResponse;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Created with IntelliJ IDEA.
 * User: Antwork
 * Date: 04/11/2012
 * Time: 15:03
 * To change this template use File | Settings | File Templates.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class FanRantUserResponse extends FanRantAPIResponse {
    private FanRantUserModel[] results;

    public FanRantUserResponse(int code, String message, FanRantUserModel[] results, Object type) {
        super(code, message);
        this.results = results;
    }

    public FanRantUserResponse() {
    }

    public FanRantUserModel[] getResults() {
        return results;
    }



}
