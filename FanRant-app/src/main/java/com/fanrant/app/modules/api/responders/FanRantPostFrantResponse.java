package com.fanrant.app.modules.api.responders;

import com.fanrant.app.model.FanRantCommentModel;
import com.fanrant.app.modules.api.FanRantAPIResponse;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Created with IntelliJ IDEA.
 * User: anthonygrimmitt
 * Date: 13/04/2013
 * Time: 11:25
 * To change this template use File | Settings | File Templates.
 */
@JsonIgnoreProperties(ignoreUnknown = true, value = {"results"})
public class FanRantPostFrantResponse extends FanRantAPIResponse {
     @JsonIgnore(value = true)
    private String[] results;
    @Override
    @JsonIgnore
    public FanRantCommentModel[] getResults() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

}

