package com.fanrant.app.modules.api.responders;

import com.fanrant.app.model.FanRantWallStatusModel;
import com.fanrant.app.modules.api.FanRantAPIResponse;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Created with IntelliJ IDEA.
 * User: anthonygrimmitt
 * Date: 11/05/2013
 * Time: 17:52
 * To change this template use File | Settings | File Templates.
 */
public class FanRantWallStatusResponse extends FanRantAPIResponse {
    @JsonProperty("results")
    private FanRantWallStatusModel result;
    @Override
    public FanRantWallStatusModel[] getResults() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public FanRantWallStatusModel getResult() {
        return result;
    }

}
