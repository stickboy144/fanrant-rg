package com.fanrant.app.modules.services;

import android.content.Intent;
import android.os.*;
import android.os.Process;
import android.widget.Toast;
import com.fanrant.app.Environments;
import com.fanrant.app.events.FanRantErrorEvent;
import com.fanrant.app.mod.FanRantGameLatestUpdateModel;
import com.fanrant.app.model.*;
import com.fanrant.app.modules.IPreferences;
import com.fanrant.app.modules.api.FanRantAPIParams;
import com.fanrant.app.modules.api.FanRantAsyncTask;
import com.fanrant.app.modules.api.MockAsyncTask;
import com.fanrant.app.modules.api.responders.FanRantFixtureResponse;
import com.fanrant.app.modules.api.responders.FanRantGameUpdateResponse;
import com.fanrant.app.modules.db.DatabaseManager;
import com.fanrant.app.modules.db.FanRantDBOpenHelper;
import com.fanrant.app.modules.util.ApplicationMessages;
import com.fanrant.app.modules.util.CustomTimeNamedHandler;
import com.fanrant.app.modules.util.FeedType;
import com.fanrant.app.modules.util.Timers;
import com.google.inject.Inject;
import com.j256.ormlite.android.AndroidConnectionSource;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.support.ConnectionSource;
import roboguice.RoboGuice;
import roboguice.event.EventManager;
import roboguice.service.RoboService;
import roboguice.util.RoboAsyncTask;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Ant Grimmitt
 * Date: 30/03/2013
 * Time: 10:54
 */
public class FanRantGameUpdatesService extends RoboService {
    private static final int START_TIMER = 556;
    private static final int STOP_TIMER = 557;
    private static final long TIMER_PERIOD = 30000; //30 seconds
    private Looper mServiceLooper;
    private ServiceHandler mServiceHandler;

    @Inject
    IPreferences preferences;

    @Inject
    IFanRantUserModel userModel;

    @Inject
    EventManager eventManager;

    private ResultReceiver receiver;
    private boolean populateInit = false;
    private String lastTime;
    private String fixtureId;


    private void cipherData(FanRantDataModel[] dataModels, FeedType ftype) throws ParseException {
        List<FanRantDataModel> tmp = new ArrayList<FanRantDataModel>();
        if (lastTime != null) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
            Date lastDate = sdf.parse(lastTime);

            for (FanRantDataModel dataModel : dataModels) {
                Date itemDate = sdf.parse(dataModel.post_time);
                if (itemDate.after(lastDate)) {
                    tmp.add(dataModel);
                }
            }

//            addData((FanRantDataModel[]) tmp.toArray(new FanRantDataModel[tmp.size()]), ftype);
        }
    }

    // Handler that receives messages from the thread
    private final class ServiceHandler extends CustomTimeNamedHandler {

        public ServiceHandler(Looper looper) {
            super(looper, Timers.HOMEWALL);
        }

        @Override
        public void handleMessage(Message msg) {

            switch (msg.what) {
                case START_TIMER:
                    run();
                    break;
                case STOP_TIMER:
                    stop();
                    stopSelf(msg.arg1);
                    break;
            }
        }
    }

    private void getFixtures() {

//        Cursor cursor = getContentResolver().query(WallContentProvider.CONTENT_URI, null, null, null, null);
//
//        if (cursor == null) {
        populateInit = true;

        HashMap<String, String> postData = new HashMap<String, String>();
        postData.put("method", "getWall");
        postData.put("fixture_id", getFixtureId());
        FanRantAPIParams params = new FanRantAPIParams(FanRantAPIParams.ApiService.FIXTURE,
                FanRantGameUpdateResponse.class, postData, preferences);


        Handler dataHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                if (msg.what == ApplicationMessages.ASYNCTASKSUCESS.ordinal()) {
                    addData((FanRantGameLatestUpdateModel[]) msg.obj);
                } else if (msg.what == ApplicationMessages.ASYNCTASKFAILED.ordinal()) {
                    eventManager.fire(new FanRantErrorEvent((Exception) msg.obj));

                }
            }
        };


        RoboAsyncTask t = preferences.getEnvironment() == Environments.TESTING ?
                new MockAsyncTask(getApplicationContext(), params, dataHandler) :
                new FanRantAsyncTask(getApplicationContext(), params, dataHandler);
        t.execute();
//        } else {

//            populateInit = false;
//            try {
//                Cursor cursor1 = getContentResolver().query(WallContentProvider.CONTENT_LAST_INSERT_URI, null, null, null, null);
//
//                if (cursor1.moveToFirst()) {
//                    lastTime = cursor1.getString(cursor1.getColumnIndex(FanRantDatabase.WALL_POST_TIME));
//
//
//                    HashMap<String, String> postData = new HashMap<String, String>();
//                    postData.put("method", "search");
//                    postData.put("live", "true");
//                       88
//                    FanRantAPIParams params = new FanRantAPIParams(FanRantAPIParams.ApiService.DATA,
//                            FanRantFixtureResponse.class, postData, preferences);
//
//                    Handler dataHandler = new Handler() {
//                        @Override
//                        public void handleMessage(Message msg) {
//                            if (msg.what == ApplicationMessages.ASYNCTASKSUCESS.ordinal()) {
//                                addData((FanRantFixtureModel[]) msg.obj);
//                            } else if (msg.what == ApplicationMessages.ASYNCTASKFAILED.ordinal()) {
//                                eventManager.fire(new FanRantErrorEvent((Exception) msg.obj));
//
//                            }
//                        }
//                    };
//
//                    RoboAsyncTask t = preferences.getEnvironment() == Environments.TESTING ?
//                            new MockAsyncTask(getApplicationContext(), params, dataHandler) :
//                            new FanRantAsyncTask(getApplicationContext(), params, dataHandler);
//                    t.execute();
//

    }


    private void addData(FanRantGameLatestUpdateModel[] models) {
        FanRantDBOpenHelper databaseHelper = getHelper();
        ConnectionSource connectionSource =
                new AndroidConnectionSource(databaseHelper);
        databaseHelper.onCreate(databaseHelper.getWritableDatabase(), connectionSource);
        try {
            Dao<FanRantGameUpdateModel, String> updateModelDAO = DaoManager.createDao(connectionSource, FanRantGameUpdateModel.class);
            Dao<FanRantGameUpdatePlayerModel, String> gameUpdatePlayerModelDao = DaoManager.createDao(connectionSource, FanRantGameUpdatePlayerModel.class);
//            Dao<FanRantPlayerModel, String> playersDAO = DaoManager.createDao(connectionSource, FanRantPlayerModel.class);
            Dao<FanRantGameUpdateOfficialModel, String> officialsDAO = DaoManager.createDao(connectionSource, FanRantGameUpdateOfficialModel.class);

            for (FanRantGameLatestUpdateModel fanRantGameUpdateModel : models) {
                for (FanRantGameUpdateModel gameUpdate : fanRantGameUpdateModel.getLatest()) {

                    if (updateModelDAO.idExists(gameUpdate.getPost_id())) {
                        FanRantGameUpdateModel current = updateModelDAO.queryForId(gameUpdate.getPost_id());
                        if (current != gameUpdate) {

                            updateModelDAO.update(gameUpdate);
//                            for (FanRantGameUpdatePlayerModel model : gameUpdate.getPlayers()) {
//                                model.setGameUpdateModel(gameUpdate);
//                                gameUpdatePlayerModelDao.createIfNotExists(model);
//                            }
                        }
                    } else {

                        updateModelDAO.createIfNotExists(gameUpdate);
                        for (FanRantGameUpdatePlayerModel model : gameUpdate.getPlayers()) {
                            model.setGameUpdateModel(gameUpdate);
                            gameUpdatePlayerModelDao.createIfNotExists(model);
                        }


                        if (gameUpdate.getOfficials() != null) {
                            for (FanRantGameUpdateOfficialModel officialModel : gameUpdate.getOfficials()) {
                                officialModel.setModel(gameUpdate);
                                officialsDAO.createIfNotExists(officialModel);

                            }
                        }
                    }
                }
            }


        } catch (SQLException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        receiver.send(0, null);
        mServiceHandler.start(TIMER_PERIOD);
    }

    @Override
    public void onCreate() {
        HandlerThread thread = new HandlerThread("ServiceStartArguments",
                Process.THREAD_PRIORITY_BACKGROUND);
        thread.start();

        // Get the HandlerThread's Looper and use it for our Handler

        mServiceLooper = thread.getLooper();
        mServiceHandler = new ServiceHandler(mServiceLooper);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        receiver = intent.getParcelableExtra("receiver");
        fixtureId = intent.getStringExtra("fixture_id");
        RoboGuice.getInjector(getApplicationContext()).injectMembersWithoutViews(this);
        // For each start request, send a message to start a job and deliver the
        // start ID so we know which request we're stopping when we finish the job
        Message msg = mServiceHandler.obtainMessage();
        msg.arg1 = startId;
        msg.what = START_TIMER;

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                try {
                    getFixtures();
                } finally {
                }
            }
        };

        mServiceHandler.setTask(runnable);

        mServiceHandler.sendMessage(msg);

        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // We don't provide binding, so return null
        return null;
    }

    @Override
    public void onDestroy() {
    }

    public boolean isPopulateInit() {
        return populateInit;
    }


    private FanRantDBOpenHelper getHelper() {

        DatabaseManager<FanRantDBOpenHelper> manager = new DatabaseManager<FanRantDBOpenHelper>();
        return manager.getHelper(this);
    }


    public String getFixtureId() {
        return fixtureId;
    }

    public void setFixtureId(String fixtureId) {
        this.fixtureId = fixtureId;
    }
}