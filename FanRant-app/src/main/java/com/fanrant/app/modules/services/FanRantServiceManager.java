package com.fanrant.app.modules.services;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import com.fanrant.app.events.FanRantErrorEvent;
import com.google.inject.Inject;
import roboguice.event.EventManager;

import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * User: anthonygrimmitt
 * Date: 11/05/2013
 * Time: 12:48
 * To change this template use File | Settings | File Templates.
 */
public class FanRantServiceManager implements IFanRantServiceManager {
    @Inject
    EventManager eventManager;
    private Context mContext;

    public FanRantServiceManager(Context mContext) {
        this.mContext = mContext;
        services = new HashMap<FanRantService, FanRantServiceIntent>();
    }

    private HashMap<FanRantService, FanRantServiceIntent> services;


    @Override
    public FanRantServiceManager addService(FanRantService service, Class clazz, ResultReceiver resultReceiver) {
        services.put(service, new FanRantServiceIntent(mContext, clazz, resultReceiver));
        return this;
    }

    @Override
    public IFanRantServiceManager addService(FanRantService service, Class clazz, ResultReceiver resultReceiver, Bundle b) {
        services.put(service, new FanRantServiceIntent(mContext, clazz, resultReceiver,b));

        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void startService(FanRantService
                                         service) {
        if (services.containsKey(service)) {
            mContext.startService(services.get(service));
            services.get(service).setRunning(true);
        } else {
            eventManager.fire(new FanRantErrorEvent(new Exception("Service has not been added")));
        }
    }

    @Override
    public void restartService(FanRantService service) {
        if (services.containsKey(service)) {
            mContext.stopService(services.get(service));
            mContext.startService(services.get(service));
            services.get(service).setRunning(true);
        } else {
            eventManager.fire(new FanRantErrorEvent(new Exception("Service has not been added")));
        }
    }

    @Override
    public void stopService(FanRantService service) {
        if(services.containsKey(service)) {
            mContext.stopService(services.get(service));
            services.get(service).setRunning(false);

        }
    }

    @Override
    public void stopAllServices() {
        for (FanRantService fanRantService : services.keySet()) {
            mContext.stopService(services.get(fanRantService));
            services.get(fanRantService).setRunning(false);
        }
    }

    @Override
    public void addAndStartService(FanRantService service, Class clazz, ResultReceiver resultReceiver, Bundle b) {
        services.put(service, new FanRantServiceIntent(mContext, clazz, resultReceiver,b));
        mContext.startService(services.get(service));
    }


    public HashMap<FanRantService, FanRantServiceIntent> getServices() {
        return services;
    }


    class FanRantServiceIntent extends Intent {
        private boolean running = false;
        private ResultReceiver resultReceiver;
        private Bundle bundle;
        public FanRantServiceIntent(Context packageContext, Class<?> cls, ResultReceiver resultReceiver) {
            super(packageContext, cls);
            this.resultReceiver = resultReceiver;
            putExtra("receiver", resultReceiver);
        }

        FanRantServiceIntent(Context packageContext, Class<?> cls, ResultReceiver resultReceiver, Bundle bundle) {
            super(packageContext, cls);
            this.bundle = bundle;
            this.resultReceiver = resultReceiver;
            putExtra("receiver", resultReceiver);
            putExtras(bundle);

        }

        public boolean isRunning() {
            return running;
        }

        public void setRunning(boolean running) {
            this.running = running;
        }
    }
}
