package com.fanrant.app.modules.api.responders;

import com.fanrant.app.model.FanRantModel;
import com.fanrant.app.model.FanRantTVShowModel;
import com.fanrant.app.modules.api.FanRantAPIResponse;

/**
 * Created with IntelliJ IDEA.
 * User: anthonygrimmitt
 * Date: 14/04/2013
 * Time: 13:55
 * To change this template use File | Settings | File Templates.
 */
public class FanRantTvScheduleResponse extends FanRantAPIResponse {
    private FanRantTVShowModel[] results;
    @Override
    public FanRantTVShowModel[] getResults() {
        return results;  //To change body of implemented methods use File | Settings | File Templates.
    }
}
