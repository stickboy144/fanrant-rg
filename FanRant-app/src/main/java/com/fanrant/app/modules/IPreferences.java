package com.fanrant.app.modules;

import com.fanrant.app.Environments;

import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * User: Ant Grimmitt
 * Date: 23/03/2013
 * Time: 21:32
 */
public interface IPreferences {

    String getPref(String key);

    boolean isPref(String key);
    void addPref(HashMap<String, String> map);
    void setEnvironment(Environments environment);
    Environments getEnvironment();

    String getUrl();
    void clear();
}
