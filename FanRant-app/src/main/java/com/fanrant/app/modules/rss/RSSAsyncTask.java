package com.fanrant.app.modules.rss;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import com.fanrant.app.model.FanRantDataModel;
import com.fanrant.app.modules.util.ApplicationMessages;
import com.fanrant.app.modules.util.FeedType;
import com.fanrant.app.view.fragments.adapters.FanRantDataAdapter;

import com.google.code.rome.android.repackaged.com.sun.syndication.feed.rss.Channel;
import com.google.code.rome.android.repackaged.com.sun.syndication.feed.rss.Item;
import org.springframework.http.MediaType;
import org.springframework.http.converter.feed.RssChannelHttpMessageConverter;
import org.springframework.web.client.RestTemplate;
import roboguice.util.RoboAsyncTask;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: anthonygrimmitt
 * Date: 07/04/2013
 * Time: 15:37
 * To change this template use File | Settings | File Templates.
 */
public class RSSAsyncTask extends RoboAsyncTask<FanRantDataModel[]> {

    private String url;
    private FanRantDataAdapter dataAdapter;
    private FeedType type;
    private String team_rss_feed;

    public RSSAsyncTask(Context context, Handler handler) {
        super(context, handler);
    }

    public RSSAsyncTask(Context baseContext, Handler rssHandler, String team_rss_feed) {
        super(baseContext, rssHandler);    //To change body of overridden methods use File | Settings | File Templates.
        this.team_rss_feed = team_rss_feed;
    }


    @Override
    protected void onPreExecute() {
//        context.showLoadingProgressDialog();
    }


    @Override
    public FanRantDataModel[] call() throws Exception {
        RestTemplate restTemplate = new RestTemplate();

        RssChannelHttpMessageConverter rssChannelConverter = new RssChannelHttpMessageConverter();
        rssChannelConverter.setSupportedMediaTypes(Collections.singletonList(MediaType.TEXT_XML));

        restTemplate.getMessageConverters().add(rssChannelConverter);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        List tmp = new ArrayList();

        Channel channel = restTemplate.getForObject(team_rss_feed, Channel.class);

        for (Object o : channel.getItems()) {
            Item item = (Item) o;
            FanRantDataModel model = new FanRantDataModel();
            model.username = item.getAuthor();
            model.comment = item.getTitle();
            model.type = FeedType.TEAMNEWS;
            model.post_time = sdf.format(item.getPubDate());
            model.link = item.getLink();
            tmp.add(model);


        }

        return (FanRantDataModel[]) tmp.toArray(new FanRantDataModel[tmp.size()]);

    }

    @Override
    protected void onException(Exception e) throws RuntimeException {
        Message message = handler.obtainMessage(ApplicationMessages.ASYNCTASKFAILED.ordinal(), e);
        handler.sendMessage(message);
    }

    @Override
    protected void onSuccess(FanRantDataModel[] model) throws Exception {
        Message message = handler().obtainMessage(ApplicationMessages.ASYNCTASKSUCESS.ordinal(), model);
        handler.sendMessage(message);
    }


}