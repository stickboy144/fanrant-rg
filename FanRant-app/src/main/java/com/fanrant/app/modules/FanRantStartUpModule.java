package com.fanrant.app.modules;

import com.fanrant.app.model.FanRantUserModel;
import com.fanrant.app.model.IFanRantUserModel;
import com.fanrant.app.modules.api.client.HTTPClient;
import com.fanrant.app.modules.api.client.IDataClient;
import com.fanrant.app.modules.twitter.ITwitterUtils;
import com.fanrant.app.modules.twitter.TwitterUtils;
import com.google.inject.Binder;
import com.google.inject.Module;
import com.google.inject.Scopes;
import roboguice.inject.SharedPreferencesName;

/**
 * Created with IntelliJ IDEA.
 * User: Ant Grimmitt
 * Date: 23/03/2013
 * Time: 21:36
 */

public class FanRantStartUpModule implements Module {
    public void configure(Binder binder) {
        binder.bind(IPreferences.class).to(FanRantPreferences.class);
        binder.bind(IFanRantUserModel.class).to(FanRantUserModel.class).in(Scopes.NO_SCOPE);
        binder.bind(IDataClient.class).to(HTTPClient.class);
        binder.bind(ITwitterUtils.class).to(TwitterUtils.class);

        binder.bindConstant().annotatedWith(SharedPreferencesName
                .class).to("com.fanrant.app");
//        binder.bind(IFRUrls.class).to(FanRantURL.class);


    }
}
