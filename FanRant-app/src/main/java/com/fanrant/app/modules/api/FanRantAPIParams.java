package com.fanrant.app.modules.api;

import com.fanrant.app.modules.IPreferences;
import com.google.inject.Inject;

import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Ant Grimmitt
 * Date: 27/03/2013
 * Time: 11:20
 */
public class FanRantAPIParams {


    public enum ApiService {
        POST("post"),
        TEAM("team"),
        USER("user"),
        DATA("data"),
        FIXTURE("fixture");

        /**
         * @param text
         */
        private ApiService(final String text) {
            this.text = text;
        }

        private final String text;

        /* (non-Javadoc)
         * @see java.lang.Enum#toString()
         */
        @Override
        public String toString() {
            return text;
        }
    }

    private Class<? extends FanRantAPIResponse> responseClass;

    private String url;

    private ApiService method;

    private Map<String, ?> postData;

    @Inject
    public FanRantAPIParams(ApiService method, Class<? extends FanRantAPIResponse> responseClass, Map<String, ?> postData, IPreferences p) {

        this.method = method;
        this.responseClass = responseClass;
        this.postData = postData;
        this.url = p.getUrl() + method;
    }

    public ApiService getMethod() {
        return method;
    }

    public void setMethod(ApiService method) {
        this.method = method;
    }

    public Class<? extends FanRantAPIResponse> getResponseClass() {
        return responseClass;
    }

    public void setResponseClass(Class<? extends FanRantAPIResponse> responseClass) {
        this.responseClass = responseClass;
    }

    public Map<String, ?> getPostData() {
        return postData;
    }

    public void setPostData(Map<String, Object> postData) {
        this.postData = postData;
    }

    public String getUrl() {
        return url;
    }

    /**
     * helper setter only should only be called for testing
     * @param url
     */
    public void setUrl(String url) {
        this.url = url;
    }
}
