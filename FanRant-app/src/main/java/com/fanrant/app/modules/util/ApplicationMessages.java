package com.fanrant.app.modules.util;

import android.os.Message;

/**
 * Created with IntelliJ IDEA.
 * User: Antwork
 * Date: 13/11/2012
 * Time: 13:55
 * To change this template use File | Settings | File Templates.
 */
public enum ApplicationMessages {
    USERLOGGEDIN, GUESTMODEENABLED, ASYNCTASKSUCESS, ASYNCTASKFAILED, SERVICEERROR;
    public static final int TWITTERAUTH = 1;
    public static final int TWEETED = 22;
    public static final int TWITTERTIMEOUT = 55;
    public static final int TWITTERAUTHFAILED = 556456;
    public static final int TWITTERAUTHCODE = 145145;


    public static Message getMessage(ApplicationMessages msg) {
        Message message = new Message();
        message.what = msg.ordinal();
        return message;
    }


}
