package com.fanrant.app.modules.providers;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;
import com.fanrant.app.modules.db.FanRantDatabase;

/**
 * Created with IntelliJ IDEA.
 * User: Ant Grimmitt
 * Date: 26/03/2013
 * Time: 21:14
 */
public class FixtureContentProvider extends ContentProvider {

    public static String AUTHORITY = "com.fanrant.app.modules.providers.FixtureContentProvider";
    public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/fixtures");
    private FanRantDatabase mDb;

    public static final String FIXTURES_MIME_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE +
            "/vnd.com.fanrant.app.modules.db.fixture";


    // UriMatcher stuff
    private static final int FIXTURE = 0;
    private static final int GET_FIXTURES = 1;
    private static final int GET_DIVISIONS = 2;
    private static final int GET_DIVISION_NAMES = 3;
    private static final UriMatcher sURIMatcher = buildUriMatcher();

    /**
     * Builds up a UriMatcher for getting fixtures or a fixture
     */
    private static UriMatcher buildUriMatcher() {
        UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
        // to get definitions...
        matcher.addURI(AUTHORITY, "fixtures", FIXTURE);
        matcher.addURI(AUTHORITY, "fixtures/#", GET_FIXTURES);
        return matcher;
    }

    @Override
    public boolean onCreate() {
       mDb = new FanRantDatabase(getContext());
        return true;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs,
                        String sortOrder) {

        switch (sURIMatcher.match(uri)) {

            case GET_FIXTURES:

                return getFixtures();

        }
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    private Cursor getFixtures() {
//        Cursor c = mDb.getFixtures();
//        mDb.close();
        return null;
    }


    /**
     * This method is required in order to query the supported types.
     * It's also useful in our own query() method to determine the type of Uri received.
     */
    @Override
    public String getType(Uri uri) {
        switch (sURIMatcher.match(uri)) {
            case FIXTURE:
                return FIXTURES_MIME_TYPE;
            case GET_FIXTURES:
                return FIXTURES_MIME_TYPE;
            default:
                throw new IllegalArgumentException("Unknown URL " + uri);
        }
    }

    @Override
    public Uri insert(Uri uri, ContentValues initialValues) {
        return null;
    }

    @Override
    public int delete(Uri uri, String s, String[] strings) {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public int update(Uri uri, ContentValues contentValues, String s, String[] strings) {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }


}
