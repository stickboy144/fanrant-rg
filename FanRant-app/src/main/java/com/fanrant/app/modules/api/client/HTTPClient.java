package com.fanrant.app.modules.api.client;

import com.fanrant.R;
import com.fanrant.app.model.FanRantModel;
import com.fanrant.app.modules.IPreferences;
import com.fanrant.app.modules.PreferenceConstants;
import com.fanrant.app.modules.api.FanRantAPIParams;
import com.fanrant.app.modules.api.FanRantAPIResponse;
import com.google.inject.Inject;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJacksonHttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;
import roboguice.event.EventManager;

import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Antwork
 * Date: 27/10/2012
 * Time: 14:00
 * To change this template use File | Settings | File Templates.
 */
public class HTTPClient implements IDataClient {

    @Inject
    IPreferences preferences;

    @Inject
    EventManager eventManager;

    public HTTPClient() {


    }

    @Override
    public FanRantAPIResponse post(FanRantAPIParams params) {

        FanRantAPIResponse response = null;
        SimpleClientHttpRequestFactory requestFactory = new SimpleClientHttpRequestFactory();
        requestFactory.setBufferRequestBody(false);
        RestTemplate rest = new RestTemplate(requestFactory);
        HttpMessageConverter formHttpMessageConverter = new FormHttpMessageConverter();
        HttpMessageConverter stringHttpMessageConverternew = new MappingJacksonHttpMessageConverter();
        List<HttpMessageConverter<?>> httpMessageConverters = new ArrayList<HttpMessageConverter<?>>();
        httpMessageConverters.add(formHttpMessageConverter);
        httpMessageConverters.add(stringHttpMessageConverternew);
        rest.setMessageConverters(httpMessageConverters);

        MultiValueMap<String, Object> map = new LinkedMultiValueMap<String, Object>();

        for (String key : params.getPostData().keySet()) {
            if (key.equals("avatar")) {
                map.add(key, new FileSystemResource((String) params.getPostData().get(key)));
            } else {
                map.add(key, params.getPostData().get(key));
            }
        }
        map.add("uid", preferences.getPref(PreferenceConstants.UID));

        try {
            response = rest.postForObject(params.getUrl(), map, params.getResponseClass());
        } catch (Exception ae) {
            ae.printStackTrace();
            response = new FanRantAPIResponse() {
                @Override
                public <T extends FanRantModel> T[] getResults() {
                    return null;  //To change body of implemented methods use File | Settings | File Templates.
                }
            };
            response.setCode(500);
            response.setMessage(ae.getMessage());
        }
        return response;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void setForceFail(boolean fail) {
        // not used here as its for testing
    }
}
