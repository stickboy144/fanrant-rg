package com.fanrant.app.modules.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import com.fanrant.app.mod.FanRantGameLatestUpdateModel;
import com.fanrant.app.model.*;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: anthonygrimmitt
 * Date: 14/04/2013
 * Time: 20:03
 * To change this template use File | Settings | File Templates.
 */
public class FanRantDBOpenHelper extends OrmLiteSqliteOpenHelper {
    // name of the database file for your application -- change to something appropriate for your app
    private static final String DATABASE_NAME = "fanrant.db";
    // any time you make changes to your database objects, you may have to increase the database version
    private static final int DATABASE_VERSION = 1;
    private Context context;
    private List<Class> tables;
    private FanRantDBOpenHelper databaseHelper;

    public FanRantDBOpenHelper(Context context, String databaseName, SQLiteDatabase.CursorFactory factory, int databaseVersion) {
        super(context, databaseName, factory, databaseVersion);
    }

    public FanRantDBOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
        DatabaseInitializer initializer = new DatabaseInitializer(context);
            initializer.close();

        tables = new ArrayList<Class>();
        tables.add(FanRantDataModel.class);
        tables.add(FanRantFixtureModel.class);
        tables.add(FanRantPlayerModel.class);
        tables.add(FanRantOfficialsModel.class);
        tables.add(FanRantGameUpdateModel.class);
        tables.add(FanRantGameUpdatePlayerModel.class);
        tables.add(FanRantGameUpdateOfficialModel.class);

    }

    /**
     * This is called when the database is first created. Usually you should call createTable statements here to create
     * the tables that will store your data.
     */
    @Override
    public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource) {
        try {
            for (Class table : tables) {
                TableUtils.createTableIfNotExists(connectionSource, table);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * This is called when your application is upgraded and it has a higher version number. This allows you to adjust
     * the various data to match the new version number.
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        try {
            for (Class table : tables) {
                TableUtils.dropTable(connectionSource, table,true);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
    /**
     * Close the database connections and clear any cached DAOs.
     */
    @Override
    public void close() {
        super.close();
    }
}
