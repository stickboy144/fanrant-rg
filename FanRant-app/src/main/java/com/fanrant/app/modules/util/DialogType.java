package com.fanrant.app.modules.util;

/**
 * Created with IntelliJ IDEA.
 * User: Antwork
 * Date: 18/11/2012
 * Time: 16:51
 * To change this template use File | Settings | File Templates.
 */
public enum DialogType {
    ERROR,
    WARN,
    COMMENTTYPE,
    WARNING,
    PLAYERS,
    TEAMS,
    SHARE,
    TWEET,
    PROGRESS, REGNOW;
}
