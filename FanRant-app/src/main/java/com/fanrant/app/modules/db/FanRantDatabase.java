package com.fanrant.app.modules.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.provider.BaseColumns;
import com.fanrant.app.mod.FanRantGameLatestUpdateModel;
import com.fanrant.app.model.*;
import com.fanrant.app.modules.util.FeedType;
import com.j256.ormlite.android.AndroidConnectionSource;
import com.j256.ormlite.android.AndroidDatabaseResults;
import com.j256.ormlite.dao.CloseableIterator;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Ant Grimmitt
 * Date: 26/03/2013
 * Time: 18:43
 */
public class FanRantDatabase implements IDatabaseHelper {
    private final FanRantOpenHelper helper;

    public static final int DATABASE_VERSION = 1;

    /**
     * tables
     */
    public static final String LIVE_GAME_TABLE = "livegame";
    public static final String PLAYER_TABLE = "play";
    public static final String FIXTURE_TABLE = "fixture";
    public static final String TEAMS_TABLE_NAME = "team";
    public static final String WALL_TABLE_NAME = "home_wall";
    public static final String TEAMSHEET_TABLE = "team_sheet";
    public static final String OFFICIAL_TABLE = "officials";
    public static final String GAME_OFFICIAL_TABLE = "gameoffical";

    /**
     * teams
     */
    public static final String TEAMID = "teamid";
    public static final String COMPETITION_ID = "competition_id";
    public static final String TEAMNAME = "name";
    public static final String RSS_FEED = "rssfeed";
    public static final String TWITTER_FEED = "twitter_feed";
    public static final String FIXTURES_URL = "fixtures_url";
    public static final String COMPETITION_NAME = "competition_name";

    /**
     * Wall data
     */
    public static final String WALL_POST_ID = "post_id";
    public static final String WALL_SUBJECT = "subject";
    public static final String WALL_USERNAME = "username";
    public static final String WALL_COMMENT = "comment";
    public static final String WALL_AVATAR = "avatar";
    public static final String WALL_POST_TIME = "post_time";
    public static final String WALL_POST_TYPE = "post_type";
    public static final String WALL_POST_LINK = "post_link";


    /**
     * Live Game data
     */

    public static final String GAME_AVATAR = "avatar";
    public static final String GAME_COMMENT = "comment";
    public static final String GAME_FIXTURE_ID = "fixture_id";
    public static final String GAME_IMAGE = "image";
    public static final String GAME_MATCH_TIME = "match_time";
    public static final String GAME_PLAYERS = "players";
    public static final String GAME_POST_ID = "post_id";
    public static final String GAME_POST_TIME = "post_time";
    public static final String GAME_POST_TYPE = "post_type";
    public static final String GAME_RATING = "rating";
    public static final String GAME_SUBJECT = "subject";
    public static final String GAME_TEAM_ID = "team_id";
    public static final String GAME_USER_ID = "user_id";
    public static final String GAME_USER_NAME = "user_name";

    /**
     * FanRantPlayerModel
     */

    public static final String PLAYER_FIRST_NAME = "first_name";
    public static final String PLAYER_ID = "id";
    public static final String PLAYER_LAST_NAME = "last_name";
    public static final String PLAYER_NICKNAME = "nickname";
    public static final String PLAYER_NUMBER = "number";
    public static final String PLAYER_POSITION = "position";
    public static final String PLAYER_TEAM_ID = "team_id";
    public static final String PLAYER_TEAM_NAME = "team_name";


    /**
     * FIXTURE
     */
    public static final String FIXTURE_AWAY_TEAM_ID = "away_team_id";
    public static final String FIXTURE_AWAY_TEAM_NAME = "away_team_name";
    public static final String FIXTURE_AWAY_TEAM_PLAYERS = "away_team_players";
    public static final String FIXTURE_COMPETITION_ID = "competition_id";
    public static final String FIXTURE_COMPETITION_NAME = "competition_name";
    public static final String FIXTURE_GROUND_ID = "ground_id";
    public static final String FIXTURE_GROUND_LATITUDE = "ground_latitude";
    public static final String FIXTURE_GROUND_LONGITUDE = "ground_longitude";
    public static final String FIXTURE_GROUND_NAME = "ground_name";
    public static final String FIXTURE_HOME_TEAM_ID = "home_team_id";
    public static final String FIXTURE_POSTPONED = "postponed";
    public static final String FIXTURE_HOME_TEAM_NAME = "home_team_name";
    public static final String FIXTURE_HOME_TEAM_PLAYERS = "home_team_players";
    public static final String FIXTURE_ID = "id";
    public static final String FIXTURE_KICKOFF = "kickoff";
    public static final String FIXTURE_LIVE = "live";
    public static final String FIXTURE_MATCHTIME = "matchtime";
    public static final String FIXTURE_OFFICIALS = "officials";
    public static final String FIXTURE_SCORELINE = "scoreline";


    /**
     * TEAMSHEET
     */

    public static final String TEAM_SHEET_FIX_ID = "fixid";
    public static final String TEAM_SHEET_PLAYER = "playerid";
    public static final String TEAM_SHEET_TEAM = "teamid";

    /**
     * Officals
     */

    public static final String OFF_FIRST_NAME = "first_name";
    public static final String OFF_HOME_TOWN = "home_town";
    public static final String OFF_ID = "id";
    public static final String OFF_LAST_NAME = "last_name";
    public static final String OFF_POSITION = "position";

    /**
     * Officating match
     */
    public static final String OFF_FIXID = "offfixid";
    public static final String OFF_OFFICIAL_ID = "offid";

    private static final HashMap<String, String> mColumnMap = buildColumnMap();


    private Context mContext;
    private Cursor wall;
    private Cursor lastInsert;
    private Cursor fixtures;

    public FanRantDatabase(Context context) {
//        RoboGuice.injectMembers(context, this);
        mContext = context;
        helper = new FanRantOpenHelper(mContext);
    }

    private static HashMap<String, String> buildColumnMap() {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(TEAMID, TEAMID);
        map.put(COMPETITION_ID, COMPETITION_ID);
        map.put(TEAMNAME, TEAMNAME);
        map.put(TWITTER_FEED, TWITTER_FEED);
        map.put(FIXTURES_URL, FIXTURES_URL);

        map.put(BaseColumns._ID, "rowid AS " +
                BaseColumns._ID);

        return map;
    }


    public void addAllTeams(FanRantTeamsByCompetitonModel[] teams) {
        for (FanRantTeamsByCompetitonModel team : teams) {
            for (TeamModel teamModel1 : team.getTeams()) {
                teamModel1.setCompetition_id(team.getCompetition_id());
                teamModel1.setCompetition_name(team.getCompetition_name());
                addTeam(teamModel1);
            }
        }
    }

    public long addTeam(TeamModel teamModel) {
        ContentValues contentValues = new ContentValues();

        contentValues.put(TEAMID, teamModel.getid());
        contentValues.put(TEAMNAME, teamModel.getName());
        contentValues.put(COMPETITION_ID, teamModel.getCompetition_id());
        contentValues.put(COMPETITION_NAME, teamModel.getCompetition_name());
        contentValues.put(RSS_FEED, teamModel.getTeam_rss_feed());
        contentValues.put(TWITTER_FEED, teamModel.getTeam_twitter_feed());
        contentValues.put(FIXTURES_URL, teamModel.getTeam_fixtures_url());

         long id = helper.getWritableDatabase().insert(TEAMS_TABLE_NAME, null, contentValues);  //To change body of created methods use File | Settings | File Templates.
        helper.getWritableDatabase().close();
        helper.close();
        return id;
    }

    public void truncate() {
        FanRantDBOpenHelper helper = getORMLiteHelper();
        ConnectionSource connectionSource =
                new AndroidConnectionSource(helper);
        try {
            TableUtils.clearTable(connectionSource, FanRantDataModel.class);
        } catch (SQLException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }


    public Cursor getDivisions(String selectionArgs, String[] columns) {
        SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
        builder.setTables(TEAMS_TABLE_NAME);
        HashMap<String, String> cols = new HashMap<String, String>();
        cols.put(TEAMID, TEAMID);
        cols.put(TEAMNAME, TEAMNAME);
        cols.put(COMPETITION_ID, COMPETITION_ID);
        cols.put(TEAMID, TEAMID + " AS " + BaseColumns._ID);
        builder.setProjectionMap(cols);
        builder.appendWhere(COMPETITION_ID + " = " + selectionArgs);

        Cursor cursor = builder.query(helper.getReadableDatabase(), null, COMPETITION_ID + " = " + selectionArgs, null, null, null, null);

        if (cursor == null) {
            return null;
        } else if (!cursor.moveToFirst()) {
            cursor.close();
            return null;
        }
        return cursor;
    }

    public Cursor getDivisionsNames(String selectionArgs, String[] columns) {
        SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
        builder.setTables(TEAMS_TABLE_NAME);
        HashMap<String, String> cols = new HashMap<String, String>();
        cols.put(COMPETITION_ID, COMPETITION_ID + " AS " + BaseColumns._ID);
        cols.put(COMPETITION_NAME, COMPETITION_NAME);
        builder.setProjectionMap(cols);
        builder.setDistinct(true);

        Cursor cursor = builder.query(helper.getReadableDatabase(), columns,
                selectionArgs + String.valueOf(COMPETITION_NAME + " LIKE 'english%'"), null, null, null, null);

        if (cursor == null) {
            return null;
        } else if (!cursor.moveToFirst()) {
            cursor.close();
            return null;
        }
        return cursor;
    }

    public void addData(FanRantDataModel[] models, FeedType type) {
        for (FanRantDataModel model : models) {

            ContentValues values = new ContentValues();
            values.put(WALL_POST_ID, model.post_id);
            values.put(WALL_SUBJECT, model.subject);
            values.put(WALL_COMMENT, model.comment);
            values.put(WALL_USERNAME, model.username);
            values.put(WALL_AVATAR, model.avatar);
            values.put(WALL_POST_TYPE, type.toString());
            values.put(WALL_POST_TIME, model.post_time);
            if (model.link != null) {
                values.put(WALL_POST_LINK, model.link);
            }
            helper.getWritableDatabase().insert(WALL_TABLE_NAME, null, values);

        }
    }

    public FanRantDataModel[] getWall() {
        FanRantDataModel[] results = new FanRantDataModel[0];
        FanRantDBOpenHelper helper = getORMLiteHelper();
        ConnectionSource connectionSource =
                new AndroidConnectionSource(helper);

        try {
            Dao<FanRantDataModel, Integer> dataModelDao = DaoManager.createDao(connectionSource, FanRantDataModel.class);
            QueryBuilder queryBuilder = dataModelDao.queryBuilder();
            queryBuilder.orderBy("post_time",false);
            List<FanRantDataModel> models = dataModelDao.query(queryBuilder.prepare());
            results = (FanRantDataModel[]) models.toArray(new FanRantDataModel[models.size()]);
        } catch (SQLException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } finally {
            connectionSource.closeQuietly();

        }
        return results;
    }


    public Cursor getLastInsert() {
        SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
        builder.setTables(WALL_TABLE_NAME);
        HashMap<String, String> cols = new HashMap<String, String>();
        cols.put(WALL_POST_ID, WALL_POST_ID);
        cols.put(WALL_POST_TIME, WALL_POST_TIME);
        builder.setProjectionMap(cols);
        builder.setDistinct(true);

        Cursor cursor = builder.query(helper.getReadableDatabase(), null, null, null, null, null, WALL_POST_TIME + " DESC ", "1");

        if (cursor == null) {
            return null;
        } else if (!cursor.moveToFirst()) {
            cursor.close();
            return null;
        }

        return cursor;
    }

    public void close() {
        helper.close();
    }

    public FanRantFixtureModel[] getFixtures() {
        FanRantFixtureModel[] models = null;
        FanRantDBOpenHelper helper = getORMLiteHelper();
        ConnectionSource connectionSource =
                new AndroidConnectionSource(helper);

        try {
            Dao<FanRantFixtureModel, String> fixtureModelDao = DaoManager.createDao(connectionSource, FanRantFixtureModel.class);
            Dao<FanRantPlayerModel, String> playersDAO = DaoManager.createDao(connectionSource, FanRantPlayerModel.class);
            Dao<FanRantOfficialsModel, String> officialsDAO = DaoManager.createDao(connectionSource, FanRantOfficialsModel.class);
            QueryBuilder<FanRantFixtureModel, String> fixturesQuery = fixtureModelDao.queryBuilder();
            fixturesQuery.join(playersDAO.queryBuilder());
            fixturesQuery.join(officialsDAO.queryBuilder());

            fixturesQuery.distinct();
            fixturesQuery.groupBy("id");
            PreparedQuery<FanRantFixtureModel> preparedQuery = fixturesQuery.prepare();

            List<FanRantFixtureModel> fixtureModels = fixtureModelDao.query(preparedQuery);

            models = (FanRantFixtureModel[]) fixtureModels.toArray(new FanRantFixtureModel[fixtureModels.size()]);

            System.out.println("models.length = " + models.length);
        } catch (SQLException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } finally {
            connectionSource.closeQuietly();
        }
        return models;
    }

    public void addFixtureData(FanRantFixtureModel[] models) {
        for (FanRantFixtureModel model : models) {
            ContentValues contentValues = new ContentValues();

            contentValues.put(FIXTURE_ID, model.getId());
            contentValues.put(FIXTURE_AWAY_TEAM_ID, model.getAway_team_id());
            contentValues.put(FIXTURE_AWAY_TEAM_NAME, model.getAway_team_name());
            contentValues.put(FIXTURE_HOME_TEAM_ID, model.getHome_team_id());
            contentValues.put(FIXTURE_HOME_TEAM_NAME, model.getHome_team_name());
            contentValues.put(FIXTURE_POSTPONED, model.getPostponed());
            contentValues.put(FIXTURE_SCORELINE, model.getScoreline());

        }
    }

    public FanRantGameUpdateModel[] getGameUpdatesForFixture(String fixid) {
        FanRantGameUpdateModel[] models = null;
        FanRantDBOpenHelper helper = getORMLiteHelper();
        ConnectionSource connectionSource =
                new AndroidConnectionSource(helper);

        try {
            Dao<FanRantGameUpdateModel, String> gameUpdateModelDao = DaoManager.createDao(connectionSource, FanRantGameUpdateModel.class);
            Dao<FanRantGameUpdatePlayerModel, String> playersDAO = DaoManager.createDao(connectionSource, FanRantGameUpdatePlayerModel.class);
            Dao<FanRantGameUpdateOfficialModel, String> officialsDAO = DaoManager.createDao(connectionSource, FanRantGameUpdateOfficialModel.class);
            QueryBuilder<FanRantGameUpdateModel, String> gameUpdatesQuery = gameUpdateModelDao.queryBuilder();
            gameUpdatesQuery.leftJoin(playersDAO.queryBuilder());
//            gameUpdatesQuery.leftJoin(officialsDAO.queryBuilder());
            gameUpdatesQuery.where().eq("fixture_id", fixid);
            gameUpdatesQuery.orderBy("post_time", false);
            PreparedQuery<FanRantGameUpdateModel> preparedQuery = gameUpdatesQuery.prepare();

            List<FanRantGameUpdateModel> updateModels = gameUpdateModelDao.query(preparedQuery);

            models = updateModels.toArray(new FanRantGameUpdateModel[updateModels.size()]);

        } catch (SQLException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } finally {
            connectionSource.closeQuietly();
        }
        return models;
    }

    public FanRantFixtureModel getFixture(String fixtureId) {
        FanRantDBOpenHelper helper = getORMLiteHelper();
        ConnectionSource connectionSource =
                new AndroidConnectionSource(helper);

        try {
            Dao<FanRantFixtureModel, String> fixtureModelDao = DaoManager.createDao(connectionSource, FanRantFixtureModel.class);
            Dao<FanRantPlayerModel, String> playersDAO = DaoManager.createDao(connectionSource, FanRantPlayerModel.class);
            Dao<FanRantOfficialsModel, String> officialsDAO = DaoManager.createDao(connectionSource, FanRantOfficialsModel.class);
            QueryBuilder<FanRantFixtureModel, String> fixturesQuery = fixtureModelDao.queryBuilder();
            fixturesQuery.join(playersDAO.queryBuilder());
            fixturesQuery.join(officialsDAO.queryBuilder());
            fixturesQuery.where().eq("id", fixtureId);
            PreparedQuery<FanRantFixtureModel> query = fixturesQuery.prepare();
            FanRantFixtureModel model = fixtureModelDao.query(query).get(0);
            return model;
        } catch (SQLException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } finally {
            connectionSource.closeQuietly();
        }
        return null;  //To change body of created methods use File | Settings | File Templates.
    }

    public void truncateWall() {
        FanRantDBOpenHelper helper = getORMLiteHelper();
        ConnectionSource connectionSource =
                new AndroidConnectionSource(helper);

        try {
            Dao<FanRantDataModel, Integer> dataModelDao = DaoManager.createDao(connectionSource, FanRantDataModel.class);
            dataModelDao.delete(dataModelDao.queryForAll());
        } catch (SQLException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } finally {
            connectionSource.closeQuietly();
        }
    }


    private static class FanRantOpenHelper extends SQLiteOpenHelper {

        private static final String TEAM_TABLE_CREATE = "CREATE TABLE " + TEAMS_TABLE_NAME + " (\n" +
                "  " + TEAMID + " integer  NOT NULL ,\n" +
                "  " + COMPETITION_ID + " integer NULL DEFAULT '0',\n" +
                "  " + COMPETITION_NAME + " varchar(255) NOT NULL,\n" +
                "  " + TEAMNAME + " varchar(50) DEFAULT '',\n" +
                "  " + RSS_FEED + " varchar(255),\n" +
                "  " + TWITTER_FEED + " varchar(255) DEFAULT NULL,\n" +
                "  " + FIXTURES_URL + " varchar(255) NULL);\n";


        private static final String HOME_WALL_TABLE_CREATE = "CREATE TABLE " + WALL_TABLE_NAME + " (\n" +
                "  " + WALL_POST_ID + " integer   NULL ,\n" +
                "  " + WALL_SUBJECT + " varchar(50) DEFAULT '',\n" +
                "  " + WALL_USERNAME + " varchar(255),\n" +
                "  " + WALL_AVATAR + " varchar(255),\n" +
                "  " + WALL_COMMENT + " varchar(255) DEFAULT NULL,\n" +
                "  " + WALL_POST_TYPE + " varchar(255) DEFAULT NULL,\n" +
                "  " + WALL_POST_LINK + " varchar(255) DEFAULT NULL,\n" +
                "  " + WALL_POST_TIME + " varchar(255) NULL);\n";


        private static final String LIVE_GAME_TABLE_CREATE = "CREATE TABLE " + LIVE_GAME_TABLE + " (\n" +
                "  " + GAME_FIXTURE_ID + " integer NOT NULL ,\n" +
                "  " + GAME_AVATAR + " varchar(255) DEFAULT NULL,\n" +
                "  " + GAME_COMMENT + " varchar(255) DEFAULT NULL,\n" +

                "  " + GAME_IMAGE + " varchar(255) DEFAULT NULL,\n" +
                "  " + GAME_MATCH_TIME + " varchar(255) DEFAULT NULL,\n" +
                "  " + GAME_PLAYERS + " varchar(255) DEFAULT NULL,\n" +
                "  " + GAME_POST_ID + " varchar(255) DEFAULT NULL,\n" +
                "  " + GAME_POST_TIME + " varchar(255) DEFAULT NULL,\n" +
                "  " + GAME_POST_TYPE + " varchar(255) DEFAULT NULL,\n" +
                "  " + GAME_RATING + " varchar(255) DEFAULT NULL,\n" +
                "  " + GAME_SUBJECT + " varchar(255) DEFAULT NULL,\n" +
                "  " + GAME_TEAM_ID + " varchar(255) DEFAULT NULL,\n" +
                "  " + GAME_USER_ID + " varchar(255) DEFAULT NULL,\n" +
                "  " + GAME_USER_NAME + " varchar(255) NULL);\n";


        private static final String PLAYER_TABLE_CREATE = "CREATE TABLE " + PLAYER_TABLE + " (\n" +
                "  " + PLAYER_ID + " integer NOT NULL ,\n" +
                "  " + PLAYER_FIRST_NAME + " varchar(255) DEFAULT NULL,\n" +
                "  " + PLAYER_LAST_NAME + " varchar(255) DEFAULT NULL,\n" +
                "  " + PLAYER_NICKNAME + " varchar(255) DEFAULT NULL,\n" +
                "  " + PLAYER_NUMBER + " varchar(255) DEFAULT NULL,\n" +
                "  " + PLAYER_POSITION + " varchar(255) DEFAULT NULL,\n" +
                "  " + PLAYER_TEAM_ID + " varchar(255) DEFAULT NULL,\n" +
                "  " + PLAYER_TEAM_NAME + " varchar(255) NULL);\n";


        private static final String FIXTURE_TABLE_CREATE = "CREATE TABLE " + FIXTURE_TABLE + " (\n" +
                "  " + FIXTURE_AWAY_TEAM_ID + " integer NOT NULL ,\n" +
                "  " + FIXTURE_AWAY_TEAM_NAME + " varchar(255) DEFAULT NULL,\n" +
                "  " + FIXTURE_AWAY_TEAM_PLAYERS + " varchar(255) DEFAULT NULL,\n" +
                "  " + FIXTURE_COMPETITION_ID + " varchar(255) DEFAULT NULL,\n" +
                "  " + FIXTURE_COMPETITION_NAME + " varchar(255) DEFAULT NULL,\n" +
                "  " + FIXTURE_GROUND_ID + " varchar(255) DEFAULT NULL,\n" +
                "  " + FIXTURE_GROUND_LATITUDE + " varchar(255) DEFAULT NULL,\n" +
                "  " + FIXTURE_GROUND_LONGITUDE + " varchar(255) DEFAULT NULL,\n" +
                "  " + FIXTURE_GROUND_NAME + " varchar(255) DEFAULT NULL,\n" +
                "  " + FIXTURE_HOME_TEAM_ID + " varchar(255) DEFAULT NULL,\n" +
                "  " + FIXTURE_POSTPONED + " varchar(255) DEFAULT NULL,\n" +
                "  " + FIXTURE_HOME_TEAM_NAME + " varchar(255) DEFAULT NULL,\n" +
                "  " + FIXTURE_HOME_TEAM_PLAYERS + " varchar(255) DEFAULT NULL,\n" +
                "  " + FIXTURE_ID + " INTEGER NOT NULL,\n" +
                "  " + FIXTURE_KICKOFF + " varchar(255) DEFAULT NULL,\n" +
                "  " + FIXTURE_LIVE + " varchar(255) DEFAULT NULL,\n" +
                "  " + FIXTURE_MATCHTIME + " varchar(255) DEFAULT NULL,\n" +
                "  " + FIXTURE_OFFICIALS + " varchar(255) DEFAULT NULL,\n" +
                "  " + FIXTURE_SCORELINE + " varchar(255) NULL);\n";


        private static final String TEAMSHEET_TABLE_CREATE = "CREATE TABLE " + TEAMSHEET_TABLE + " (\n" +
                "  " + TEAM_SHEET_FIX_ID + " integer NOT NULL ,\n" +
                "  " + TEAM_SHEET_PLAYER + " varchar(255) DEFAULT NULL,\n" +
                "  " + TEAM_SHEET_TEAM + " varchar(255) DEFAULT NULL);";

        private static final String OFFICIAL_TABLE_CREATE = "CREATE TABLE " + OFFICIAL_TABLE + " (\n" +
                "  " + OFF_ID + " integer NOT NULL ,\n" +
                "  " + OFF_HOME_TOWN + " varchar(255) DEFAULT NULL,\n" +
                "  " + OFF_FIRST_NAME + " varchar(255) DEFAULT NULL,\n" +
                "  " + OFF_LAST_NAME + " varchar(255) DEFAULT NULL,\n" +
                "  " + OFF_POSITION + " varchar(255) DEFAULT NULL);\n";

        private static final String GAME_OFFICIAL_TABLE_CREATE = "CREATE TABLE " + GAME_OFFICIAL_TABLE + " (\n" +
                "  " + OFF_FIXID + " integer NOT NULL ,\n" +
                "  " + OFF_OFFICIAL_ID + " integer NOT NULL);\n";


        private SQLiteDatabase mDatabase;

        FanRantOpenHelper(Context context) {
            super(context, TEAMS_TABLE_NAME, null, DATABASE_VERSION);
        }


        @Override
        public void onCreate(SQLiteDatabase sqLiteDatabase) {
            mDatabase = sqLiteDatabase;
            mDatabase.execSQL(TEAM_TABLE_CREATE);
            mDatabase.execSQL(HOME_WALL_TABLE_CREATE);
            mDatabase.execSQL(LIVE_GAME_TABLE_CREATE);
            mDatabase.execSQL(FIXTURE_TABLE_CREATE);
            mDatabase.execSQL(PLAYER_TABLE_CREATE);
            mDatabase.execSQL(TEAMSHEET_TABLE_CREATE);
            mDatabase.execSQL(OFFICIAL_TABLE_CREATE);
            mDatabase.execSQL(GAME_OFFICIAL_TABLE_CREATE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + TEAMS_TABLE_NAME);
            db.execSQL("DROP TABLE IF EXISTS " + HOME_WALL_TABLE_CREATE);
            onCreate(db);

        }
    }


    private FanRantDBOpenHelper getORMLiteHelper() {

        DatabaseManager<FanRantDBOpenHelper> manager = new DatabaseManager<FanRantDBOpenHelper>();
        return manager.getHelper(mContext);
    }
}
