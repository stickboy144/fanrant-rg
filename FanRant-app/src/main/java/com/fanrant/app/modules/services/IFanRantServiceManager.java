package com.fanrant.app.modules.services;

import android.os.Bundle;
import android.os.ResultReceiver;

/**
 * Created with IntelliJ IDEA.
 * User: anthonygrimmitt
 * Date: 11/05/2013
 * Time: 13:11
 * To change this template use File | Settings | File Templates.
 */
public interface IFanRantServiceManager {
    IFanRantServiceManager addService(FanRantService service, Class clazz, ResultReceiver resultReceiver);

    IFanRantServiceManager addService(FanRantService service, Class clazz, ResultReceiver resultReceiver, Bundle b);

    void startService(FanRantService service);

    void restartService(FanRantService service);

    void stopService(FanRantService service);

    void stopAllServices();

    void addAndStartService(FanRantService service, Class clazz, ResultReceiver resultReceiver, Bundle b);
}
