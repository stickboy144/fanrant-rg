package com.fanrant.app.modules.api.responders;

import com.fanrant.app.model.FanRantCommentModel;
import com.fanrant.app.model.FanRantModel;
import com.fanrant.app.modules.api.FanRantAPIResponse;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Created with IntelliJ IDEA.
 * User: anthonygrimmitt
 * Date: 13/04/2013
 * Time: 11:25
 * To change this template use File | Settings | File Templates.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class  FanRantPostCommentResponse extends FanRantAPIResponse {
    @JsonProperty("results")
    private FanRantCommentModel result;
    @Override
    public FanRantCommentModel[] getResults() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public FanRantCommentModel getResult() {
        return result;
    }
}

