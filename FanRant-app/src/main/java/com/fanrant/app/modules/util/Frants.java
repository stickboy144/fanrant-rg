package com.fanrant.app.modules.util;

/**
 * Created with IntelliJ IDEA.
 * User: Antwork
 * Date: 08/12/2012
 * Time: 14:58
 * To change this template use File | Settings | File Templates.
 */
public enum Frants {
    COMMENT,
    GOAL,
    PENALTY,
    DIVE,
    ONFIRE,
    WHATADONKEY,
    PLAYACTOR,
    CHOPPER,
    OFFSIDE,
    THEREFSBLIND,
    YELLOWCARD,
    REDCARD,
    CANCELLED;

    public static Frants fromOrd(int i) {
        if (i < 0 || i >= Frants.values().length) {
            throw new IndexOutOfBoundsException("Invalid ordinal");
        }
        return Frants.values()[i];
    }

    public static Frants fromString(String frant) {
        frant = frant.toUpperCase().trim().replaceAll(" ","").replaceAll("'","");


        for (Frants frants : Frants.values()) {
            if(frants.toString().equals(frant)) {
                return frants;
            }
        }

        return null;
    }
}
