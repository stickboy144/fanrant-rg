package com.fanrant.app.modules.api;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import com.fanrant.R;
import com.fanrant.app.model.FanRantModel;
import com.fanrant.app.model.FanRantWallStatusModel;
import com.fanrant.app.modules.api.client.IDataClient;
import com.fanrant.app.modules.api.responders.FanRantWallStatusResponse;
import com.fanrant.app.modules.util.ApplicationMessages;
import com.google.inject.Inject;
import roboguice.util.RoboAsyncTask;

import java.util.Arrays;

//import com.fanrant.old.FanRantApplication;

/**
 * Created with IntelliJ IDEA.
 * User: Antwork
 * Date: 28/10/2012
 * Time: 20:42
 * To change this template use File | Settings | File Templates.
 */
public class FanRantAsyncTask<T> extends RoboAsyncTask<FanRantModel[]> {
    @Inject
    IDataClient IDataClient;


    private FanRantAPIParams params;

    public FanRantAsyncTask(Context context, FanRantAPIParams params, Handler handler) {
        super(context, handler);

        this.params = params;

    }

    @Override
    protected void onPreExecute() throws Exception {
        super.onPreExecute();
    }


    @Override
    public FanRantModel[] call() throws Exception {
        IFanRantAPIResponse result = IDataClient.post(params);

        if (result != null) {
            if (result.getCode() == 200) {
                /**
                 * hack for when the api returns an item instead of an array of items
                  */
                if(params.getResponseClass() == FanRantWallStatusResponse.class) {
                    FanRantWallStatusResponse response = (FanRantWallStatusResponse) result;
                    return (FanRantWallStatusModel[])Arrays.asList(response.getResult()).toArray(new FanRantWallStatusModel[1]);
                }
                return result.getResults();

            } else {
                throw new Exception(result.getMessage());
            }
        } else {
            throw new Exception("No Network");
        }
    }

    @Override
    protected void onException(Exception e) throws RuntimeException {
        Message message = handler.obtainMessage(ApplicationMessages.ASYNCTASKFAILED.ordinal(), e);
        handler.sendMessage(message);
//        super.onException(e);
    }

    @Override
    protected void onSuccess(FanRantModel[] model) throws Exception {
            Message message = handler().obtainMessage(ApplicationMessages.ASYNCTASKSUCESS.ordinal(), model);
            handler.sendMessage(message);
        super.onSuccess(model);
    }

//    @Override
//    protected void onSuccess(FanRantUserModel fanRantUserModel) throws Exception {
//       Ln.d(fanRantUserModel);
//        super.onSuccess(fanRantUserModel);
//    }
}
