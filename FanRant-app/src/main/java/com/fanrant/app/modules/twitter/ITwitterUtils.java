package com.fanrant.app.modules.twitter;

import android.app.Activity;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import oauth.signpost.commonshttp.CommonsHttpOAuthConsumer;
import oauth.signpost.commonshttp.CommonsHttpOAuthProvider;
//import com.google.android.maps.GeoPoint;

/**
 * Created with IntelliJ IDEA.
 * User: anthonygrimmitt
 * Date: 13/04/2013
 * Time: 18:02
 * To change this template use File | Settings | File Templates.
 */
public interface ITwitterUtils {

    void authenticate(Handler handler);

    boolean isTwitterPrefsSet();

    void sendTweet(String msg, Handler handler) throws Exception;

    void loginAndAuthenicate(final Handler handler, AsyncTask task);

    String getAuthCallback();

    void retrieveAccessToken(Handler handler, Uri uri);

    CommonsHttpOAuthConsumer getConsumer();

    CommonsHttpOAuthProvider getProvider();
}
