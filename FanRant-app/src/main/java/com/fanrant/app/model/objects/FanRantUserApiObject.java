package com.fanrant.app.model.objects;

import com.fanrant.app.model.FanRantUserModel;
import org.codehaus.jackson.map.annotate.JsonDeserialize;

/**
 * Created with IntelliJ IDEA.
 * User: Antwork
 * Date: 21/10/2012
 * Time: 15:58
 * To change this template use File | Settings | File Templates.
 */
@JsonDeserialize(as = FanRantUserModel.class)
public abstract class FanRantUserApiObject {
}
