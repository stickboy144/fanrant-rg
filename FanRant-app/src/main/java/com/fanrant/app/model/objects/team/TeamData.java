
package com.fanrant.app.model.objects.team;

public class TeamData {
   	private String competition_id;
   	private String team;
   	private String team_fixtures_url;
   	private String team_id;
   	private String team_rss_feed;
   	private String team_twitter_feed;

 	public String getCompetition_id(){
		return this.competition_id;
	}
	public void setCompetition_id(String competition_id){
		this.competition_id = competition_id;
	}
 	public String getTeam(){
		return this.team;
	}
	public void setTeam(String team){
		this.team = team;
	}
 	public String getTeam_fixtures_url(){
		return this.team_fixtures_url;
	}
	public void setTeam_fixtures_url(String team_fixtures_url){
		this.team_fixtures_url = team_fixtures_url;
	}
 	public String getTeam_id(){
		return this.team_id;
	}
	public void setTeam_id(String team_id){
		this.team_id = team_id;
	}
 	public String getTeam_rss_feed(){
		return this.team_rss_feed;
	}
	public void setTeam_rss_feed(String team_rss_feed){
		this.team_rss_feed = team_rss_feed;
	}
 	public String getTeam_twitter_feed(){
		return this.team_twitter_feed;
	}
	public void setTeam_twitter_feed(String team_twitter_feed){
		this.team_twitter_feed = team_twitter_feed;
	}
}
