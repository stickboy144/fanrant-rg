package com.fanrant.app.model;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Created with IntelliJ IDEA.
 * User: anthonygrimmitt
 * Date: 13/04/2013
 * Time: 11:26
 * To change this template use File | Settings | File Templates.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class FanRantCommentModel extends FanRantModel {

    @JsonProperty
    private Fanrant fanrant;

    public Fanrant getFanrant() {
        return fanrant;
    }

    public void setFanrant(Fanrant fanrant) {
        this.fanrant = fanrant;
    }

}
