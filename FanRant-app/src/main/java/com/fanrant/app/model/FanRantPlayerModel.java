
package com.fanrant.app.model;

import com.fanrant.app.model.FanRantFixtureModel;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@DatabaseTable(tableName = "players")
@JsonIgnoreProperties(ignoreUnknown = true)
public class FanRantPlayerModel {
    @DatabaseField(id = true)
    private String id;
    @DatabaseField
    private String first_name;
    @DatabaseField
   	private String last_name;
    @DatabaseField
   	private String nickname;
    @DatabaseField
   	private String number;
    @DatabaseField
   	private String position;
    @DatabaseField
    private String team_id;
    @DatabaseField
    private String team_name;
    @DatabaseField(canBeNull = false, foreign = true)
    private FanRantFixtureModel fixtureModel;
    @DatabaseField(canBeNull = true, foreign = true)
    private FanRantGameUpdateModel gameUpdateModel;
    public String getTeam_name() {
        return team_name;
    }

    public void setTeam_name(String team_name) {
        this.team_name = team_name;
    }

    private String substitute;

    public String getTeam_id() {
        return team_id;
    }

    public void setTeam_id(String team_id) {
        this.team_id = team_id;
    }

    public String getFirst_name(){
		return this.first_name;
	}
	public void setFirst_name(String first_name){
		this.first_name = first_name;
	}
 	public String getId(){
		return this.id;
	}
	public void setId(String id){
		this.id = id;
	}
 	public String getLast_name(){
		return this.last_name;
	}
	public void setLast_name(String last_name){
		this.last_name = last_name;
	}
 	public String getNickname(){
		return this.nickname;
	}
	public void setNickname(String nickname){
		this.nickname = nickname;
	}
 	public String getNumber(){
		return this.number;
	}
	public void setNumber(String number){
		this.number = number;
	}
 	public String getPosition(){
		return this.position;
	}
	public void setPosition(String position){
		this.position = position;
	}
 	public String getSubstitute(){
		return this.substitute;
	}
	public void setSubstitute(String substitute){
		this.substitute = substitute;
	}

    public FanRantFixtureModel getFixtureModel() {
        return fixtureModel;
    }

    public void setFixtureModel(FanRantFixtureModel fixtureModel) {
        this.fixtureModel = fixtureModel;
    }

    public FanRantGameUpdateModel getGameUpdateModel() {
        return gameUpdateModel;
    }

    public void setGameUpdateModel(FanRantGameUpdateModel gameUpdateModel) {
        this.gameUpdateModel = gameUpdateModel;
    }
}

