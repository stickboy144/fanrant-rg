package com.fanrant.app.model.objects.fixture;

import com.fanrant.app.model.FanRantFixtureModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Antwork
 * Date: 24/11/2012
 * Time: 18:55
 * To change this template use File | Settings | File Templates.
 */
public class Division {
    private int id;
    private String name;
    private List<FanRantFixtureModel> games;

    public Division() {
    }

    public Division(int id, String name) {
        this.id = id;
        this.name = name;
        games = new ArrayList<FanRantFixtureModel>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<FanRantFixtureModel> getGames() {
        return games;
    }
}
