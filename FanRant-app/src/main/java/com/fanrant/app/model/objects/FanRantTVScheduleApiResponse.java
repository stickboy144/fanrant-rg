
package com.fanrant.app.model.objects;

import com.fanrant.app.model.FanRantTVShowModel;

import java.util.List;

public class FanRantTVScheduleApiResponse {
   	private Number code;
   	private String message;
   	private List<FanRantTVShowModel> results;
   	private Number time;

 	public Number getCode(){
		return this.code;
	}
	public void setCode(Number code){
		this.code = code;
	}
 	public String getMessage(){
		return this.message;
	}
	public void setMessage(String message){
		this.message = message;
	}
 	public List<FanRantTVShowModel> getResults(){
		return this.results;
	}
	public void setResults(List<FanRantTVShowModel> results){
		this.results = results;
	}
 	public Number getTime(){
		return this.time;
	}
	public void setTime(Number time){
		this.time = time;
	}
}
