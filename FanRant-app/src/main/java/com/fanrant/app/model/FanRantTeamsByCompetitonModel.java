
package com.fanrant.app.model;

import java.util.List;

public class FanRantTeamsByCompetitonModel extends FanRantModel {
   	private String competition_name;
    private String competition_id;
    private String type;
   	private List<TeamModel> teams;

 	public String getCompetition_name(){
		return this.competition_name;
	}
	public void setCompetition_name(String competition_name){
		this.competition_name = competition_name;
	}
 	public List<TeamModel> getTeams(){
		return this.teams;
	}
	public void setTeams(List<TeamModel> teams){
		this.teams = teams;
	}

    public String getCompetition_id() {
        return competition_id;
    }

    public void setCompetition_id(String competition_id) {
        this.competition_id = competition_id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
