package com.fanrant.app.model.objects;

import com.fanrant.app.model.FanRantTVShowModel;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ShowDay {
    private Date start;
    private List<FanRantTVShowModel> shows = new ArrayList<FanRantTVShowModel>();

    public ShowDay(Date start) {
        this.start = start;
    }

    public List<FanRantTVShowModel> getShows() {
        return shows;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }
}
