
package com.fanrant.app.model.objects;

import com.fanrant.app.model.objects.team.TeamData;

import java.util.List;

public class FanRantTeamDataApiResponse {
   	private Number code;
   	private String message;
   	private List<TeamData> results;
   	private Number time;

 	public Number getCode(){
		return this.code;
	}
	public void setCode(Number code){
		this.code = code;
	}
 	public String getMessage(){
		return this.message;
	}
	public void setMessage(String message){
		this.message = message;
	}
 	public List<TeamData> getResults(){
		return this.results;
	}
	public void setResults(List<TeamData> results){
		this.results = results;
	}
 	public Number getTime(){
		return this.time;
	}
	public void setTime(Number time){
		this.time = time;
	}
}
