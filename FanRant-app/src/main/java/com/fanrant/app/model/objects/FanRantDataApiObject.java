package com.fanrant.app.model.objects;

import com.fanrant.app.model.FanRantDataModel;
import org.codehaus.jackson.map.annotate.JsonDeserialize;

/**
 * Created with IntelliJ IDEA.
 * User: Antwork
 * Date: 04/11/2012
 * Time: 15:04
 * To change this template use File | Settings | File Templates.
 */
@JsonDeserialize(as = FanRantDataModel.class)
public abstract class FanRantDataApiObject {
}
