
package com.fanrant.app.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.util.Collection;
@JsonIgnoreProperties(ignoreUnknown = true)
@DatabaseTable(tableName = "fixtures")
public class FanRantFixtureModel extends FanRantModel {
    @DatabaseField
   	private String away_team_id;
    @DatabaseField
    private String away_team_name;

    @ForeignCollectionField(eager = false)
   	private Collection<FanRantPlayerModel> away_team_players;
    @DatabaseField
    private String competition_id;
    @DatabaseField
   	private String competition_name;
    @DatabaseField
   	private String ground_id;
    @DatabaseField
   	private String ground_latitude;
    @DatabaseField
   	private String ground_longitude;
    @DatabaseField
   	private String ground_name;
    @DatabaseField
    private String home_team_id;
    @DatabaseField
    private String postponed;
    @DatabaseField
   	private String home_team_name;

    @ForeignCollectionField(eager = false)
    private Collection<FanRantPlayerModel> home_team_players;
    @DatabaseField(id = true)
   	private String id;
    @DatabaseField
   	private String kickoff;
    @DatabaseField
   	private String live;
    @DatabaseField
   	private String matchtime;
    @ForeignCollectionField(eager = false)
    private Collection<FanRantOfficialsModel> officials;
    @DatabaseField
   	private String scoreline;

 	public String getAway_team_id(){
		return this.away_team_id;
	}
	public void setAway_team_id(String away_team_id){
		this.away_team_id = away_team_id;
	}
 	public String getAway_team_name(){
		return this.away_team_name;
	}
	public void setAway_team_name(String away_team_name){
		this.away_team_name = away_team_name;
	}
	public Collection getAway_team_players(){
		return this.away_team_players;
	}
	public void setAway_team_players(Collection<FanRantPlayerModel> away_team_players){
		this.away_team_players = away_team_players;
	}

    public String getCompetition_id(){
		return this.competition_id;
	}
	public void setCompetition_id(String competition_id){
		this.competition_id = competition_id;
	}
 	public String getCompetition_name(){
		return this.competition_name;
	}
	public void setCompetition_name(String competition_name){
		this.competition_name = competition_name;
	}
 	public String getGround_id(){
		return this.ground_id;
	}
	public void setGround_id(String ground_id){
		this.ground_id = ground_id;
	}
 	public String getGround_latitude(){
		return this.ground_latitude;
	}
	public void setGround_latitude(String ground_latitude){
		this.ground_latitude = ground_latitude;
	}
 	public String getGround_longitude(){
		return this.ground_longitude;
	}
	public void setGround_longitude(String ground_longitude){
		this.ground_longitude = ground_longitude;
	}
 	public String getGround_name(){
		return this.ground_name;
	}
	public void setGround_nam0e(String ground_name){
		this.ground_name = ground_name;
	}
 	public String getHome_team_id(){
		return this.home_team_id;
	}
	public void setHome_team_id(String home_team_id){
		this.home_team_id = home_team_id;
	}
 	public String getHome_team_name(){
		return this.home_team_name;
	}
	public void setHome_team_name(String home_team_name){
		this.home_team_name = home_team_name;
	}
 	public Collection getHome_team_players(){
		return this.home_team_players;
	}
	public void setHome_team_players(Collection<FanRantPlayerModel> home_team_players){
		this.home_team_players = home_team_players;
	}
 	public String getId(){
		return this.id;
	}
	public void setId(String id){
		this.id = id;
	}
 	public String getKickoff(){
		return this.kickoff;
	}
	public void setKickoff(String kickoff){
		this.kickoff = kickoff;
	}
 	public String getLive(){
		return this.live;
	}
	public void setLive(String live){
		this.live = live;
	}
 	public String getMatchtime(){
		return this.matchtime;
	}
	public void setMatchtime(String matchtime){
		this.matchtime = matchtime;
	}
	public Collection getOfficials(){
		return this.officials;
	}
	public void setOfficials(Collection<FanRantOfficialsModel> officials){
		this.officials = officials;
	}
 	public String getScoreline(){
		return this.scoreline;
	}
	public void setScoreline(String scoreline){
		this.scoreline = scoreline;
	}

    public String getPostponed() {
        return postponed;
    }

    public void setPostponed(String postponed) {
        this.postponed = postponed;
    }
}
