package com.fanrant.app.model.objects;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Created with IntelliJ IDEA.
 * User: Antwork
 * Date: 28/10/2012
 * Time: 21:49
 * To change this template use File | Settings | File Templates.
 */
public class FanRantUser extends FanRantUserApiObject {
    @JsonProperty
    private int id;
    @JsonProperty
    private String username;
    @JsonProperty
    private String email_address;
    @JsonProperty
    private String dob;

    @JsonProperty
    private String avatar;
    @JsonProperty
    private String gender;
    @JsonProperty
    private int phone;
    @JsonProperty
    private String city;
    @JsonProperty
    private String postcode;
    @JsonProperty
    private long facebookId;
    @JsonProperty
    private String twitterId;
    @JsonProperty
    private int team_id;
    @JsonProperty
    private String national_team;
    @JsonProperty
    private boolean receiveEmails;
    @JsonProperty
    private String signup;
    @JsonProperty
    private int compId;
    @JsonProperty
    private String compName;
    @JsonProperty
    private String team_rss_feed;
    @JsonProperty
    private String team_twitter_feed;
    @JsonProperty
    private String team_fixtures_url;
    @JsonProperty
    private int rating;

    public FanRantUser() {
    }

    public FanRantUser(int id, String userName, String email, String dob, String avatar, String gender, int phone, String city, String postcode, long facebookId, String twitterId, int teamId, String national_team, boolean receiveEmails, String signupDate, int compId, String compName, String teamRss, String teamTwitterRss, String teamFixRss, int rating) {
        this.id = id;
        this.username = userName;
        this.email_address = email;
        this.dob = dob;
        this.avatar = avatar;
        this.gender = gender;
        this.phone = phone;
        this.city = city;
        this.postcode = postcode;
        this.facebookId = facebookId;
        this.twitterId = twitterId;
        this.team_id = teamId;
        this.national_team = national_team;
        this.receiveEmails = receiveEmails;
        this.signup = signupDate;
        this.compId = compId;
        this.compName = compName;
        this.team_rss_feed = teamRss;
        this.team_twitter_feed = teamTwitterRss;
        this.team_fixtures_url = teamFixRss;
        this.rating = rating;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail_address() {
        return email_address;
    }

    public void setEmail_address(String email_address) {
        this.email_address = email_address;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public int getPhone() {
        return phone;
    }

    public void setPhone(int phone) {
        this.phone = phone;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public long getFacebookId() {
        return facebookId;
    }

    public void setFacebookId(long facebookId) {
        this.facebookId = facebookId;
    }

    public String getTwitterId() {
        return twitterId;
    }

    public void setTwitterId(String twitterId) {
        this.twitterId = twitterId;
    }

    public int getTeam_id() {
        return team_id;
    }

    public void setTeam_id(int team_id) {
        this.team_id = team_id;
    }

    public String getNational_team() {
        return national_team;
    }

    public void setNational_team(String national_team) {
        this.national_team = national_team;
    }

    public boolean isReceiveEmails() {
        return receiveEmails;
    }

    public void setReceiveEmails(boolean receiveEmails) {
        this.receiveEmails = receiveEmails;
    }

    public String getSignup() {
        return signup;
    }

    public void setSignup(String signup) {
        this.signup = signup;
    }

    public int getCompId() {
        return compId;
    }

    public void setCompId(int compId) {
        this.compId = compId;
    }

    public String getCompName() {
        return compName;
    }

    public void setCompName(String compName) {
        this.compName = compName;
    }

    public String getTeam_rss_feed() {
        return team_rss_feed;
    }

    public void setTeam_rss_feed(String team_rss_feed) {
        this.team_rss_feed = team_rss_feed;
    }

    public String getTeam_twitter_feed() {
        return team_twitter_feed;
    }

    public void setTeam_twitter_feed(String team_twitter_feed) {
        this.team_twitter_feed = team_twitter_feed;
    }

    public String getTeam_fixtures_url() {
        return team_fixtures_url;
    }

    public void setTeam_fixtures_url(String team_fixtures_url) {
        this.team_fixtures_url = team_fixtures_url;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    @Override
    public String toString() {
        return "FanRantUser{" +
                "id=" + id +
                ",\n username='" + username + '\'' +
                ",\n email_address='" + email_address + '\'' +
                ",\n dob='" + dob + '\'' +
                ",\n phone=" + phone +
                ",\n city='" + city + '\'' +
                ",\n postcode='" + postcode + '\'' +
                ",\n facebookId=" + facebookId +
                ",\n twitterId='" + twitterId + '\'' +
                ",\n team_id=" + team_id +
                ",\n national_team=" + national_team +
                ",\n receiveEmails=" + receiveEmails +
                ",\n signup='" + signup + '\'' +
                ",\n compId=" + compId +
                ",\n compName='" + compName + '\'' +
                ",\n team_rss_feed='" + team_rss_feed + '\'' +
                ",\n team_twitter_feed='" + team_twitter_feed + '\'' +
                ",\n team_fixtures_url='" + team_fixtures_url + '\'' +
                ",\n rating=" + rating +
                '}';
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
}

