package com.fanrant.app.model;

import android.content.Context;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Created with IntelliJ IDEA.
 * User: Ant Grimmitt
 * Date: 23/03/13
 *
 * FanRantUserModel
 * ----------------
 * Holds all the user info for the app
 * Also used in unregistered model if registered == false
 *
 * Populated using Spring Mapping hence the @JsonProperty
 *
 * This is a singleton instance used throughout the app
 */
@JsonIgnoreProperties(ignoreUnknown = true) //  ignore props with no api from the api
@Singleton
public class FanRantUserModel extends FanRantModel implements IFanRantUserModel {
    @JsonProperty("team")
    public String teamName;

    @Inject
    public FanRantUserModel(Provider<Context> contextProvider) {
        super(contextProvider);
    }

    public FanRantUserModel() {
    }

    private boolean registered;
    public String facebookAccessToken;

    private boolean loggedIn = false;
    @JsonProperty
    public String username = "";
    @JsonProperty
    public String email_address = "";
    @JsonProperty
    public String password = "";

    @JsonProperty
    public String avatar = "";
    @JsonProperty
    public long facebookId =0;
    @JsonProperty
    public String twitterId = "";
    public int team_id =0;
    @JsonProperty
    public String national_team = "";
    @JsonProperty
    public String signup = "";
    @JsonProperty
    public String competition_id = "";
    @JsonProperty
    public String competition_name = "";
    @JsonProperty
    public String team_rss_feed = "";
    @JsonProperty
    public String team_twitter_feed = "";
    @JsonProperty
    public String team_fixtures_url = "";

    @Override
    public String getAvatar() {
        return avatar;
    }

    @Override
    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    @Override
    public String getCompetition_id() {
        return competition_id;
    }

    @Override
    public void setCompetition_id(String competition_id) {
        this.competition_id = competition_id;
    }

    @Override
    public String getCompetition_name() {
        return competition_name;
    }

    @Override
    public void setCompetition_name(String competition_name) {
        this.competition_name = competition_name;
    }

    @Override
    public String getEmail_address() {
        return email_address;
    }

    @Override
    public void setEmail_address(String email_address) {
        this.email_address = email_address;
    }

    @Override
    public String getFacebookAccessToken() {
        return facebookAccessToken;
    }

    @Override
    public void setFacebookAccessToken(String facebookAccessToken) {
        this.facebookAccessToken = facebookAccessToken;
    }

    @Override
    public long getFacebookId() {
        return facebookId;
    }

    @Override
    public void setFacebookId(long facebookId) {
        this.facebookId = facebookId;
    }

    @Override
    public String getNational_team() {
        return national_team;
    }

    @Override
    public void setNational_team(String national_team) {
        this.national_team = national_team;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean isRegistered() {
        return registered;
    }

    @Override
    public void setRegistered(boolean registered) {
        this.registered = registered;
    }

    @Override
    public String getSignup() {
        return signup;
    }

    @Override
    public void setSignup(String signup) {
        this.signup = signup;
    }

    @Override
    public String getTeam_fixtures_url() {
        return team_fixtures_url;
    }

    @Override
    public void setTeam_fixtures_url(String team_fixtures_url) {
        this.team_fixtures_url = team_fixtures_url;
    }

    @Override
    public int getTeam_id() {
        return team_id;
    }

    @Override
    public void setTeam_id(int team_id) {
        this.team_id = team_id;
    }

    @Override
    public String getTeam_rss_feed() {
        return team_rss_feed;
    }

    @Override
    public void setTeam_rss_feed(String team_rss_feed) {
        this.team_rss_feed = team_rss_feed;
    }

    @Override
    public String getTeam_twitter_feed() {
        return team_twitter_feed;
    }

    @Override
    public void setTeam_twitter_feed(String team_twitter_feed) {
        this.team_twitter_feed = team_twitter_feed;
    }

    @Override
    public String getTwitterId() {
        return twitterId;
    }

    @Override
    public void setTwitterId(String twitterId) {
        this.twitterId = twitterId;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public boolean isLoggedIn() {
        return loggedIn;
    }

    public void setLoggedIn(boolean loggedIn) {
        this.loggedIn = loggedIn;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    @Override
    public String toString() {
        return "FanRantUserModel{" +
                ",\n username='" + username + '\'' +
                ",\n email_address='" + email_address + '\'' +
                ",\n facebookId=" + facebookId +
                ",\n twitterId='" + twitterId + '\'' +
                ",\n team_id=" + team_id +
                ",\n national_team=" + national_team +
                ",\n signup='" + signup + '\'' +
                ",\n compId=" + competition_id +
                ",\n compName='" + competition_name + '\'' +
                ",\n team_rss_feed='" + team_rss_feed + '\'' +
                ",\n team_twitter_feed='" + team_twitter_feed + '\'' +
                ",\n team_fixtures_url='" + team_fixtures_url + '\'' +
                '}';
    }

    public void populate(FanRantUserModel userModel) {
        setUsername(userModel.getUsername());
        setPassword(userModel.getPassword());
        setAvatar(userModel.getAvatar());
        setTeam_fixtures_url(userModel.getTeam_fixtures_url());
        setCompetition_id(userModel.getCompetition_id());
        setTeam_twitter_feed(userModel.getTeam_twitter_feed());
        setTeam_rss_feed(userModel.getTeam_rss_feed());
        setEmail_address(userModel.getEmail_address());
        setTeam_id(userModel.getTeam_id());
        setTeamName(userModel.getTeamName());

    }
}
