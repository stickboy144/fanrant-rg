
package com.fanrant.app.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.List;
@JsonIgnoreProperties(ignoreUnknown = true)
@DatabaseTable(tableName = "gameupdates")
public class FanRantGameUpdateModel extends FanRantModel implements Comparable<FanRantGameUpdateModel> {
    @DatabaseField
    @JsonProperty
   	private String avatar;
    @DatabaseField
    @JsonProperty
   	private String comment;
    @DatabaseField
    @JsonProperty
   	private String fixture_id;
    @DatabaseField
    @JsonProperty
   	private String image;
    @DatabaseField
    @JsonProperty
   	private String match_time;
    @ForeignCollectionField(eager = false)
   	private Collection<FanRantGameUpdatePlayerModel> players;
    @ForeignCollectionField(eager = false)
   	private Collection<FanRantGameUpdateOfficialModel> officials;
    @JsonProperty
    @DatabaseField(id = true)
   	private String post_id;
    @JsonProperty
    @DatabaseField
   	private String post_time;
    @JsonProperty
    @DatabaseField
   	private String post_type;
    @JsonProperty
    @DatabaseField
   	private String rating;
    @JsonProperty
    @DatabaseField
   	private String subject;
    @JsonProperty
    @DatabaseField
   	private String team_id;
    @JsonProperty
    @DatabaseField
   	private String user_id;
    @JsonProperty
    @DatabaseField
   	private String user_name;
    @JsonProperty
    @DatabaseField
    private String username;

 	public String getAvatar(){
		return this.avatar;
	}
	public void setAvatar(String avatar){
		this.avatar = avatar;
	}
 	public String getComment(){
		return this.comment;
	}
	public void setComment(String comment){
		this.comment = comment;
	}
 	public String getFixture_id(){
		return this.fixture_id;
	}
	public void setFixture_id(String fixture_id){
		this.fixture_id = fixture_id;
	}
 	public String getImage(){
		return this.image;
	}
	public void setImage(String image){
		this.image = image;
	}
 	public String getMatch_time(){
		return this.match_time;
	}
	public void setMatch_time(String match_time){
		this.match_time = match_time;
	}
 	public Collection<FanRantGameUpdatePlayerModel> getPlayers(){
		return this.players;
	}
	public void setPlayers(Collection<FanRantGameUpdatePlayerModel> players){
		this.players = players;
	}
 	public String getPost_id(){
		return this.post_id;
	}
	public void setPost_id(String post_id){
		this.post_id = post_id;
	}
 	public String getPost_time(){
		return this.post_time;
	}
	public void setPost_time(String post_time){
		this.post_time = post_time;
	}
 	public String getPost_type(){
		return this.post_type;
	}
	public void setPost_type(String post_type){
		this.post_type = post_type;
	}
 	public String getRating(){
		return this.rating;
	}
	public void setRating(String rating){
		this.rating = rating;
	}
 	public String getSubject(){
		return this.subject;
	}
	public void setSubject(String subject){
		this.subject = subject;
	}
 	public String getTeam_id(){
		return this.team_id;
	}
	public void setTeam_id(String team_id){
		this.team_id = team_id;
	}
 	public String getUser_id(){
		return this.user_id;
	}
	public void setUser_id(String user_id){
		this.user_id = user_id;
	}
 	public String getUser_name(){
		return this.user_name;
	}
	public void setUser_name(String user_name){
		this.user_name = user_name;
	}
 	public String getUsername(){
		return this.username;
	}
	public void setUsername(String username){
		this.username = username;
	}

    public Collection<FanRantGameUpdateOfficialModel> getOfficials() {
        return officials;
    }

    public void setOfficials(Collection<FanRantGameUpdateOfficialModel> officials) {
        this.officials = officials;
    }

    @Override
    public boolean equals(Object o) {
        if(o instanceof FanRantGameUpdateModel) {
            FanRantGameUpdateModel comp = (FanRantGameUpdateModel) o;
            if(comp.getPost_type().equals(post_type) && comp.post_time.equals(post_time)
                    && comp.username.equals(username)) {
                            return true;
            }
        }
        return false;
    }

    @Override
    public int compareTo(FanRantGameUpdateModel another) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date aDate = null;
        Date bDate = null;
        try {
            aDate = sdf.parse(String.valueOf(post_time));
            bDate = sdf.parse(String.valueOf(another.post_time));

        } catch (ParseException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        if(aDate == null || bDate == null) {
            return 0;
        }

        return bDate.compareTo(aDate);  //To change body of implemented methods use File | Settings | File Templates.

    }
}
