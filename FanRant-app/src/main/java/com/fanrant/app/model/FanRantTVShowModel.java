
package com.fanrant.app.model;

public class FanRantTVShowModel extends FanRantModel {
   	private String channel;
   	private String show;
   	private String start;

 	public String getChannel(){
		return this.channel;
	}
	public void setChannel(String channel){
		this.channel = channel;
	}
 	public String getShow(){
		return this.show;
	}
	public void setShow(String show){
		this.show = show;
	}
 	public String getStart(){
		return this.start;
	}
	public void setStart(String start){
		this.start = start;
	}
}
