package com.fanrant.app.model;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: anthonygrimmitt
 * Date: 11/05/2013
 * Time: 17:50
 * To change this template use File | Settings | File Templates.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class FanRantWallStatusModel extends FanRantModel {
    public static enum  WallState {
        LIVEGAME,
        COUNTRYGAME, //deprecated
        DIVISIONGAME,
        NOGAMES;

        public static final WallState fromOrdinal(int ord) {
            return WallState.values()[--ord];
        }
    }

    static WallState state;

    private List<FanRantFixtureModel> fixture_data;
    private int fixture_id;
    private int status;

    public List<FanRantFixtureModel> getFixture_data() {
        return this.fixture_data;
    }

    public void setFixture_data(List<FanRantFixtureModel> fixture_data) {
        this.fixture_data = fixture_data;
    }

    public int getFixture_id() {
        return this.fixture_id;
    }

    public void setFixture_id(int fixture_id) {
        this.fixture_id = fixture_id;
    }

    public int getStatus() {
        return this.status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public WallState getState() {
        state = WallState.fromOrdinal(getStatus());
        return state;
    }
}

