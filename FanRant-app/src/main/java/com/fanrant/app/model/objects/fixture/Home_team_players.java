
package com.fanrant.app.model.objects.fixture;

import com.fanrant.app.model.FanRantPlayerModel;

public class Home_team_players extends FanRantPlayerModel {
   	private String first_name;
   	private String id;
   	private String last_name;
   	private String nickname;
   	private String number;
   	private String position;
   	private String substitute;

 	public String getFirst_name(){
		return this.first_name;
	}
	public void setFirst_name(String first_name){
		this.first_name = first_name;
	}
 	public String getId(){
		return this.id;
	}
	public void setId(String id){
		this.id = id;
	}
 	public String getLast_name(){
		return this.last_name;
	}
	public void setLast_name(String last_name){
		this.last_name = last_name;
	}
 	public String getNickname(){
		return this.nickname;
	}
	public void setNickname(String nickname){
		this.nickname = nickname;
	}
 	public String getNumber(){
		return this.number;
	}
	public void setNumber(String number){
		this.number = number;
	}
 	public String getPosition(){
		return this.position;
	}
	public void setPosition(String position){
		this.position = position;
	}
 	public String getSubstitute(){
		return this.substitute;
	}
	public void setSubstitute(String substitute){
		this.substitute = substitute;
	}
}
