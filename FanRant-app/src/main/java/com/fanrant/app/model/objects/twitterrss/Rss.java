package com.fanrant.app.model.objects.twitterrss;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created with IntelliJ IDEA.
 * User: Antwork
 * Date: 11/11/2012
 * Time: 19:50
 * To change this template use File | Settings | File Templates.
 */
@Root(strict = false)
public class Rss {
    @Element
    private Channel channel;

    public Channel getChannel() {
        return channel;
    }
}
