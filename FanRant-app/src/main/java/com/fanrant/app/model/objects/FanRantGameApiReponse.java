
package com.fanrant.app.model.objects;

import com.fanrant.app.model.objects.gamecontent.Game;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.util.List;
@JsonIgnoreProperties(ignoreUnknown = true)
public class FanRantGameApiReponse {
   	private Number code;
   	private String message;
   	private List<Game> results;
   	private Number time;

 	public Number getCode(){
		return this.code;
	}
	public void setCode(Number code){
		this.code = code;
	}
 	public String getMessage(){
		return this.message;
	}
	public void setMessage(String message){
		this.message = message;
	}
 	public List<Game> getResults(){
		return this.results;
	}
	public void setResults(List<Game> results){
		this.results = results;
	}
 	public Number getTime(){
		return this.time;
	}
	public void setTime(Number time){
		this.time = time;
	}
}
