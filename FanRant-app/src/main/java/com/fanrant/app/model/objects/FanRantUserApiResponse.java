package com.fanrant.app.model.objects;

import com.fanrant.app.model.FanRantModel;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Created with IntelliJ IDEA.
 * User: Antwork
 * Date: 21/10/2012
 * Time: 12:59
 * To change this template use File | Settings | File Templates.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class FanRantUserApiResponse {
    private int code;
    private String message;
    private FanRantModel[] results;

    public  class auth {
    }
     @JsonIgnore
    private String time;
    private String token;
    private int token_duration;

    public FanRantUserApiResponse() {
    }

    public FanRantUserApiResponse(int code, String message, FanRantModel[] results, auth a) {
        this.code = code;
        this.message = message;
        this.results = results;
//        this.auth = a;
     }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public FanRantModel[] getResults() {
        return results;
    }

    public void setResults(FanRantModel[] results) {
        this.results = results;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
