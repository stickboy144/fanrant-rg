
package com.fanrant.app.model;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TeamModel  extends FanRantModel{
    private String competition_id;
    private String name;
    private String team_fixtures_url;
    private String id;
    private String team_rss_feed;
    private String team_twitter_feed;
    private String competition_name;

    public String getCompetition_id(){
        return this.competition_id;
    }
    public void setCompetition_id(String competition_id){
        this.competition_id = competition_id;
    }
    public String getName(){
        return this.name;
    }
    public void setName(String name){
        this.name = name;
    }
    public String getTeam_fixtures_url(){
        return this.team_fixtures_url;
    }
    public void setTeam_fixtures_url(String team_fixtures_url){
        this.team_fixtures_url = team_fixtures_url;
    }
    public String getid(){
        return this.id;
    }
    public void setid(String team_id){
        this.id = team_id;
    }
    public String getTeam_rss_feed(){
        return this.team_rss_feed;
    }
    public void setTeam_rss_feed(String team_rss_feed){
        this.team_rss_feed = team_rss_feed;
    }
    public String getTeam_twitter_feed(){
        return this.team_twitter_feed;
    }
    public void setTeam_twitter_feed(String team_twitter_feed){
        this.team_twitter_feed = team_twitter_feed;
    }

    public void setCompetition_name(String competition_name) {
        this.competition_name = competition_name;
    }

    public String getCompetition_name() {
        return competition_name;
    }
}
