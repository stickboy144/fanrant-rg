package com.fanrant.app.model.objects.twitterrss;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Antwork
 * Date: 11/11/2012
 * Time: 19:08
 * To change this template use File | Settings | File Templates.
 */
@Root(strict = false)
public class Channel {
    @ElementList(inline=true)
    List<Item> item;

    public List<Item> getItem() {
        return item;
    }

    public void setItem(List<Item> item) {
        this.item = item;
    }
}
