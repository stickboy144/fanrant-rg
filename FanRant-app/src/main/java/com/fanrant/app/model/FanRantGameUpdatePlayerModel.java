package com.fanrant.app.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created with IntelliJ IDEA.
 * User: anthonygrimmitt
 * Date: 27/04/2013
 * Time: 13:21
 * To change this template use File | Settings | File Templates.
 */
@DatabaseTable(tableName = "gameupdateplayer")
public class FanRantGameUpdatePlayerModel extends FanRantModel{
    @DatabaseField(generatedId = true)
    private int pid;
    @DatabaseField
    private String id;
    @DatabaseField
    private String first_name;
    @DatabaseField
    private String last_name;
    @DatabaseField
    private String nickname;
    @DatabaseField
    private String number;
    @DatabaseField
    private String position;
    @DatabaseField
    private String team_id;
    @DatabaseField
    private String team_name;
    @DatabaseField(canBeNull = true, foreign = true)
    private FanRantGameUpdateModel gameUpdateModel;
    public String getTeam_name() {
        return team_name;
    }

    public void setTeam_name(String team_name) {
        this.team_name = team_name;
    }

    private String substitute;

    public String getTeam_id() {
        return team_id;
    }

    public void setTeam_id(String team_id) {
        this.team_id = team_id;
    }

    public String getFirst_name(){
        return this.first_name;
    }
    public void setFirst_name(String first_name){
        this.first_name = first_name;
    }
    public String getId(){
        return this.id;
    }
    public void setId(String id){
        this.id = id;
    }
    public String getLast_name(){
        return this.last_name;
    }
    public void setLast_name(String last_name){
        this.last_name = last_name;
    }
    public String getNickname(){
        return this.nickname;
    }
    public void setNickname(String nickname){
        this.nickname = nickname;
    }
    public String getNumber(){
        return this.number;
    }
    public void setNumber(String number){
        this.number = number;
    }
    public String getPosition(){
        return this.position;
    }
    public void setPosition(String position){
        this.position = position;
    }
    public String getSubstitute(){
        return this.substitute;
    }
    public void setSubstitute(String substitute){
        this.substitute = substitute;
    }

    public FanRantGameUpdateModel getGameUpdateModel() {
        return gameUpdateModel;
    }

    public void setGameUpdateModel(FanRantGameUpdateModel gameUpdateModel) {
        this.gameUpdateModel = gameUpdateModel;
    }

    public int getPid() {
        return pid;
    }

    public void setPid(int pid) {
        this.pid = pid;
    }
}
