package com.fanrant.app.model.objects;

import com.fanrant.app.model.FanRantPlayerModel;

import java.util.Collection;

/**
 * Created with IntelliJ IDEA.
 * User: Antwork
 * Date: 09/12/2012
 * Time: 17:28
 * To change this template use File | Settings | File Templates.
 */
public class GamePlayers {
    private String type;
    private Collection<FanRantPlayerModel> players;




    public GamePlayers(String type, Collection players) {
        this.type = type;
        this.players = players;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Collection<FanRantPlayerModel> getPlayers() {
        return players;
    }

    public void setPlayers(Collection<FanRantPlayerModel> players) {
        this.players = players;
    }
}
