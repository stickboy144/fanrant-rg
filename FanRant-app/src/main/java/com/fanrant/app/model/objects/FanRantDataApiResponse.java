package com.fanrant.app.model.objects;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Created with IntelliJ IDEA.
 * User: Antwork
 * Date: 04/11/2012
 * Time: 15:03
 * To change this template use File | Settings | File Templates.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class FanRantDataApiResponse {
    private int code;
    private String message;
    private FanRantDataApiObject[] results;

    public  class auth {
    }
    @JsonIgnore
    private String time;
    private String token;
    private int token_duration;

    public FanRantDataApiResponse() {
    }

    public FanRantDataApiResponse(int code, String message, FanRantDataApiObject[] results, auth a) {
        this.code = code;
        this.message = message;
        this.results = results;
//        this.auth = a;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public FanRantDataApiObject[] getResults() {
        return results;
    }

    public void setResults(FanRantDataApiObject[] results) {
        this.results = results;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
