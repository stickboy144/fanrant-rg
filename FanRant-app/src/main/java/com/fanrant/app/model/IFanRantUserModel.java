package com.fanrant.app.model;

/**
 * Created with IntelliJ IDEA.
 * User: anthonygrimmitt
 * Date: 13/04/2013
 * Time: 13:12
 * To change this template use File | Settings | File Templates.
 */
public interface IFanRantUserModel {
    String getAvatar();

    void setAvatar(String avatar);

    String getCompetition_id();

    void setCompetition_id(String competition_id);

    String getCompetition_name();

    void setCompetition_name(String competition_name);

    String getEmail_address();

    void setEmail_address(String email_address);

    String getFacebookAccessToken();

    void setFacebookAccessToken(String facebookAccessToken);

    long getFacebookId();

    void setFacebookId(long facebookId);

    String getNational_team();

    void setNational_team(String national_team);

    String getPassword();

    void setPassword(String password);

    boolean isRegistered();

    void setRegistered(boolean registered);

    String getSignup();

    void setSignup(String signup);

    String getTeam_fixtures_url();

    void setTeam_fixtures_url(String team_fixtures_url);

    int getTeam_id();

    void setTeam_id(int team_id);

    String getTeam_rss_feed();

    void setTeam_rss_feed(String team_rss_feed);

    String getTeam_twitter_feed();

    void setTeam_twitter_feed(String team_twitter_feed);

    String getTwitterId();

    void setTwitterId(String twitterId);

    String getUsername();

    void setUsername(String username);

    void setLoggedIn(boolean loggedIn);

    boolean isLoggedIn();

    void populate(FanRantUserModel userModel);

    void setTeamName(String name);

    String getTeamName();

}
