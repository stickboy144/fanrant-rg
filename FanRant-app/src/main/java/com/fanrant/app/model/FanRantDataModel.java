package com.fanrant.app.model;

import android.provider.BaseColumns;
import com.fanrant.app.modules.util.FeedType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Antwork
 * Date: 04/11/2012
 * Time: 12:45
 * To change this template use File | Settings | File Templates.
 */
@DatabaseTable(tableName = "wallData")
@JsonIgnoreProperties(ignoreUnknown = true)
public class FanRantDataModel extends FanRantModel implements Comparable<FanRantDataModel> {
    @DatabaseField(generatedId = true, columnName = BaseColumns._ID)
    private int dataId;
    @DatabaseField
    @JsonProperty
    public String subject = new String();
    @DatabaseField
    @JsonProperty
    public String comment = new String();
    @DatabaseField
    @JsonProperty
    public String post_time = new String();
    @DatabaseField
    @JsonProperty
    public String  username = new String();
    @DatabaseField
    @JsonProperty
    public String avatar = new String();
    @DatabaseField
    public FeedType type = FeedType.COMMENTS;//default to comments
    @DatabaseField
    public String link;
    @DatabaseField
    public String post_id;

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getDataId() {
        return dataId;
    }

    public void setDataId(int dataId) {
        this.dataId = dataId;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getPost_id() {
        return post_id;
    }

    public void setPost_id(String post_id) {
        this.post_id = post_id;
    }

    public String getPost_time() {
        return post_time;
    }

    public void setPost_time(String post_time) {
        this.post_time = post_time;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public FeedType getType() {
        return type;
    }

    public void setType(FeedType type) {
        this.type = type;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }


    @Override
    public int compareTo(FanRantDataModel another) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss'Z'");

        Date aDate = null;
        Date bDate = null;
        try {
            aDate = sdf.parse(String.valueOf(post_time));
            bDate = sdf.parse(String.valueOf(another.post_time));

        } catch (ParseException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        if(aDate == null || bDate == null) {
            return 0;
        }

        return bDate.compareTo(aDate);  //To change body of implemented methods use File | Settings | File Templates.

    }

    @Override
    public boolean equals(Object o) {
        if(o instanceof FanRantDataModel) {
            FanRantDataModel model = (FanRantDataModel) o;

            if(model.type.toString().equals(type.toString())
                    && model.comment.toString().equals(comment.toString())
                    && model.username.toString().equals(username.toString())
                    && model.post_time.toString().equals(post_time.toString())) {
               return true;
            }
        }
        return false;
    }
}
