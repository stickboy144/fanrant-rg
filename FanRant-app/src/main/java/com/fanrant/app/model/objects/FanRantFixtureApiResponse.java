package com.fanrant.app.model.objects;

import com.fanrant.app.model.FanRantFixtureModel;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Created with IntelliJ IDEA.
 * User: Antwork
 * Date: 24/11/2012
 * Time: 16:44
 * To change this template use File | Settings | File Templates.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class FanRantFixtureApiResponse {
    private int code;
    private String message;
    private FanRantFixtureModel[] results;

    public  class auth {
    }
    @JsonIgnore
    private String time;
    private String token;
    private int token_duration;

    public FanRantFixtureApiResponse() {
    }

    public FanRantFixtureApiResponse(int code, String message, FanRantFixtureModel[] results, auth a) {
        this.code = code;
        this.message = message;
        this.results = results;
//        this.auth = a;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public FanRantFixtureModel[] getResults() {
        return results;
    }

    public void setResults(FanRantFixtureModel[] results) {
        this.results = results;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
