
package com.fanrant.app.model.objects.gamecontent;

import com.fanrant.app.model.FanRantGameUpdateModel;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.util.List;
@JsonIgnoreProperties(ignoreUnknown = true)
public class Game {
   	private List<FanRantGameUpdateModel> latest;
   	private String matchtime;

 	public List<FanRantGameUpdateModel> getLatest(){
		return this.latest;
	}
	public void setLatest(List<FanRantGameUpdateModel> latest){
		this.latest = latest;
	}
 	public String getMatchtime(){
		return this.matchtime;
	}
	public void setMatchtime(String matchtime){
		this.matchtime = matchtime;
	}
}
