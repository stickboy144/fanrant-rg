
package com.fanrant.app.model;

import com.fanrant.app.model.FanRantFixtureModel;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "officials")
public class FanRantOfficialsModel {
    @DatabaseField
   	private String first_name;
    @DatabaseField
   	private String home_town;
    @DatabaseField(id = true)
   	private String id;
    @DatabaseField
   	private String last_name;
    @DatabaseField
   	private String position;
    @DatabaseField(canBeNull = false, foreign = true)
    private FanRantFixtureModel model;

 	public String getFirst_name(){
		return this.first_name;
	}
	public void setFirst_name(String first_name){
		this.first_name = first_name;
	}
 	public String getHome_town(){
		return this.home_town;
	}
	public void setHome_town(String home_town){
		this.home_town = home_town;
	}
 	public String getId(){
		return this.id;
	}
	public void setId(String id){
		this.id = id;
	}
 	public String getLast_name(){
		return this.last_name;
	}
	public void setLast_name(String last_name){
		this.last_name = last_name;
	}
 	public String getPosition(){
		return this.position;
	}
	public void setPosition(String position){
		this.position = position;
	}

    public FanRantFixtureModel getModel() {
        return model;
    }

    public void setModel(FanRantFixtureModel model) {
        this.model = model;
    }
}
