package com.fanrant.app.model.objects.twitterrss;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created with IntelliJ IDEA.
 * User: Antwork
 * Date: 11/11/2012
 * Time: 19:00
 * To change this template use File | Settings | File Templates.
 */
@Root(strict = false)
public class Item {
    @Element
    private String title;
    @Element
    private String description;
    @Element
    private String pubDate;
    @Element
    private String guid;
    @Element
    private String link;
//    @Namespace(prefix = "twitter", reference = "http://api.twitter.com")
//    @Element(name = "twitter:source")
//    private String source;
//    @Namespace(prefix = "twitter", reference = "http://api.twitter.com")
//    @Element(name = "twitter:place")
//    private String place;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPubDate() {
        return pubDate;
    }

    public void setPubDate(String pubDate) {
        this.pubDate = pubDate;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

//    public String getSource() {
//        return source;
//    }
//
//    public void setSource(String source) {
//        this.source = source;
//    }
//
//    public String getPlace() {
//        return place;
//    }
//
//    public void setPlace(String place) {
//        this.place = place;
//    }
}
