package com.fanrant.app.model;

/**
 * Created with IntelliJ IDEA.
 * User: Ant Grimmitt
 * Date: 31/03/2013
 * Time: 17:59
 */
public interface IUserModel {
    String getCompetition_id();

    int getTeam_id();
}
