package com.fanrant.app.model;

import android.content.Context;
import com.google.inject.Inject;
import com.google.inject.Provider;

/**
 * Created with IntelliJ IDEA.
 * User: Ant Grimmitt
 * Date: 27/03/2013
 * Time: 11:05
 */
public class FanRantModel {

    private Provider<Context> contextProvider;

    @Inject
    public FanRantModel(Provider<Context> contextProvider) {
        this.contextProvider = contextProvider;
    }

    public FanRantModel() {
    }

    protected Context getContext() {
        return contextProvider.get();

    }
}
