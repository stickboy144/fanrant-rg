package com.fanrant.app;

/**
 * Created with IntelliJ IDEA.
 * User: Ant Grimmitt
 * Date: 24/03/2013
 * Time: 15:23
 */
public enum Environments {
    DEFAULT, TESTING, DEV, ANTDEV
}
