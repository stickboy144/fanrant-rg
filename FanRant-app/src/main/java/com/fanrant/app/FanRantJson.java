package com.fanrant.app;

/**
 * Created with IntelliJ IDEA.
 * User: Ant Grimmitt
 * Date: 23/03/2013
 * Time: 22:40
 */
public class FanRantJson {
    public static final String GETPROFILE = "{\"code\": 200, \"message\": \"\", \"results\": [\n" +
            "    {\n" +
            "        \"id\": \"212\",\n" +
            "        \"username\": \"ant2\",\n" +
            "        \"name\": \"\",\n" +
            "        \"email_address\": \"a@a.com\",\n" +
            "        \"avatar\": \"http:\\/\\/dev-api.fanrant.co.uk\\/images\\/uploads\\/212\\/avatar\\/4dazr5cp.jpg\",\n" +
            "        \"dob\": \"1969-08-10\",\n" +
            "        \"gender\": \"\",\n" +
            "        \"mobile\": \"\",\n" +
            "        \"city\": \"\",\n" +
            "        \"postcode\": \"\",\n" +
            "        \"twitter_user\": \"\",\n" +
            "        \"facebook_user\": \"\",\n" +
            "        \"team\": \"Arsenal\",\n" +
            "        \"national_team\": \"England\",\n" +
            "        \"team_id\": \"3\",\n" +
            "        \"national_team_id\": \"63\",\n" +
            "        \"receive_email\": \"0\",\n" +
            "        \"signup\": \"2012-12-16 13:22:38\",\n" +
            "        \"competition_id\": \"8\",\n" +
            "        \"competition_name\": \"English Barclays Premier League\",\n" +
            "        \"team_rss_feed\": \"http:\\/\\/www.bbc.co.uk\\/sport\\/football\\/teams\\/arsenal\\/rss.xml\",\n" +
            "        \"team_twitter_feed\": \"http:\\/\\/api.twitter.com\\/1\\/statuses\\/user_timeline.rss?screen_name=ARSENAL\",\n" +
            "        \"team_fixtures_url\": \"http:\\/\\/www.bbc.co.uk\\/sport\\/football\\/teams\\/arsenal\\/fixtures\",\n" +
            "        \"rating_count\": \"31\"\n" +
            "    }\n" +
            "], \"time\": 1364065022, \"auth\": {\n" +
            "    \"user_id\": \"212\",\n" +
            "    \"token\": \"1tmdf1xwahzyozy2yij0\",\n" +
            "    \"token_duration\": \"600\"\n" +
            "}}";

    public static final String GETWRONGLOGINPROFILE = "{\"code\":\"401\",\"message\":\"The login details were invalid\",\"error\":true,\"time\":1364141812}";

    public static final String GETTEAMDATAGUESTMODE = "{\n" +
            "  \"time\" : 1364153285,\n" +
            "  \"code\" : 200,\n" +
            "  \"message\" : \"\",\n" +
            "  \"results\" : [\n" +
            "    {\n" +
            "      \"team_twitter_feed\" : \"http://api.twitter.com/1/statuses/user_timeline.rss?screen_name=ARSENAL\",\n" +
            "      \"team_id\" : \"3\",\n" +
            "      \"team_rss_feed\" : \"http://www.bbc.co.uk/sport/football/teams/arsenal/rss.xml\",\n" +
            "      \"team\" : \"Arsenal\",\n" +
            "      \"competition_id\" : \"8\",\n" +
            "      \"team_fixtures_url\" : \"http://www.bbc.co.uk/sport/football/teams/arsenal/fixtures\"\n" +
            "    }\n" +
            "  ]\n" +
            "}";


    public static final String FIXTURES = "{\"code\":200,\"message\":\"\",\"results\":[" +
            "{\"id\":\"442160\",\"kickoff\":\"2012-12-15 15:00:00\",\"home_team_id\":\"14\"," +
            "\"home_team_name\":\"Liverpool\",\"away_team_id\":\"7\",\"away_team_name\":\"Aston Villa\"," +
            "\"ground_id\":\"39\",\"ground_name\":\"Anfield\",\"ground_latitude\":\"53.049862000\"," +
            "\"ground_longitude\":\"-2.192735000\",\"competition_id\":\"8\",\"competition_name\":" +
            "\"English Barclays Premier League\",\"live\":\"1\",\"scoreline\":\"1-3\"," +
            "\"officials\":[{\"id\":\"40923\",\"first_name\":\"Neil\",\"last_name\":\"Swarbrick\"," +
            "\"home_town\":\"\",\"position\":\"Referee\"}],\"" +
            "home_team_players\":[{\"id\":\"8432\",\"first_name\":\"Jos\\u00e9\",\"last_name\":\"Reina\"," +
            "\"number\":\"25\",\"position\":null,\"substitute\":\"0\"},{\"id\":\"21094\",\"first_name\":" +
            "\"Daniel\",\"last_name\":\"Agger\",\"number\":\"5\",\"position\":\"Defender\",\"substitute\":\"0\"}," +
            "{\"id\":\"12002\",\"first_name\":\"Stewart\",\"last_name\":\"Downing\",\"number\":\"19\",\"position\":\"Defender\",\"substitute\":\"0\"},{\"id\":\"9047\",\"first_name\":\"Glen\",\"last_name\":\"Johnson\",\"number\":\"2\",\"position\":\"Defender\",\"substitute\":\"0\"},{\"id\":\"26793\",\"first_name\":\"Martin\",\"last_name\":\"Skrtel\",\"number\":\"37\",\"position\":\"Defender\",\"substitute\":\"0\"},{\"id\":\"40555\",\"first_name\":\"Joe\",\"last_name\":\"Allen\",\"number\":\"24\",\"position\":\"Midfielder\",\"substitute\":\"0\"},{\"id\":\"1814\",\"first_name\":\"Steven\",\"last_name\":\"Gerrard\",\"number\":\"8\",\"position\":\"Midfielder\",\"substitute\":\"0\"},{\"id\":\"43191\",\"first_name\":\"Lucas\",\"last_name\":\"Leiva\",\"number\":\"21\",\"position\":\"Midfielder\",\"substitute\":\"0\"},{\"id\":\"50232\",\"first_name\":\"Jonjo\",\"last_name\":\"Shelvey\",\"number\":\"33\",\"position\":\"Striker\",\"substitute\":\"0\"},{\"id\":\"103955\",\"first_name\":\"Raheem\",\"last_name\":\"Sterling\",\"number\":\"31\",\"position\":\"Striker\",\"substitute\":\"0\"},{\"id\":\"39336\",\"first_name\":\"Luis\",\"last_name\":\"Su\\u00e1rez\",\"number\":\"7\",\"position\":\"Striker\",\"substitute\":\"0\"},{\"id\":\"9631\",\"first_name\":\"Bradley\",\"last_name\":\"Jones\",\"number\":\"1\",\"position\":null,\"substitute\":\"1\"},{\"id\":\"1809\",\"first_name\":\"Jamie\",\"last_name\":\"Carragher\",\"number\":\"23\",\"position\":\"Defender\",\"substitute\":\"1\"},{\"id\":\"78108\",\"first_name\":\"Sebasti\\u00e1n\",\"last_name\":\"Coates\",\"number\":\"16\",\"position\":\"Defender\",\"substitute\":\"1\"},{\"id\":\"89088\",\"first_name\":\"Andre\",\"last_name\":\"Wisdom\",\"number\":\"47\",\"position\":\"Defender\",\"substitute\":\"1\"},{\"id\":\"2060\",\"first_name\":\"Joe\",\"last_name\":\"Cole\",\"number\":\"10\",\"position\":\"Midfielder\",\"substitute\":\"1\"},{\"id\":\"56979\",\"first_name\":\"Jordan\",\"last_name\":\"Henderson\",\"number\":\"14\",\"position\":\"Midfielder\",\"substitute\":\"1\"},{\"id\":\"103953\",\"first_name\":\"Jes\\u00fas\",\"last_name\":\"Fern\\u00e1ndez Saez\",\"number\":\"30\",\"position\":\"Striker\",\"substitute\":\"1\"}],\"away_team_players\":[{\"id\":\"41705\",\"first_name\":\"Brad\",\"last_name\":\"Guzan\",\"number\":\"22\",\"position\":null,\"substitute\":\"0\"},{\"id\":\"52477\",\"first_name\":\"Nathan\",\"last_name\":\"Baker\",\"number\":\"32\",\"position\":\"Defender\",\"substitute\":\"0\"},{\"id\":\"58845\",\"first_name\":\"Ciaran\",\"last_name\":\"Clark\",\"number\":\"6\",\"position\":\"Defender\",\"substitute\":\"0\"},{\"id\":\"49493\",\"first_name\":\"Chris\",\"last_name\":\"Herd\",\"number\":\"31\",\"position\":\"Defender\",\"substitute\":\"0\"},{\"id\":\"59013\",\"first_name\":\"Barry\",\"last_name\":\"Bannan\",\"number\":\"25\",\"position\":\"Midfielder\",\"substitute\":\"0\"},{\"id\":\"13866\",\"first_name\":\"Brett\",\"last_name\":\"Holman\",\"number\":\"14\",\"position\":\"Midfielder\",\"substitute\":\"0\"},{\"id\":\"45139\",\"first_name\":\"Eric\",\"last_name\":\"Lichaj\",\"number\":\"30\",\"position\":\"Midfielder\",\"substitute\":\"0\"},{\"id\":\"68983\",\"first_name\":\"Matthew\",\"last_name\":\"Lowton\",\"number\":\"34\",\"position\":\"Midfielder\",\"substitute\":\"0\"},{\"id\":\"60551\",\"first_name\":\"Ashley Roy\",\"last_name\":\"Westwood\",\"number\":\"15\",\"position\":\"Midfielder\",\"substitute\":\"0\"},{\"id\":\"54861\",\"first_name\":\"Christian\",\"last_name\":\"Benteke\",\"number\":\"20\",\"position\":\"Striker\",\"substitute\":\"0\"},{\"id\":\"80979\",\"first_name\":\"Andreas\",\"last_name\":\"Weimann\",\"number\":\"26\",\"position\":\"Striker\",\"substitute\":\"0\"},{\"id\":\"1822\",\"first_name\":\"Shay\",\"last_name\":\"Given\",\"number\":\"1\",\"position\":null,\"substitute\":\"1\"},{\"id\":\"56981\",\"first_name\":\"Joe\",\"last_name\":\"Bennett\",\"number\":\"27\",\"position\":\"Defender\",\"substitute\":\"1\"},{\"id\":\"51938\",\"first_name\":\"Marc\",\"last_name\":\"Albrighton\",\"number\":\"12\",\"position\":\"Midfielder\",\"substitute\":\"1\"},{\"id\":\"41823\",\"first_name\":\"Fabian\",\"last_name\":\"Delph\",\"number\":\"16\",\"position\":\"Midfielder\",\"substitute\":\"1\"},{\"id\":\"39242\",\"first_name\":\"Karim\",\"last_name\":\"El Ahmadi\",\"number\":\"8\",\"position\":\"Midfielder\",\"substitute\":\"1\"},{\"id\":\"18737\",\"first_name\":\"Charles\",\"last_name\":\"N'Zogbia\",\"number\":\"10\",\"position\":\"Midfielder\",\"substitute\":\"1\"},{\"id\":\"56206\",\"first_name\":\"Jordan\",\"last_name\":\"Bowery\",\"number\":\"21\",\"" +
            "position\":\"Striker\",\"substitute\":\"1\"}]}]}";
}
