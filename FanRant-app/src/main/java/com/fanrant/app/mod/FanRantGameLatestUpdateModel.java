package com.fanrant.app.mod;

import com.fanrant.app.model.FanRantGameUpdateModel;
import com.fanrant.app.model.FanRantModel;

/**
 * Created with IntelliJ IDEA.
 * User: anthonygrimmitt
 * Date: 26/04/2013
 * Time: 22:31
 * To change this template use File | Settings | File Templates.
 */
public class FanRantGameLatestUpdateModel  extends FanRantModel {

    private FanRantGameUpdateModel[] latest;
    private String matchtime;



    public FanRantGameUpdateModel[] getLatest() {
        return latest;
    }

    public String getMatchtime() {
        return matchtime;
    }
}
