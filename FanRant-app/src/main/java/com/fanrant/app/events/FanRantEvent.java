package com.fanrant.app.events;

import com.fanrant.app.model.FanRantModel;

/**
 * Created with IntelliJ IDEA.
 * User: Ant Grimmitt
 * Date: 27/03/2013
 * Time: 12:02
 */
public class FanRantEvent {

    FanRantModel model;
    FanRantModel[] models;

    public FanRantEvent(FanRantModel model) {
        this.model = model;
    }

    public FanRantEvent() {
    }

    public FanRantEvent(FanRantModel[] models) {
        //To change body of created methods use File | Settings | File Templates.
        this.models = models;
    }

    public FanRantModel getModel() {
        return model;
    }

    public void setModel(FanRantModel model) {
        this.model = model;
    }

    public FanRantModel[] getModels() {
        return models;
    }

    public void setModels(FanRantModel[] models) {
        this.models = models;
    }
}
