package com.fanrant.app.events;

/**
 * Created with IntelliJ IDEA.
 * User: Ant Grimmitt
 * Date: 29/03/2013
 * Time: 08:46
 */
public class TeamSelectedEvent extends FanRantEvent{
    private String teamid;
    private String compid;
    private String teamname;

    public TeamSelectedEvent(String teamid, String comp, String teamname) {

        this.teamid = teamid;
        compid = comp;
        this.teamname = teamname;
    }

    public String getTeamid() {
        return teamid;
    }

    public void setTeamid(String teamid) {
        this.teamid = teamid;
    }

    public String getCompid() {
        return compid;
    }

    public void setCompid(String compid) {
        this.compid = compid;
    }

    public String getTeamname() {
        return teamname;
    }

    public void setTeamname(String teamname) {
        this.teamname = teamname;
    }
}
