package com.fanrant.app.events;

import com.fanrant.app.model.FanRantModel;

/**
 * Created with IntelliJ IDEA.
 * User: anthonygrimmitt
 * Date: 14/04/2013
 * Time: 13:40
 * To change this template use File | Settings | File Templates.
 */
public class TVScheduleLoadedEvent extends FanRantEvent {

    public TVScheduleLoadedEvent(FanRantModel[] model) {
        super(model);
    }
}
