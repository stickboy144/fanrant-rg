package com.fanrant.app.events;

import com.fanrant.app.modules.util.CommentType;

/**
 * Created with IntelliJ IDEA.
 * User: anthonygrimmitt
 * Date: 07/04/2013
 * Time: 21:36
 * To change this template use File | Settings | File Templates.
 */
public class FanRantCommentViewEvent extends FanRantEvent {

    private CommentType type;

    public FanRantCommentViewEvent(CommentType type) {
        this.type = type;
    }

    public CommentType getType() {
        return type;
    }

    public void setType(CommentType type) {
        this.type = type;
    }
}
