package com.fanrant.app.events;

import com.fanrant.app.model.FanRantModel;
import com.fanrant.app.model.FanRantUserModel;
import com.fanrant.app.model.IFanRantUserModel;

/**
 * Created with IntelliJ IDEA.
 * User: Ant Grimmitt
 * Date: 24/03/2013
 * Time: 17:26
 */
public class LoggedInEvent extends FanRantEvent {
    public LoggedInEvent(IFanRantUserModel model) {
        super((FanRantUserModel)model);
    }

    public LoggedInEvent() {
    }
}
