package com.fanrant.app.events;

import com.fanrant.app.model.FanRantUserModel;
import com.fanrant.app.model.IFanRantUserModel;

/**
 * Created with IntelliJ IDEA.
 * User: Ant Grimmitt
 * Date: 24/03/2013
 * Time: 17:33
 */
public class GuestModeEvent extends FanRantEvent {
    public GuestModeEvent(IFanRantUserModel userModel) {
        super((FanRantUserModel)userModel);
    }
}
