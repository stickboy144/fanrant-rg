package com.fanrant.app.events;

/**
 * Created with IntelliJ IDEA.
 * User: anthonygrimmitt
 * Date: 07/04/2013
 * Time: 20:35
 * To change this template use File | Settings | File Templates.
 */
public class DataLoadedEvent extends FanRantEvent {

    private boolean commentsLoaded = false;
    private boolean rssLoaded =false;
    private boolean twitterLoaded =false;

    public boolean isCommentsLoaded() {
        return commentsLoaded;
    }

    public void setCommentsLoaded(boolean commentsLoaded) {
        this.commentsLoaded = commentsLoaded;
    }

    public boolean isRssLoaded() {
        return rssLoaded;
    }

    public void setRssLoaded(boolean rssLoaded) {
        this.rssLoaded = rssLoaded;
    }

    public boolean isTwitterLoaded() {
        return twitterLoaded;
    }

    public void setTwitterLoaded(boolean twitterLoaded) {
        this.twitterLoaded = twitterLoaded;
    }

    public boolean isAllDataStreamsLoaded() {
        return isCommentsLoaded() && isRssLoaded() && isTwitterLoaded();
    }
}
