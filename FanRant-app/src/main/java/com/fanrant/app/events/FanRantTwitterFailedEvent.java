package com.fanrant.app.events;

/**
 * Created by anthonygrimmitt on 18/05/2013.
 */
public class FanRantTwitterFailedEvent extends FanRantEvent {
    private Exception exception;

    public FanRantTwitterFailedEvent(Exception exception) {
        this.exception = exception;
    }

    public Exception getException() {
        return exception;
    }

    public void setException(Exception exception) {
        this.exception = exception;
    }
}
