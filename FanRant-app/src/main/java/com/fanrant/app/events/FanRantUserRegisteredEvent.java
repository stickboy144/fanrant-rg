package com.fanrant.app.events;

import com.fanrant.app.model.FanRantUserModel;

/**
 * Created with IntelliJ IDEA.
 * User: anthonygrimmitt
 * Date: 12/05/2013
 * Time: 15:39
 * To change this template use File | Settings | File Templates.
 */
public class FanRantUserRegisteredEvent extends FanRantEvent {
    public FanRantUserRegisteredEvent(FanRantUserModel[] fanRantUserModels) {
        setModel(fanRantUserModels[0]);
    }
}
