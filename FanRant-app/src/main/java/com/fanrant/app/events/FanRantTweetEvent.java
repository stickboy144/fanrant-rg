package com.fanrant.app.events;

/**
 * Created with IntelliJ IDEA.
 * User: anthonygrimmitt
 * Date: 27/04/2013
 * Time: 17:26
 * To change this template use File | Settings | File Templates.
 */
public class FanRantTweetEvent {
    private String comment;
    private boolean postingComment;

    public FanRantTweetEvent(String comment, boolean postingComment) {

        this.comment = comment;
        this.postingComment = postingComment;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
