package com.fanrant.app.events;

/**
 * Created with IntelliJ IDEA.
 * User: Ant Grimmitt
 * Date: 27/03/2013
 * Time: 12:08
 */
public class FanRantErrorEvent {
    private Exception exception;

    public FanRantErrorEvent(Exception exception) {
        //To change body of created methods use File | Settings | File Templates.
        this.exception = exception;
    }

    public Exception getException() {
        return exception;
    }

    public void setException(Exception exception) {
        this.exception = exception;
    }
}
