package com.fanrant.app.controller;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import com.fanrant.app.Environments;
import com.fanrant.app.events.FanRantCommentPostedEvent;
import com.fanrant.app.events.FanRantErrorEvent;
import com.fanrant.app.events.FanRantPostInProgressEvent;
import com.fanrant.app.events.FanRantTweetEvent;
import com.fanrant.app.model.FanRantUserModel;
import com.fanrant.app.modules.IPreferences;
import com.fanrant.app.modules.PreferenceConstants;
import com.fanrant.app.modules.api.FanRantAPIParams;
import com.fanrant.app.modules.api.FanRantAsyncTask;
import com.fanrant.app.modules.api.MockAsyncTask;
import com.fanrant.app.modules.api.responders.FanRantPostCommentResponse;
import com.fanrant.app.modules.api.responders.FanRantPostFrantResponse;
import com.fanrant.app.modules.util.ApplicationMessages;
import com.fanrant.app.modules.util.CommentType;
import com.fanrant.app.modules.util.Frants;
import com.google.inject.Inject;
import com.google.inject.Provider;
import roboguice.event.EventManager;
import roboguice.util.RoboAsyncTask;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: anthonygrimmitt
 * Date: 07/04/2013
 * Time: 22:29
 * To change this template use File | Settings | File Templates.
 */
public class PostController extends FanRantController {

    @Inject
    IPreferences preferences;

    @Inject
    FanRantUserModel userModel;

    @Inject
    EventManager eventManager;
    private boolean posting;
    private boolean postToFacebook;
    private boolean postToTwitter;

    @Inject
    public PostController(Provider<Context> contextProvider) {
        super(contextProvider);
    }

    public void performPost(CommentType curentCommentType, boolean postToFacebookChecked, final String comment) {

        if (posting) {
            eventManager.fire(new FanRantPostInProgressEvent());
        } else {
            posting = true;
            String wallId = null;
            String wallType = null;

            switch (curentCommentType) {
                case CLUB:
                    wallId = String.valueOf(userModel.team_id);
                    wallType = "club_id";
                    break;
                case DIVISON:
                    wallId = userModel.competition_id;
                    wallType = "division_id";
                    break;
            }


            final Map<String, String> postData = new HashMap<String, String>();
            postData.put("method", "addComment");
            postData.put("show_teams", "1");
            postData.put(wallType, wallId);
            if (userModel.isRegistered()) {
                postData.put("username", userModel.getUsername());
                postData.put("password", userModel.getPassword());
            } else {
                postData.put("username", "unregd");
                postData.put("password", "nopw");
            }
            postData.put("comment", comment);

            if (isPostToFacebook()) {
                postData.put("facebookid", String.valueOf(userModel.facebookId));
                postData.put("facebook_access_token", userModel.facebookAccessToken);
            }

            final Handler handler = new Handler() {
                @Override
                public void handleMessage(Message msg) {
                    if (msg.what == ApplicationMessages.ASYNCTASKSUCESS.ordinal()) {
                        if (isPostToTwitter()) {
                            eventManager.fire(new FanRantTweetEvent(comment, false));
                        } else {
                            eventManager.fire(new FanRantCommentPostedEvent());
                        }
                    } else if (msg.what == ApplicationMessages.ASYNCTASKFAILED.ordinal()) {
                        eventManager.fire(new FanRantErrorEvent((Exception) msg.obj));
                    }
                    super.handleMessage(msg);
                }
            };

            FanRantAPIParams params = new FanRantAPIParams(FanRantAPIParams.ApiService.POST, FanRantPostCommentResponse.class, postData, preferences); //TODO: inject prefs
            RoboAsyncTask t = preferences.getEnvironment() == Environments.TESTING ?
                    new MockAsyncTask(getContext(), params, handler) : new FanRantAsyncTask(getContext(), params, handler);
            t.execute();

        }
    }

    public void performFrantPost(Frants currentFrant, String currentFixtureId, float rating, final String comment, int currentPlayerId) {

        HashMap<String, String> postData = new HashMap<String, String>();
        postData.put("method", "addRating");
        postData.put("fixture_id", currentFixtureId);
        postData.put("rating", String.valueOf(Math.round(rating)));
        postData.put("comment", comment);

        postData.put("subject_id", String.valueOf(currentFrant.ordinal()));
        if (currentFrant == Frants.THEREFSBLIND) {
            postData.put("official_ids", String.valueOf(currentPlayerId));
        } else {
            postData.put("player_ids", String.valueOf(currentPlayerId));
        }
        if (isPostToFacebook()) {
            postData.put("facebookid", String.valueOf(userModel.facebookId));
            postData.put("facebook_access_token", userModel.facebookAccessToken);
        }


        if (userModel.isRegistered()) {

            postData.put("username", userModel.getUsername());
            postData.put("password", userModel.getPassword());
        } else {
            postData.put("username", "unregd");
            postData.put("password", "nopw");
        }
        posting = true;

        final Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                if (msg.what == ApplicationMessages.ASYNCTASKSUCESS.ordinal()) {
                    if (isPostToTwitter()) {
                        eventManager.fire(new FanRantTweetEvent(comment, true));
                    } else {
                        eventManager.fire(new FanRantCommentPostedEvent());
                    }
                } else if (msg.what == ApplicationMessages.ASYNCTASKFAILED.ordinal()) {
                    eventManager.fire(new FanRantErrorEvent((Exception) msg.obj));
                }
                super.handleMessage(msg);
            }
        };

        FanRantAPIParams params = new FanRantAPIParams(FanRantAPIParams.ApiService.POST, FanRantPostFrantResponse.class, postData, preferences); //TODO: inject prefs
        RoboAsyncTask t = preferences.getEnvironment() == Environments.TESTING ?
                new MockAsyncTask(getContext(), params, handler) : new FanRantAsyncTask(getContext(), params, handler);
        t.execute();

    }

    public void setPostToFacebook(boolean postToFacebook) {
        this.postToFacebook = postToFacebook;
    }

    public boolean isPostToFacebook() {
        return postToFacebook;
    }

    public void setPostToTwitter(boolean postToTwitter) {
        this.postToTwitter = postToTwitter;
    }

    public boolean isPostToTwitter() {
        return postToTwitter;
    }

    public void performPostCard(String currentFixtureId, int currentPlayerId, boolean yellowCard) {
        HashMap<String, String> postData = new HashMap<String, String>();

        postData.put("method", "addCard");
        postData.put("fixture_id", currentFixtureId);
        postData.put("player_ids", String.valueOf(currentPlayerId));


        if (userModel.isRegistered()) {

            postData.put("username", userModel.getUsername());
            postData.put("password", userModel.getPassword());
        } else {
            postData.put("username", "unregd");
            postData.put("password", "nopw");
        }

        if (yellowCard) {
            postData.put("card_type", "yellow");
        } else {
            postData.put("card_type", "red");
        }
        posting = true;

        final Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                if (msg.what == ApplicationMessages.ASYNCTASKSUCESS.ordinal()) {
                    eventManager.fire(new FanRantCommentPostedEvent());

                } else if (msg.what == ApplicationMessages.ASYNCTASKFAILED.ordinal()) {
                    eventManager.fire(new FanRantErrorEvent((Exception) msg.obj));
                }
                super.handleMessage(msg);
            }
        };

        FanRantAPIParams params = new FanRantAPIParams(FanRantAPIParams.ApiService.POST, FanRantPostFrantResponse.class, postData, preferences); //TODO: inject prefs
        RoboAsyncTask t = preferences.getEnvironment() == Environments.TESTING ?
                new MockAsyncTask(getContext(), params, handler) : new FanRantAsyncTask(getContext(), params, handler);
        t.execute();


    }
}
