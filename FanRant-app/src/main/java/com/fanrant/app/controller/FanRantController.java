package com.fanrant.app.controller;

import android.content.Context;
import com.google.inject.Inject;
import com.google.inject.Provider;

/**
 * Created with IntelliJ IDEA.
 * User: Ant Grimmitt
 * Date: 24/03/2013
 * Time: 16:48
 */
public class FanRantController {

    private Provider<Context> contextProvider;

    @Inject
    public FanRantController(Provider<Context> contextProvider) {
        this.contextProvider = contextProvider;
    }

    protected Context getContext() {
       return contextProvider.get();

   }
}
