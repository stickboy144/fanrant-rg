package com.fanrant.app.controller;

import android.content.Context;
import com.google.inject.Inject;
import com.google.inject.Provider;

/**
 * Created with IntelliJ IDEA.
 * User: Ant Grimmitt
 * Date: 26/03/2013
 * Time: 16:19
 */
public class WelcomeController extends FanRantController{

    @Inject
    public WelcomeController(Provider<Context> contextProvider) {
        super(contextProvider);
    }

}
