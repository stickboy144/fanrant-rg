package com.fanrant.app.controller;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.os.Handler;
import android.os.Message;
import com.fanrant.app.Environments;
import com.fanrant.app.events.*;
import com.fanrant.app.model.*;
import com.fanrant.app.modules.IPreferences;
import com.fanrant.app.modules.PreferenceConstants;
import com.fanrant.app.modules.api.FanRantAPIParams;
import com.fanrant.app.modules.api.FanRantAsyncTask;
import com.fanrant.app.modules.api.MockAsyncTask;
import com.fanrant.app.modules.api.responders.FanRantTeamResponse;
import com.fanrant.app.modules.api.responders.FanRantTeamsByDivisionResponse;
import com.fanrant.app.modules.api.responders.FanRantUserResponse;
import com.fanrant.app.modules.db.FanRantDatabase;
import com.fanrant.app.modules.providers.TeamContentProvider;
import com.fanrant.app.modules.util.ApplicationMessages;
import com.google.inject.Inject;
import com.google.inject.Provider;
import org.codehaus.jackson.map.ObjectMapper;
import roboguice.event.EventManager;
import roboguice.util.RoboAsyncTask;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * H
 * as three modes
 * <p/>
 * 1) startWelcomeMode - no prior login
 * 2) startLoginMode - IS A FanRantUserModel User and has logged in to the device
 * 3) startGuestMode - IS NOT A however has used the app before as a guest
 * User: Ant Grimmitt
 * Date: 24/03/2013
 * Time: 16:43
 */
public class UserController extends FanRantController {
    @Inject
    IFanRantUserModel userModel;

    @Inject
    FanrantTeamsListModel teamsListModel;

    @Inject
    IPreferences preferences;

    @Inject
    EventManager eventManager;

    @Inject
    ContentResolver resolver;
    private String user = null;
    private String pass = null;
    private boolean guestUser = false;
    private FanRantTeamsByCompetitonModel[] teamModels;//hack
    private FanRantUserModel savedUserModel;

    @Inject
    public UserController(Provider<Context> contextProvider) {
        super(contextProvider);
    }


    public void init() {

        setUpAppPreferences();

        if(savedUserModel != null) {
           if(savedUserModel.isRegistered()) {
               eventManager.fire(new LoggedInEvent(savedUserModel));
               return;
           } else {
               eventManager.fire(new GuestModeEvent(savedUserModel));
               return;

           }
        }

        loadTeamData();


        if (guestUser) {
            startGuestMode();
        } else if (user != null || pass != null) {
            startLoginMode();
        } else {
            startWelcomeMode();
        }
    }

    private void loadTeamData() {
        Cursor cursor = resolver.query(TeamContentProvider.CONTENT_URI, null, null, new String[]{"query"}, null);

        if (cursor == null) {
            loadTeams();
            //todo: reload teams preseason!
        } else {

        }
    }

    private void startWelcomeMode() {
        eventManager.fire(new WelcomeEvent());
    }

    public void startLoginMode() {
        Handler loginHandler = new Handler() {
            @Override
            public void handleMessage(Message message) {
                if (message.what == ApplicationMessages.ASYNCTASKSUCESS.ordinal()) {
                    IFanRantUserModel[] models = (FanRantUserModel[]) message.obj;
                    userModel = models[0];
                    eventManager.fire(new LoggedInEvent(userModel));
                }
            }
        };
        Map<String, String> postData = new HashMap<String, String>();
        postData.put("method", "getProfile");
        postData.put("username", user);
        postData.put("password", pass);

        FanRantAPIParams params = new FanRantAPIParams(FanRantAPIParams.ApiService.USER, FanRantUserResponse.class, postData, preferences);

        RoboAsyncTask t = preferences.getEnvironment() == Environments.TESTING ?
                new MockAsyncTask(getContext(), params, loginHandler) : new FanRantAsyncTask(getContext(), params, loginHandler);

        t.execute();
    }

    private String getTeamName(int team_id) {
        for (FanRantTeamsByCompetitonModel teamModel : teamModels) {
            for (TeamModel model : teamModel.getTeams()) {
                if(model.getid() == String.valueOf(team_id)) {
                    return model.getName();
                }
            }
        }
        return null;  //To change body of created methods use File | Settings | File Templates.
    }

    public void startGuestMode() {
        Map<String, String> postData = new HashMap<String, String>();
        postData.put("method", "getTeamData");
        postData.put("team_id", preferences.getPref(PreferenceConstants.UNREGTEAMID));

        FanRantAPIParams params = new FanRantAPIParams(FanRantAPIParams.ApiService.TEAM, FanRantTeamResponse.class, postData, preferences);

        userModel.setRegistered(false);
        userModel.setPassword("noreg");
        userModel.setTeam_id(Integer.parseInt(preferences.getPref(PreferenceConstants.UNREGTEAMID)));
        userModel.setCompetition_id( preferences.getPref(PreferenceConstants.UNREGCOMPID));




        Handler teamDataHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                if (msg.what == ApplicationMessages.ASYNCTASKSUCESS.ordinal()) {
                    if(msg.obj != null) {
                    FanRantTeamModel[] teamModels = (FanRantTeamModel[]) msg.obj;
                    userModel.setTeam_rss_feed(teamModels[0].getTeam_rss_feed());
                    userModel.setTeam_fixtures_url(teamModels[0].getTeam_fixtures_url());
                    userModel.setTeam_twitter_feed(teamModels[0].getTeam_twitter_feed());
                        userModel.setTeamName(teamModels[0].getTeam());
                    eventManager.fire(new GuestModeEvent(userModel));
                    } else {
                        eventManager.fire(new FanRantErrorEvent(new Exception("oh dear Fanrant needs a data connection to work ... please try later")));
                    }
                } else if (msg.what == ApplicationMessages.ASYNCTASKFAILED.ordinal()) {
                    eventManager.fire(new FanRantErrorEvent((Exception) msg.obj));
                }
            }
        };

        /**
         * do I really need todo this every init store team data locally?? speak to morgan on this
         */
        RoboAsyncTask task = preferences.getEnvironment() == Environments.TESTING ?
                new MockAsyncTask(getContext(), params, teamDataHandler) :
                new FanRantAsyncTask(getContext(), params, teamDataHandler);
        task.execute();

    }


    public void register(HashMap<String,String> postData) {
        postData.put("method", "register");


        Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                if (msg.what == ApplicationMessages.ASYNCTASKSUCESS.ordinal()) {
                    eventManager.fire(new FanRantUserRegisteredEvent((FanRantUserModel[]) msg.obj));
                } else if (msg.what == ApplicationMessages.ASYNCTASKFAILED.ordinal()) {
                    eventManager.fire(new FanRantErrorEvent((Exception) msg.obj));
                }
                super.handleMessage(msg);
            }
        };

        FanRantAPIParams params = new FanRantAPIParams(FanRantAPIParams.ApiService.USER, FanRantUserResponse.class, postData, preferences); //TODO: inject prefs
        new FanRantAsyncTask<FanRantModel>(getContext(), params, handler).execute();

    }

    private void setUpAppPreferences() {
        ObjectMapper mapper = new ObjectMapper();
        if(preferences.isPref(PreferenceConstants.SAVEDUSERMODEL)) {
            try {
                savedUserModel = mapper.readValue(preferences.getPref(PreferenceConstants.SAVEDUSERMODEL), FanRantUserModel.class);
            } catch (IOException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
        user = preferences.getPref(PreferenceConstants.USERNAME);
        pass = preferences.getPref(PreferenceConstants.PASSWORD);
        guestUser = Boolean.valueOf(preferences.getPref(PreferenceConstants.UNREGUSER));
    }

    public boolean isGuestUser() {
        return guestUser;
    }

    public void setGuestUser(boolean guestUser) {
        this.guestUser = guestUser;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }


    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public void loadTeams() {
        Map<String, String> postData = new HashMap<String, String>();
        postData.put("method", "getCompetitions");
        postData.put("show_teams", "1");

        Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                if (msg.what == ApplicationMessages.ASYNCTASKSUCESS.ordinal()) {
                    onTeamsLoaded((FanRantTeamsByCompetitonModel[]) msg.obj);
                } else if (msg.what == ApplicationMessages.ASYNCTASKFAILED.ordinal()) {
                    eventManager.fire(new FanRantErrorEvent((Exception) msg.obj));
                }
                super.handleMessage(msg);
            }
        };

        FanRantAPIParams params = new FanRantAPIParams(FanRantAPIParams.ApiService.DATA, FanRantTeamsByDivisionResponse.class, postData, preferences); //TODO: inject prefs
        new FanRantAsyncTask<FanRantModel>(getContext(), params, handler).execute();

    }

    public void onTeamsLoaded(FanRantTeamsByCompetitonModel[] model) {
        teamModels = model;
        FanRantDatabase db = new FanRantDatabase(getContext());
        db.addAllTeams(model);

        db = null;
    }


    public void login(String username, String password) {
        setUser(username);
        setPass(password);
        startLoginMode();
    }


    public void saveUserModelAsPref(IFanRantUserModel uModel) {
        ObjectMapper mapper = new ObjectMapper();

        try {
            String userAsJsonString = mapper.writeValueAsString(uModel);
            HashMap<String,String> pref = new HashMap<String, String>();
            pref.put(PreferenceConstants.SAVEDUSERMODEL,userAsJsonString);
            preferences.addPref(pref);
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

}
