package com.fanrant.app.controller;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import com.fanrant.app.Environments;
import com.fanrant.app.events.TVScheduleLoadedEvent;
import com.fanrant.app.model.FanRantModel;
import com.fanrant.app.modules.IPreferences;
import com.fanrant.app.modules.PreferenceConstants;
import com.fanrant.app.modules.api.FanRantAPIParams;
import com.fanrant.app.modules.api.FanRantAsyncTask;
import com.fanrant.app.modules.api.MockAsyncTask;
import com.fanrant.app.modules.api.responders.FanRantTeamResponse;
import com.fanrant.app.modules.api.responders.FanRantTvScheduleResponse;
import com.fanrant.app.modules.util.ApplicationMessages;
import com.google.inject.Inject;
import com.google.inject.Provider;
import roboguice.event.EventManager;
import roboguice.util.RoboAsyncTask;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: anthonygrimmitt
 * Date: 14/04/2013
 * Time: 13:35
 * To change this template use File | Settings | File Templates.
 */
public class TVScheduleController extends FanRantController {
    @Inject
    EventManager eventManager;
    @Inject
    IPreferences preferences;

    @Inject
    public TVScheduleController(Provider<Context> contextProvider) {
        super(contextProvider);
    }

    public void loadTvSchedule() {

        Map<String, String> postData = new HashMap<String, String>();
        postData.put("method", "getTVSchedule");

        FanRantAPIParams params = new FanRantAPIParams(FanRantAPIParams.ApiService.FIXTURE, FanRantTvScheduleResponse.class, postData, preferences);
        Handler tvScheduleHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                if(msg.what == ApplicationMessages.ASYNCTASKSUCESS.ordinal()) {
                    eventManager.fire(new TVScheduleLoadedEvent((FanRantModel[])msg.obj));}
            }
        };

        RoboAsyncTask task = preferences.getEnvironment() == Environments.TESTING ?
                new MockAsyncTask(getContext(), params,tvScheduleHandler) :
                new FanRantAsyncTask(getContext(), params, tvScheduleHandler);
        task.execute();
    }
}
