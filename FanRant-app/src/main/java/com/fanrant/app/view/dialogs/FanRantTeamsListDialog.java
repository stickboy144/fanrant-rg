package com.fanrant.app.view.dialogs;

import android.content.AsyncQueryHandler;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.BaseColumns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorTreeAdapter;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.SimpleCursorTreeAdapter;
import com.fanrant.R;
import com.fanrant.app.events.FanRantErrorEvent;
import com.fanrant.app.events.TeamSelectedEvent;
import com.fanrant.app.modules.db.FanRantDatabase;
import com.fanrant.app.modules.providers.TeamContentProvider;
import com.fanrant.app.view.dialogs.adapters.ExpandableListFragment;
import roboguice.event.EventManager;

public class FanRantTeamsListDialog extends ExpandableListFragment {

    static EventManager eventManager;
    private static final String[] COMP_PROJECTION = new String[]{
            "competition_id",
            "comp_name"
    };
    private static final int GROUP_ID_COLUMN_INDEX = 0;

    private static final String[] TEAM_PROJECTION = new String[]{
            "teamid", "team_name"
    };

    private static final int TOKEN_GROUP = 0;
    private static final int TOKEN_CHILD = 1;

    public FanRantTeamsListDialog(EventManager eventManager) {
        this.eventManager = eventManager;
    }


    public void close() {
        dismiss();
    }

    private static final class QueryHandler extends AsyncQueryHandler {
        private CursorTreeAdapter mAdapter;
        private FanRantTeamsListDialog dialog;

        public QueryHandler(Context context, CursorTreeAdapter adapter, FanRantTeamsListDialog dialog) {
            super(context.getContentResolver());
            this.mAdapter = adapter;
            this.dialog = dialog;
        }

        @Override
        protected void onQueryComplete(int token, Object cookie, Cursor cursor) {
            switch (token) {
                case TOKEN_GROUP:
                    mAdapter.setGroupCursor(cursor);
                    setupTeams(cursor);
                    break;

                case TOKEN_CHILD:
                    int groupPosition = (Integer) cookie;
                    mAdapter.setChildrenCursor(groupPosition, cursor);

                    break;
            }
        }

        private void setupTeams(Cursor cursor) {
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    do {

                        startQuery(TOKEN_CHILD, cursor.getPosition(), TeamContentProvider.DIVISIONS_URI, null,
                                null, new String[]{cursor.getString(cursor.getColumnIndexOrThrow(BaseColumns._ID))}, null);

                    } while (cursor.moveToNext());
                }
            } else {
                eventManager.fire(new FanRantErrorEvent(new Exception("Unable to retrieve teams, please check internet")));
                dialog.dismiss();
            }
        }

    }

    public class MyExpandableListAdapter extends SimpleCursorTreeAdapter {

        // Note that the constructor does not take a Cursor. This is done to avoid querying the
        // database on the main thread.
        public MyExpandableListAdapter(Context context, int groupLayout,
                                       int childLayout, String[] groupFrom, int[] groupTo, String[] childrenFrom,
                                       int[] childrenTo) {

            super(context, null, groupLayout, groupFrom, groupTo, childLayout, childrenFrom,
                    childrenTo);
            // Given the group, we return a cursor for all the children within that group

            // Return a cursor that points to this contact's phone numbers

        }

        @Override
        protected Cursor getChildrenCursor(Cursor groupCursor) {
            // Given the group, we return a cursor for all the children within that group

            mQueryHandler.startQuery(TOKEN_CHILD, groupCursor.getPosition(), TeamContentProvider.DIVISIONS_URI, null,
                    null, new String[]{groupCursor.getString(groupCursor.getColumnIndexOrThrow(BaseColumns._ID))}, null);

            return null;
        }
    }

    private QueryHandler mQueryHandler;
    private CursorTreeAdapter mAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Set up our adapter
        mAdapter = new MyExpandableListAdapter(
                getActivity(),
                android.R.layout.simple_expandable_list_item_2,
                android.R.layout.simple_expandable_list_item_2,
                new String[]{FanRantDatabase.COMPETITION_NAME}, // Name for group layouts
                new int[]{android.R.id.text1},
                new String[]{FanRantDatabase.TEAMNAME}, // Number for child layouts
                new int[]{android.R.id.text1});

        setListAdapter(mAdapter);

        mQueryHandler = new QueryHandler(getActivity(), mAdapter, this);

        // Query for people
        mQueryHandler.startQuery(TOKEN_GROUP, null, TeamContentProvider.DIVISION_NAMES_URI, null, null, new String[]{""}, null);

    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        getExpandableListView().setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView expandableListView, View view, int groupPosition, int childPosition, long id) {
                Cursor team = mAdapter.getChild(groupPosition, ++childPosition); // + 1  because the ids start at 1 no
                if (team.moveToFirst()) {
                    do {
                        String teamid = team.getString(team.getColumnIndexOrThrow(BaseColumns._ID));
                        String comp = team.getString(team.getColumnIndexOrThrow(FanRantDatabase.COMPETITION_ID));
                        String teamname = team.getString(team.getColumnIndexOrThrow(FanRantDatabase.TEAMNAME));

                        if (id == Long.valueOf(teamid)) {
                            eventManager.fire(new TeamSelectedEvent(teamid, comp, teamname));
                            dismiss();
                        }

                    } while (team.moveToNext());
                }
                return true;

            }
        });


        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onDestroy() {
        super.onDestroyView();

        // Null out the group cursor. This will cause the group cursor and all of the child cursors
        // to be closed.
        mAdapter.changeCursor(null);
        mAdapter = null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        LinearLayout view = (LinearLayout) inflater.inflate(R.layout.teampicker, container, false);

        return view;
    }
}