package com.fanrant.app.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.*;
import com.fanrant.R;
import com.fanrant.app.controller.UserController;
import com.fanrant.app.events.FanRantUserRegisteredEvent;
import com.fanrant.app.events.TeamSelectedEvent;
import com.fanrant.app.model.FanRantUserModel;
import com.fanrant.app.model.IFanRantUserModel;
import com.fanrant.app.view.dialogs.FanRantDialog;
import com.fanrant.app.view.dialogs.FanRantTeamsListDialog;
import com.google.inject.Inject;
import roboguice.event.EventManager;
import roboguice.event.Observes;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * User: Antwork
 * Date: 21/10/2012
 * Time: 16:18
 * To change this template use File | Settings | File Templates.
 */
@ContentView(R.layout.register)
public class FanRantRegisterActivity extends FanRantActivity implements FanRantDialog.FanRantDialogListener {
    @Inject
    EventManager eventManager;
    @Inject
    UserController userController;
    @Inject
    IFanRantUserModel userModel;
    @InjectView(R.id.pickteam)
    Button selectTeamButton;

    private FanRantTeamsListDialog tlf;
    private TeamSelectedEvent teamSelected;
    private EditText pass;


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("register");

        ImageView registerImageView = (ImageView) findViewById(R.id.regsubmit);

        final EditText email = (EditText) findViewById(R.id.regEmail);
        final EditText user = (EditText) findViewById(R.id.regUser);
         pass = (EditText) findViewById(R.id.regPass);
        final EditText confpass = (EditText) findViewById(R.id.regConfPass);


        registerImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!confpass.getText().toString().equals(pass.getText().toString())) {
                    showFanRantDialog(R.string.passwordmismatch, false);
                } else if(teamSelected == null) {
                    showFanRantDialog(R.string.selteam,false);
                }
                else {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("email", email.getText().toString());
                params.put("user_name", user.getText().toString());
                params.put("password1", pass.getText().toString());
                params.put("password2", confpass.getText().toString());
                params.put("team_id", teamSelected.getTeamid());

                userController.register(params);
                }
            }
        });
        selectTeamButton = (Button) findViewById(R.id.pickteam);

        tlf = new FanRantTeamsListDialog(eventManager);
        selectTeamButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tlf.show(getSupportFragmentManager(), "teams list");
            }

        });

        teamSelected = new TeamSelectedEvent(String.valueOf(userModel.getTeam_id()), userModel.getCompetition_id(), userModel.getTeamName());

        if(teamSelected.getTeamname() != null) {

            eventManager.fire(teamSelected);

        }
    }

    @Override
    public void onDialogPositiveClick(String
                                              dialog) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialog) {
        //To change body of implemented methods use File | Settings | File Templates.
    }


    public void teamSelected(@Observes TeamSelectedEvent teamSelectedEvent) {
        selectTeamButton.setText("My Team: " + teamSelectedEvent.getTeamname());
        teamSelected = teamSelectedEvent;

    }

    public void userRegistered(@Observes FanRantUserRegisteredEvent fanRantUserRegisterEvent) {
        preferences.clear();
        userModel.populate((FanRantUserModel) fanRantUserRegisterEvent.getModel());
        userModel.setLoggedIn(true);
        userModel.setPassword(pass.getText().toString());
        userModel.setRegistered(true);
        userController.saveUserModelAsPref(userModel);
        Intent intent = new Intent();

        intent.setClassName("com.fanrant", FanRantMainActivity.class.getName());
        startActivity(intent);
        overridePendingTransition(R.anim.slidein, 0);
        finish();

    }



    protected void initUserMode(IFanRantUserModel userModel) {


    }

}