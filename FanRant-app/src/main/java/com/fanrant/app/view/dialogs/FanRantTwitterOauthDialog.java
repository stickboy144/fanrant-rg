package com.fanrant.app.view.dialogs;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.*;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.fanrant.R;
import com.fanrant.app.events.FanRantTwitterTimeOutEvent;
import com.fanrant.app.modules.twitter.ITwitterUtils;
import com.fanrant.app.modules.twitter.TwitterConstants;
import com.fanrant.app.modules.util.ApplicationMessages;
import com.google.inject.Inject;
import oauth.signpost.OAuthConsumer;
import oauth.signpost.OAuthProvider;
import oauth.signpost.commonshttp.CommonsHttpOAuthConsumer;
import oauth.signpost.commonshttp.CommonsHttpOAuthProvider;
import oauth.signpost.exception.OAuthCommunicationException;
import oauth.signpost.exception.OAuthExpectationFailedException;
import oauth.signpost.exception.OAuthMessageSignerException;
import oauth.signpost.exception.OAuthNotAuthorizedException;
import roboguice.event.EventManager;
import roboguice.fragment.RoboDialogFragment;

import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * Created by anthonygrimmitt on 18/05/2013.
 */
public class FanRantTwitterOauthDialog extends RoboDialogFragment {

    private static final String TAG = "FRTWITTER";
    private static final int SETURL = 41220;
    private WebView webViewOauth;

    @Inject
    ITwitterUtils twitterUtils;

    @Inject
    EventManager eventManager;

    Handler handler;

    public FanRantTwitterOauthDialog(Handler handler) {
        this.handler = handler;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String uri) {
            //check if the login was successful and the access token returned
            //this test depend of your API
            if (uri.contains(TwitterConstants.OAUTH_CALLBACK_URL)) {
                //save your token
                twitterUtils.retrieveAccessToken(handler, Uri.parse(uri));
                dismiss();
                return true;
            }
            return false;
        }
    }

    private void saveAccessToken(String uri) {

    }

    @Override
    public void onViewCreated(View arg0, Bundle arg1) {
        super.onViewCreated(arg0, arg1);

        try {
            Handler handler = new Handler() {
                @Override
                public void handleMessage(Message msg) {
                    if (msg.what == SETURL) {
                        webViewOauth.loadUrl((String) msg.obj);
                    }
                }
            };
            new OAuthRequestTokenTask(getActivity(), twitterUtils.getConsumer(), twitterUtils.getProvider(), handler).execute().get(30, TimeUnit.SECONDS);

            //set the web client
            webViewOauth.setWebViewClient(new MyWebViewClient());
            //activates JavaScript (just in case)
            WebSettings webSettings = webViewOauth.getSettings();
            webSettings.setJavaScriptEnabled(true);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            eventManager.fire(new FanRantTwitterTimeOutEvent());
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //Retrieve the webview
        View v = inflater.inflate(R.layout.oauthdialog, container, false);
        webViewOauth = (WebView) v.findViewById(R.id.web_oauth);
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        return v;
    }

    public class OAuthRequestTokenTask extends AsyncTask<Void, String, String> {

        final String TAG = getClass().getName();
        private Context context;
        private OAuthProvider provider;
        private Handler handler;
        private OAuthConsumer consumer;

        /**
         * We pass the OAuth consumer and provider.
         *
         * @param context  Required to be able to start the intent to launch the browser.
         * @param provider The OAuthProvider object
         * @param consumer The OAuthConsumer object
         */
        public OAuthRequestTokenTask(Context context, OAuthConsumer consumer, OAuthProvider provider, Handler handler) {
            this.context = context;
            this.consumer = consumer;
            this.provider = provider;
            this.handler = handler;
        }

        /**
         * Retrieve the OAuth Request Token and present a browser to the user to authorize the token.
         */
        @Override
        protected String doInBackground(Void... v) {

            try {
                Log.i(TAG, "Retrieving request token from Google servers");
               String url = provider.retrieveRequestToken(consumer, TwitterConstants.OAUTH_CALLBACK_URL);
                return url;
            } catch (Exception e) {
                Log.e(TAG, "Error during OAUth retrieve request token", e);
                return  null;
            }

        }

        @Override
        protected void onPostExecute(String s) {
            handler.sendMessage(handler.obtainMessage(SETURL, s));
            super.onPostExecute(s);
        }
    }
}
