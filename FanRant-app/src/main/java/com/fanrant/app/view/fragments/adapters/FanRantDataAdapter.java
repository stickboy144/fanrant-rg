package com.fanrant.app.view.fragments.adapters;

import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.fanrant.FanRantApp;
import com.fanrant.R;
import com.fanrant.app.model.FanRantDataModel;
import com.fanrant.app.modules.util.AvatarLoaderTask;
import com.fanrant.app.modules.util.CacheStore;
import com.fanrant.app.modules.util.FeedType;
import com.fanrant.app.view.FanRantMainActivity;
import com.novoda.imageloader.core.ImageManager;
import com.novoda.imageloader.core.model.ImageTag;
import com.novoda.imageloader.core.model.ImageTagFactory;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created with IntelliJ IDEA.
 * User: Antwork
 * Date: 04/11/2012
 * Time: 15:23
 * To change this template use File | Settings | File Templates.
 */
public class FanRantDataAdapter extends ArrayAdapter {
    private final ImageTagFactory imageTagFactory;
    private FanRantMainActivity context;
    private LayoutInflater inflater;
    private CacheStore cache = CacheStore.getInstance();
    private FanRantDataModel[] data;
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
    private ImageManager imageManager;


    public FanRantDataAdapter(FanRantMainActivity context, int resource, FanRantDataModel[] objects, LayoutInflater inflater) {
        super(context, resource, objects);
        this.context = context;
        this.data = objects;
        this.inflater = inflater;
        imageTagFactory = ImageTagFactory.newInstance(context, R.drawable.profile);

        initImageLoader();
    }

    private void initImageLoader() {
        imageManager = FanRantApp.getImageManager();
        imageTagFactory.setErrorImageId(R.drawable.profile);
        imageTagFactory.setSaveThumbnail(true);
//        context.setAnimationFromIntent(imageTagFactory);
    }



    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewContainer viewContainer;

        FanRantDataModel model = data[position];

        convertView = inflater.inflate(R.layout.datarow, null);
        viewContainer = new ViewContainer(convertView);
        viewContainer.getComment().setText(model.comment.toString());
        if (model.getType() != FeedType.TEAMNEWS) {
            viewContainer.getUsername().setText(model.getUsername());
        } else {
            viewContainer.getUsername().setText(model.getLink());
        }
        try {
            System.out.println("model = " + model.post_time.toString());
            Date date = sdf.parse(model.post_time.toString());
            System.out.println("date.toString() = " + date.toString());
            Date now = new Date(System.currentTimeMillis());
            long timeDiff = now.getTime() - date.getTime();
            String time = "unavailable";
            if (TimeUnit.DAYS.convert(timeDiff, TimeUnit.MILLISECONDS) > 0) {
                long res = TimeUnit.DAYS.convert(timeDiff, TimeUnit.MILLISECONDS);
                if (res == 1) {
                    time = String.valueOf(res) + " Day";
                } else {
                    time = String.valueOf(res) + " Days";
                }

            } else if (TimeUnit.HOURS.convert(timeDiff, TimeUnit.MILLISECONDS) > 0) {
                long res = TimeUnit.HOURS.convert(timeDiff, TimeUnit.MILLISECONDS);
                if (res == 1) {
                    time = String.valueOf(res) + " Hr";
                } else {
                    time = String.valueOf(res) + " Hrs";
                }

            } else if (TimeUnit.MINUTES.convert(timeDiff, TimeUnit.MILLISECONDS) > 0) {
                long res = TimeUnit.MINUTES.convert(timeDiff, TimeUnit.MILLISECONDS);
                if (res == 1) {
                    time = String.valueOf(res) + " Min";
                } else {
                    time = String.valueOf(res) + " Mins";
                }

            } else if (TimeUnit.SECONDS.convert(timeDiff, TimeUnit.MILLISECONDS) > 0) {
                long res = TimeUnit.SECONDS.convert(timeDiff, TimeUnit.MILLISECONDS);
                if (res == 1) {
                    time = String.valueOf(res) + " Sec";
                } else {
                    time = String.valueOf(res) + " Secs";
                }

            }
            viewContainer.getTime().setText(time);
        } catch (ParseException e) {
            context.handleException(e);  //To change body of catch statement use File | Settings | File Templates.
        }

        switch (model.type) {

            case COMMENTS:
                viewContainer.getRowLayout().setBackgroundDrawable(null);

                if (model.avatar != null) {
                    setImageTag(viewContainer.getAvatar(), model.avatar);
                    loadImage(viewContainer.getAvatar());
                } else {
                    viewContainer.getAvatar().setImageResource(R.drawable.profile);
                }
                break;
            case TEAMNEWS:
                viewContainer.getAvatar().setImageResource(R.drawable.btn_news);
                break;
            case FIXTURES:

                break;
            case TWITTER:
                viewContainer.getAvatar().setImageResource(R.drawable.twitter);
                break;
        }
        return convertView;
    }


    private void setImageTag(ImageView view, String url) {
        view.setTag(imageTagFactory.build(url, context));
    }

    private void loadImage(ImageView view) {
        imageManager.getLoader().load(view);
    }


    @Override
    public void notifyDataSetChanged() {

        super.notifyDataSetChanged();
    }

    public FanRantDataModel[] getData() {
        return data;
    }

    public void setData(FanRantDataModel[] data) {
        this.data = data;
    }

    private class ViewContainer {
        private View row;

        private TextView comment;

        private LinearLayout rowLayout;

        private TextView username;
        private TextView time;

        private TextView player;

        private TextView rating;

        private ImageView avatar;


        public ViewContainer(View row) {
            this.row = row;
        }

        public TextView getComment() {
            if (comment == null) {
                comment = (TextView) row.findViewById(R.id.latestcomment);
            }
            return comment;
        }

        public TextView getUsername() {
            if (username == null) {
                username = (TextView) row.findViewById(R.id.latestuser);
            }
            return username;
        }

        public TextView getTime() {
            if (time == null) {
                time = (TextView) row.findViewById(R.id.latesttime);
            }

            return time;
        }

        public ImageView getAvatar() {
            if (avatar == null) {
                avatar = (ImageView) row.findViewById(R.id.latestavatar);
            }
            return avatar;
        }

        public LinearLayout getRowLayout() {
            if (rowLayout == null) {
                rowLayout = (LinearLayout) row.findViewById(R.id.rowlayout);
            }
            return rowLayout;
        }

        public TextView getPlayer() {
            if (player == null) {
                player = (TextView) row.findViewById(R.id.latestplayer);
            }
            return player;
        }

        public TextView getRating() {
            if (rating == null) {
                rating = (TextView) row.findViewById(R.id.latestratevalue);
            }
            return rating;
        }
    }
}
