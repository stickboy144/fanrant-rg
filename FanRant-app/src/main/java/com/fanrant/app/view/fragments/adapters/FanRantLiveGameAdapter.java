package com.fanrant.app.view.fragments.adapters;

import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.fanrant.FanRantApp;
import com.fanrant.R;
import com.fanrant.app.mod.FanRantGameLatestUpdateModel;
import com.fanrant.app.model.FanRantGameUpdateModel;
import com.fanrant.app.model.FanRantGameUpdatePlayerModel;
import com.fanrant.app.model.FanRantPlayerModel;
import com.fanrant.app.modules.util.CacheStore;
import com.fanrant.app.modules.util.FanRantScoreListener;
import com.fanrant.app.modules.util.Frants;
import com.fanrant.app.modules.util.GameEvent;
import com.fanrant.app.view.FanRantMainActivity;
import com.fanrant.app.modules.util.AvatarLoaderTask;
import com.novoda.imageloader.core.ImageManager;
import com.novoda.imageloader.core.model.ImageTagFactory;
import org.apache.commons.lang3.StringEscapeUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created with IntelliJ IDEA.
 * User: Antwork
 * Date: 04/11/2012
 * Time: 15:23
 * To change this template use File | Settings | File Templates.
 */
public class FanRantLiveGameAdapter extends ArrayAdapter {
    private final ImageTagFactory imageTagFactory;
    private FanRantMainActivity context;
    private LayoutInflater inflater;
    private CacheStore cache = CacheStore.getInstance();
    private FanRantGameUpdateModel[] data;
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    private SimpleDateFormat sdf2 = new SimpleDateFormat("hh:mm");
    private ImageManager imageManager;


    public FanRantLiveGameAdapter(FanRantMainActivity context, int resource, FanRantGameUpdateModel[] objects) {
        super(context, resource,
                 objects);
        this.context = context;
        this.data = objects;
        this.inflater = context.getLayoutInflater();
        imageTagFactory = ImageTagFactory.newInstance(context, R.drawable.profile);

    }

    private void initImageLoader() {
        imageManager = FanRantApp.getImageManager();
        imageTagFactory.setErrorImageId(R.drawable.profile);
        imageTagFactory.setSaveThumbnail(true);
//        context.setAnimationFromIntent(imageTagFactory);
    }





    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewContainer viewContainer;

        FanRantGameUpdateModel model = (FanRantGameUpdateModel) getItem(position);

        if(model.getPost_type().equals("rating")) {
            convertView = inflater.inflate(R.layout.liveratingrow, null);
        } else {
        convertView = inflater.inflate(R.layout.livegamerow, null);
        }
        viewContainer = new ViewContainer(convertView);

        if(model.getMatch_time() == null) {
            model.setMatch_time("FT");
        }
        if(model.getComment() != null && (model.getPost_type().equals("rating") || model.getPost_type().equals("comment"))) {
            viewContainer.getComment().setText(StringEscapeUtils.unescapeJava(model.getComment()));
        } else if(model.getComment() != null ) {
            viewContainer.getComment().setText(model.getMatch_time() + " - " + StringEscapeUtils.unescapeJava(model.getComment()));
        } else {
            viewContainer.getComment().setText(model.getMatch_time());
        }
        viewContainer.getUsername().setText("By " + model.getUsername());

        try {
            System.out.println("model = " + model.getPost_time());
            Date date = sdf.parse(model.getPost_time());
            System.out.println("date.toString() = " + date.toString());
            Date now = new Date(System.currentTimeMillis());
            long timeDiff = now.getTime() - date.getTime();
            String time = "unavailable";
            if (TimeUnit.DAYS.convert(timeDiff, TimeUnit.MILLISECONDS) > 0) {
                long res = TimeUnit.DAYS.convert(timeDiff, TimeUnit.MILLISECONDS);
                if (res == 1) {
                    time = String.valueOf(res) + " Day";
                } else {
                    time = String.valueOf(res) + " Days";
                }

            } else if (TimeUnit.HOURS.convert(timeDiff, TimeUnit.MILLISECONDS) > 0) {
                long res = TimeUnit.HOURS.convert(timeDiff, TimeUnit.MILLISECONDS);
                if (res == 1) {
                    time = String.valueOf(res) + " Hr";
                } else {
                    time = String.valueOf(res) + " Hrs";
                }

            } else if (TimeUnit.MINUTES.convert(timeDiff, TimeUnit.MILLISECONDS) > 0) {
                long res = TimeUnit.MINUTES.convert(timeDiff, TimeUnit.MILLISECONDS);
                if (res == 1) {
                    time = String.valueOf(res) + " Min";
                } else {
                    time
                            = String.valueOf(res) + " Mins";
                }

            } else if (TimeUnit.SECONDS.convert(timeDiff, TimeUnit.MILLISECONDS) > 0) {
                long res = TimeUnit.SECONDS.convert(timeDiff, TimeUnit.MILLISECONDS);
                if (res == 1) {
                    time = String.valueOf(res) + " Sec";
                } else {
                    time = String.valueOf(res) + " Secs";
                }

            } else {
                time = sdf2.format(date);
            }
            viewContainer.getTime().setText(time);
        } catch (ParseException e) {
            context.handleException(e);  //To change body of catch statement use File | Settings | File Templates.
        }

        if (model.getPost_type().equals("event")) {
            switch (GameEvent.fromString(model.getSubject())) {
                case ATTENDANCE:
                    viewContainer.getAvatar().setImageResource(R.drawable.frant);
                    break;
                case EXTRAFIRSTHALF:
                    viewContainer.getAvatar().setImageResource(R.drawable.frant);
                    break;
                case EXTRASECONDHALF:
                    viewContainer.getAvatar().setImageResource(R.drawable.frant);
                    break;
                case FIRSTHALF:
                    viewContainer.getAvatar().setImageResource(R.drawable.frant);
                    break;
                case FULLTIME:
                    viewContainer.getAvatar().setImageResource(R.drawable.frant);
                    break;
                case GOAL:
                    FanRantGameUpdatePlayerModel player = (FanRantGameUpdatePlayerModel)model.getPlayers().toArray()[0];
                    StringBuilder sb = new StringBuilder(viewContainer.getComment().getText());
                    sb.append(StringEscapeUtils.unescapeJava(" " + player.getLast_name() + " (" + player.getTeam_name() + ")"));
                    viewContainer.getAvatar().setImageResource(R.drawable.goal);
                    viewContainer.getComment().setText(sb);
                    break;
                case HALFTIME:
                    viewContainer.getAvatar().setImageResource(R.drawable.frant);
                    break;
                case INJURY:
                    viewContainer.getAvatar().setImageResource(R.drawable.frant);
                    break;
                case MISSEDPENALTY:
                    viewContainer.getAvatar().setImageResource(R.drawable.penalty);
                    break;
                case PENALTY:
                    viewContainer.getAvatar().setImageResource(R.drawable.penalty);
                    break;
                case PENALTYSHOT:
                    viewContainer.getAvatar().setImageResource(R.drawable.penalty);
                    break;
                case POSTPONEMENT:
                    viewContainer.getAvatar().setImageResource(R.drawable.frant);
                    break;
                case PREMATCH:
                    viewContainer.getAvatar().setImageResource(R.drawable.frant);
                    break;
                case PREMATCHAWAYLINEUP:
                    viewContainer.getAvatar().setImageResource(R.drawable.frant);
                    break;
                case PREMATCHAWAYSUBS:
                    viewContainer.getAvatar().setImageResource(R.drawable.frant);
                    break;
                case PREMATCHHOMELINEUP:
                    viewContainer.getAvatar().setImageResource(R.drawable.frant);
                    break;
                case PREMATCHHOMESUBS:
                    viewContainer.getAvatar().setImageResource(R.drawable.frant);
                    break;
                case REDCARD:
                    player = (FanRantGameUpdatePlayerModel)model.getPlayers().toArray()[0];
                    sb = new StringBuilder(viewContainer.getComment().getText());
                    sb.append(StringEscapeUtils.unescapeJava(" " + player.getLast_name() + " (" + player.getTeam_name() + ")"));
                    viewContainer.getAvatar().setImageResource(R.drawable.red_card);
                    break;
                case SCORELINE:
                    player = (FanRantGameUpdatePlayerModel)model.getPlayers().toArray()[0];
                    sb = new StringBuilder(viewContainer.getComment().getText());
                    sb.append(StringEscapeUtils.unescapeJava(" " + player.getLast_name() + " (" + player.getTeam_name() + ")"));
                    viewContainer.getComment().setText(sb);
                    viewContainer.getAvatar().setImageResource(R.drawable.frant);
//                    scoreListener.score(model.getComment());
                    break;
                case SECONDYELLOWCARD:
                    viewContainer.getAvatar().setImageResource(R.drawable.yellow_card);
                    break;
                case SECONDHALF:
                    viewContainer.getAvatar().setImageResource(R.drawable.frant);
                    break;
                case SHOOTOUT:
                    viewContainer.getAvatar().setImageResource(R.drawable.frant);
                    break;
                case STATUS:
                    viewContainer.getAvatar().setImageResource(R.drawable.frant);
                    break;
                case SUBSTITUTION:
                    //todo fix subs
                    player = (FanRantGameUpdatePlayerModel)model.getPlayers().toArray()[0];
                    sb = new StringBuilder(viewContainer.getComment().getText());
                    sb.append(StringEscapeUtils.unescapeJava(" " + player.getLast_name() + " (" + player.getTeam_name() + ")"));
                    viewContainer.getComment().setText(sb);
                    viewContainer.getAvatar().setImageResource(R.drawable.frant);
                    break;
                case YELLOWCARD:
                    player = (FanRantGameUpdatePlayerModel)model.getPlayers().toArray()[0];
                    sb = new StringBuilder(viewContainer.getComment().getText());
                    sb.append(StringEscapeUtils.unescapeJava(" " + player.getLast_name() + " (" + player.getTeam_name() + ")"));
                    viewContainer.getAvatar().setImageResource(R.drawable.yellow_card);
                    viewContainer.getComment().setText(sb);
                    break;
            }
        } else if (model.getPost_type().equals("rating")) {
//            if(m.get)
            if (model.getPlayers().size() > 0) {
                FanRantGameUpdatePlayerModel player = (FanRantGameUpdatePlayerModel)model.getPlayers().toArray()[0];
                StringBuilder sb = new StringBuilder();
                if (player.getTeam_name() != null) {
                    sb.append(StringEscapeUtils.unescapeJava( player.getLast_name()));
                } else {
                    sb.append(StringEscapeUtils.unescapeJava(player.getLast_name()));

                }
//                viewContainer.getComment().setText(sb);
                viewContainer.getPlayer().setText(sb);
                viewContainer.getRating().setText(model.getRating());
            }
            switch (Frants.fromString(model.getSubject())) {

                case COMMENT:
                    setImageTag(viewContainer.getAvatar(), model.getAvatar());
                    loadImage(viewContainer.getAvatar());
                    break;
                case YELLOWCARD:
                    viewContainer.getAvatar().setImageResource(R.drawable.frant_yellow);
                    break;
                case REDCARD:
                    viewContainer.getAvatar().setImageResource(R.drawable.frant_red);
                    break;
                case GOAL:
                    viewContainer.getAvatar().setImageResource(R.drawable.frant_goal);
                    break;
                case PENALTY:
                    viewContainer.getAvatar().setImageResource(R.drawable.frant_penalty);
                    break;
                case DIVE:
                    viewContainer.getAvatar().setImageResource(R.drawable.frant_dive);
                    break;
                case ONFIRE:
                    viewContainer.getAvatar().setImageResource(R.drawable.frant_fire);
                    break;
                case WHATADONKEY:
                    viewContainer.getAvatar().setImageResource(R.drawable.frant_donkey);
                    break;
                case PLAYACTOR:
                    viewContainer.getAvatar().setImageResource(R.drawable.frant_actor);
                    break;
                case CHOPPER:
                    viewContainer.getAvatar().setImageResource(R.drawable.frant_chopper);
                    break;
                case OFFSIDE:
                    viewContainer.getAvatar().setImageResource(R.drawable.frant_offside);
                    break;
                case THEREFSBLIND:
                    viewContainer.getAvatar().setImageResource(R.drawable.frant_blind);
                    break;
            }
        } else if (model.getPost_type().equals("card")) {
            if (model.getSubject().equals("yellow")) {
                FanRantGameUpdatePlayerModel player = (FanRantGameUpdatePlayerModel)model.getPlayers().toArray()[0];
                StringBuilder sb = new StringBuilder(viewContainer.getComment().getText());
                sb.append(StringEscapeUtils.unescapeJava(" " + player.getLast_name() + " (" + player.getTeam_name() + ")"));
                viewContainer.getAvatar().setImageResource(R.drawable.yellow_card);
                viewContainer.getComment().setText(sb);

            } else  {
                FanRantGameUpdatePlayerModel player = (FanRantGameUpdatePlayerModel)model.getPlayers().toArray()[0];
                StringBuilder sb = new StringBuilder(viewContainer.getComment().getText());
                sb.append(StringEscapeUtils.unescapeJava(" " + player.getLast_name() + " (" + player.getTeam_name() + ")"));
                viewContainer.getAvatar().setImageResource(R.drawable.red_card);
                viewContainer.getComment().setText(sb);

            }
        } else if(model.getPost_type().equals("comment")) {
            Bitmap img = cache.getCacheFile(model.getAvatar());
            if (img == null) {
                AvatarLoaderTask imageLoader = new AvatarLoaderTask(viewContainer.getAvatar());
                imageLoader.execute(model.getAvatar());
            } else {
                viewContainer.getAvatar().setImageBitmap(img);
            }

        }

        convertView.setOnClickListener(getShareAction(position));
        return convertView;
    }

    @Override
    public void notifyDataSetChanged() {

        super.notifyDataSetChanged();
    }

    public FanRantGameUpdateModel[] getData() {
        return data;
    }

    public void setData(FanRantGameUpdateModel[] data) {
        this.data = data;
    }


    private void setImageTag(ImageView view, String url) {
        view.setTag(imageTagFactory.build(url, context));
    }

    private void loadImage(ImageView view) {
        imageManager.getLoader().load(view);
    }


    public View.OnClickListener getShareAction(final int childPosition) {
        return null;
    }

    private class ViewContainer {
        private View row;

        private TextView comment;

        private LinearLayout rowLayout;

        private TextView username;
        private TextView time;

        private ImageView avatar;
        private TextView player;
        private TextView rating;


        public ViewContainer(View row) {
            this.row = row;
        }

        public TextView getComment() {
            if (comment == null) {
                comment = (TextView) row.findViewById(R.id.latestcomment);
            }
            return comment;
        }

        public TextView getUsername() {
            if (username == null) {
                username = (TextView) row.findViewById(R.id.latestuser);
            }
            return username;
        }

        public TextView getTime() {
            if (time == null) {
                time = (TextView) row.findViewById(R.id.latesttime);
            }

            return time;
        }

        public ImageView getAvatar() {
            if (avatar == null) {
                avatar = (ImageView) row.findViewById(R.id.latestavatar);
            }
            return avatar;
        }

        public LinearLayout getRowLayout() {
            if (rowLayout == null) {
                rowLayout = (LinearLayout) row.findViewById(R.id.rowlayout);
            }
            return rowLayout;
        }

        public TextView getPlayer() {
            if(player == null) {
                player = (TextView) row.findViewById(R.id.latestplayer);
            }
            return player;
        }

        public TextView getRating() {
            if(rating == null) {
                rating = (TextView) row.findViewById(R.id.latestratevalue);
            }
            return rating;
        }
    }
}
