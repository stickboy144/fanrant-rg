package com.fanrant.app.view;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.widget.TextView;
import com.appblade.framework.AppBlade;
import com.fanrant.R;
import com.fanrant.app.events.FanRantErrorEvent;
import com.fanrant.app.events.FanRantTweetEvent;
import com.fanrant.app.events.FanRantTwitterFailedEvent;
import com.fanrant.app.events.FanRantTwitterTimeOutEvent;
import com.fanrant.app.modules.IPreferences;
import com.fanrant.app.modules.services.IFanRantServiceManager;
import com.fanrant.app.modules.util.*;
import com.fanrant.app.view.dialogs.FanRantDialog;
import com.github.rtyley.android.sherlock.roboguice.activity.RoboSherlockFragmentActivity;
import com.google.inject.Inject;
import roboguice.event.EventManager;
import roboguice.event.Observes;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Antwork
 * Date: 13/10/2012
 * Time: 19:07
 * To change this template use File | Settings | File Templates.
 */
public class FanRantActivity extends RoboSherlockFragmentActivity implements FanRantDialog.FanRantDialogListener {
    @Inject
    EventManager eventManager;

    @Inject
    IPreferences preferences;

    public static final String FANRANTLOG = "Fan Rant Log ::";

    private List<CustomNamedHandler> handlers = new ArrayList<CustomNamedHandler>();

    private ArrayList<CustomTimeNamedHandler> timers = new ArrayList<CustomTimeNamedHandler>();
    private SharedPreferences mPrefs;
    private TextView windowTitle;
    private boolean windowLoader;

    IFanRantServiceManager serviceManager;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread thread, Throwable ex) {
                if(getServiceManager() != null) {
                    getServiceManager().stopAllServices();
                }

                AppBlade.notify(ex);
                error("exception thrown " + ex.getMessage());
                for (StackTraceElement stackTraceElement : ex.getStackTrace()) {
                    error(stackTraceElement.toString());
                }
            }

        });
    }

    @Override
    protected void onResume() {

        super.onResume();
//        AppBlade.authorize(this);
    }


    public void showFanRantDialog(int message, boolean cancelable) {
        DialogFragment dialogFragment = new FanRantDialog().newInstance(this, getApplicationContext().getString(message), cancelable, DialogType.ERROR) ;
        dialogFragment.show(getSupportFragmentManager(), "Error Message");

    }

    public void showFanRantDialog(int message, boolean cancelable,boolean okAction) {
        if(okAction) {
        DialogFragment dialogFragment = new FanRantDialog().newInstance(this, getApplicationContext().getString(message), cancelable, DialogType.ERROR) ;
        dialogFragment.show(getSupportFragmentManager(), "Error Message");
        } else {
            DialogFragment dialogFragment = new FanRantDialog().newInstance(this, getApplicationContext().getString(message), cancelable, DialogType.WARN) ;
            dialogFragment.show(getSupportFragmentManager(), "Error Message");

        }

    }

    public void registerHandlers(CustomNamedHandler handler) {
        handlers.add(handler);
    }

    public Handler getHandler(Handlers handler) {
        Handler h = null;
        for (CustomNamedHandler customNamedHandler : handlers) {
            if (customNamedHandler.getHandlerName() == handler) {
                h = customNamedHandler;
            }
        }
        return h;
    }

    FanRantDialog progressDialog = FanRantDialog.newInstance(this, null, false, DialogType.PROGRESS);

    public FanRantDialog showProgessBar() {

        if(progressDialog.getDialog() == null) {
        progressDialog.show(getSupportFragmentManager(),"progess");
        }
        return progressDialog;
    }

    public void hideProgessBar() {
        if(progressDialog.getDialog() != null) {
            progressDialog.dismiss();
        }
    }

    public String    getPreference(String pref) {
        if (mPrefs == null) {

            mPrefs = getSharedPreferences(getApplicationContext().getString(R.string.app_name), MODE_PRIVATE);
        }
        return mPrefs.getString(pref, null);
    }

    public void addSharedPref(Map<String, String> pref) {
        SharedPreferences.Editor editor = mPrefs.edit();
        for (String key : pref.keySet()) {
            editor.putString(key, pref.get(key));
            editor.commit();
        }
    }

    public void clearSharedPref() {
        SharedPreferences.Editor editor = mPrefs.edit();
        editor.clear().commit();
    }

    public void deletePref(String pref) {

        SharedPreferences.Editor editor = mPrefs.edit();

        editor.remove(pref).commit();

    }

    public void clearFacebokSharedPref() {
        SharedPreferences.Editor editor = mPrefs.edit();
        editor.remove("facebook_id")
                .remove("facebook_access_token")
                .commit();
    }

    public void setWindowTitle(String title) {
        if (windowTitle == null) {
            windowTitle = (TextView) findViewById(R.id.windowTitle);
        }

//        windowTitle.setText(title);
    }

    public static void debug(String msg) {
        Log.d(FANRANTLOG, msg);

    }

    public static void error(String msg) {
        Log.e(FANRANTLOG, msg);
    }

    public static void verbose(String msg) {
        Log.v(FANRANTLOG, msg);
    }



    public void growBar() {
//        getSherlock().setContentView(R.);
//        View v = (View) getSherlock().getActionBar().getCustomView();
//        new DropDownAnim(v,500,true);
        setTheme(R.style.Theme_Fanrantls);
//        recreate();
    }

//    public void setMatchtime(String mt) {
//        if (titleTextView.getText().toString().contains("(")) {
//            int bPos = titleTextView.getText().toString().indexOf("(");
//
//            String orig = titleTextView.getText().toString().substring(0, bPos);
//            titleTextView.setText(orig + "(" + mt + ")");
//        }
//    }



    public void showFanRantDialog(int message, String message2, boolean cancel) {
        DialogFragment dialogFragment = new FanRantDialog().newInstance(this, getApplicationContext().getString(message) + " " + message2, cancel, DialogType.ERROR);
        dialogFragment.show(getSupportFragmentManager(), "Error Message");

    }

    public void showLiveGameScoreLine() {
//        Resources.Theme theme = getTheme();
//        standardTheme = theme;
//        getTheme().applyStyle(R.style.Theme_FanrantScore,true);
//        View v = getSherlock().getActionBar().getCustomView();
//        TableLayout tableLayout = (TableLayout) v.findViewById(R.id.windowscoreline);
//        tableLayout.setVisibility(View.VISIBLE);
    }

    public void handleException(Exception e) {
        error(e.getMessage());
        AppBlade.notify(e);
    }
    public void recreate() {
        //This SUCKS! Figure out a way to call the super method and support Android 1.6
        /*
        if (IS_HONEYCOMB) {
            super.recreate();
        } else {
        */
        final Intent intent = getIntent();
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

        startActivity(intent);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ECLAIR) {
//            OverridePendingTransition.invoke(this);
//        }
//
        finish();
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ECLAIR) {
//            OverridePendingTransition.invoke(this);
//        }
        /*
        }
        */
    }

    public void registerTimer(CustomTimeNamedHandler timer) {
        timers.add(timer);
    }

    public CustomTimeNamedHandler getTimer(Timers timer) {
        CustomTimeNamedHandler t = null;
        for (CustomTimeNamedHandler customTimeNamedHandler : timers) {
            if(customTimeNamedHandler.getTimerName() == timer) {
                t = customTimeNamedHandler;
            }
        }
        return t;
    }

    public void stopAllTimers() {
        for (CustomTimeNamedHandler customTimeNamedHandler : timers) {
            customTimeNamedHandler.stop();
        }
    }

    @Override
    public void onDialogPositiveClick(String error) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialog) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void tweet(@Observes FanRantTweetEvent tweetEvent) {
        FanRantDialog.newInstance(this, tweetEvent.getComment(), false, DialogType.TWEET).show(getSupportFragmentManager(),"tweet");
    }

    public void twitterAuthFailed(@Observes FanRantTwitterFailedEvent fanRantTwitterFailedEvent) {
        showFanRantDialog(R.string.twittererror,false);
    }

    public void twitterTimedOut(@Observes FanRantTwitterTimeOutEvent fanRantTwitterTimeOutEvent) {
        showFanRantDialog(R.string.twittertimedout,false);
    }

    public void onError(@Observes FanRantErrorEvent errorEvent) {
        showFanRantDialog(R.string.erroroccured,errorEvent.getException().getMessage(),false);
    }


    public IFanRantServiceManager getServiceManager() {
        return serviceManager;
    }

    public void setServiceManager(IFanRantServiceManager serviceManager) {
        this.serviceManager = serviceManager;
    }
}
