package com.fanrant.app.view.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.ResultReceiver;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.facebook.widget.LoginButton;
import com.fanrant.R;
import com.fanrant.app.controller.PostController;
import com.fanrant.app.events.FanRantFacebookEnableEvent;
import com.fanrant.app.events.FanRantFacebookPostedEvent;
import com.fanrant.app.events.TwitterEnableEvent;
import com.fanrant.app.model.FanRantFixtureModel;
import com.fanrant.app.model.FanRantGameUpdateModel;
import com.fanrant.app.modules.db.FanRantDatabase;
import com.fanrant.app.modules.services.FanRantGameUpdatesService;
import com.fanrant.app.modules.services.FanRantLiveFixturesService;
import com.fanrant.app.modules.services.FanRantService;
import com.fanrant.app.modules.twitter.ITwitterUtils;
import com.fanrant.app.modules.util.*;
import com.fanrant.app.view.FanRantMainActivity;
import com.fanrant.app.view.dialogs.FanRantDialog;
import com.fanrant.app.view.fragments.adapters.FanRantLiveGameAdapter;
import com.fanrant.app.view.fragments.adapters.FanRantLiveScoresAdapter;
import com.fanrant.app.view.menu.FanRantMenuItem;
import com.github.rtyley.android.sherlock.roboguice.fragment.RoboSherlockFragment;
import com.google.inject.Inject;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import roboguice.event.EventManager;
import roboguice.event.Observes;
import roboguice.inject.InjectView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

//import com.facebook.widget.LoginButton;

/**
 * Created with IntelliJ IDEA.
 * User: Antwork
 * Date: 30/10/2012
 * Time: 19:00
 * To change this template use File | Settings | File Templates.
 */
public class FanRantLiveScores extends RoboSherlockFragment implements FanRantScoreListener, TextWatcher {

    @Inject
    ITwitterUtils twitterUtils;

    @InjectView(R.id.posttofb)
    LoginButton postToFacebook;
    @InjectView(R.id.posttotw)
    ImageView postToTwitter;

    @Inject
    EventManager eventManager;
    @Inject
    PostController postController;
    private FanRantMainActivity context;
    private ExpandableListView listView;
    private FanRantLiveScoresAdapter adapter;
    private ViewFlipper vFlip;
    private boolean liveGameView = false;
    private PullToRefreshListView gameListView;
    private FanRantLiveGameAdapter liveGameArrayAdapter;
    private TextView scoreTextView;
    private TableLayout scoreTableLayout;
    private LinearLayout view;
    private ViewFlipper frantViewFlipper;
    private RatingBar rating;
    private ImageView frantCommit;
    private TextView playerTextView;
    private Frants currentFrant;
    private int currentPlayerId;
    private TextView commentCounter;
    private EditText commentBody;
    private TextView ratingCommentCounter;
    private EditText ratingComment;
    private TextView cardPlayerName;
    private ImageView cardImage;
    private boolean posting = false;
    private int currentView = 0;

    private TextView noGames;

    private CustomTimeNamedHandler latestScoreTimer = new CustomTimeNamedHandler(Timers.LATESTSCORE) {
        @Override
        public void handleMessage(Message msg) {
            initFanRantLiveScoresAdapter();
        }
    };
    private String currentFixtureId;
    private TextView liveScoreLine;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        setHasOptionsMenu(true);


        context.getServiceManager().addService(FanRantService.FIXTURES, FanRantLiveFixturesService.class,
                new FixtureUpdatesReceiver(latestScoreTimer)).startService(FanRantService.FIXTURES);

        if (getArguments() != null) {
            if (getArguments().containsKey("fixtureId")) {
                currentFixtureId = getArguments().getString("fixtureId");

                context.setTabArguments("Tab4",null);
            }
        }


        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (container == null) {
            return null;
        }
        view = (LinearLayout) inflater.inflate(R.layout.livescores, container, false);

        setHasOptionsMenu(true);

        vFlip = (ViewFlipper) view.findViewById(R.id.liveGameFlipper);

        /**
         * Live scores
         */
        setupLiveScoresView();

        /**
         * Frants
         */

        setupFrantView();

        postToTwitter = (ImageView) view.findViewById(R.id.posttotw);


        postToTwitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (twitterUtils.isTwitterPrefsSet()) {
                    twitterUtils.authenticate(twitterAuthHandler);
                } else {
                    Intent intent = new Intent();
                    intent.setClassName("com.fanrant", "com.fanrant.app.view.FanRantTwitterLoginActivity");
                    startActivityForResult(intent, ApplicationMessages.TWITTERAUTH);

                    context.overridePendingTransition(R.anim.slidein, 0);
                }
            }
        });

        if (currentFixtureId != null && getArguments() != null) {
            initLiveGame(getArguments().getString("fixtureId"), false);

        }


        return view;
    }

    private void setupLiveScoresView() {
        listView = (ExpandableListView) view.findViewById(R.id.livescoreslist);
        noGames = (TextView) view.findViewById(R.id.nogames);
        initFanRantLiveScoresAdapter();

    }

    private void setupFrantView() {
        scoreTableLayout = (TableLayout) view.findViewById(R.id.windowscoreline);
        gameListView = (PullToRefreshListView) view.findViewById(R.id.livegameupdates);


        gameListView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<ListView>() {
            @Override
            public void onRefresh(PullToRefreshBase<ListView> refreshView) {
                context.getServiceManager().restartService(FanRantService.GAMEUPDATES);

            }
        });

        ImageView backButton = (ImageView) view.findViewById(R.id.backToLive);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetWall();

                context.getSupportActionBar().show();

            }
        });

        liveScoreLine = (TextView) view.findViewById(R.id.scoreline);

        scoreTextView = (TextView) view.findViewById(R.id.liveScore);
        Typeface tf = Typeface.createFromAsset(context.getAssets(),
                "fonts/LCDPHONE2.TTF");
        scoreTextView.setTypeface(tf);

        ImageView frantButton = (ImageView) view.findViewById(R.id.frantButton);
        frantButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                frant(currentFixtureId);

            }

        });
        frantViewFlipper = (ViewFlipper) view.findViewById(R.id.frantviewflip);
        frantViewFlipper.setInAnimation(context, R.anim.in_from_right);
        frantViewFlipper.setOutAnimation(context, R.anim.out_to_left);

        rating = (RatingBar) view.findViewById(R.id.frantratingBar);


        playerTextView = (TextView) view.findViewById(R.id.playerRate);
        frantCommit = (ImageView) view.findViewById(R.id.postrating);

        ratingComment = (EditText) view.findViewById(R.id.commentRating);
        frantCommit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                postFrant();
            }
        });
        ImageView commentBtn = (ImageView) view.findViewById(R.id.postcomment);
        commentBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                postFrant();
            }
        });

        commentCounter = (TextView) view.findViewById(R.id.counter);
        ratingCommentCounter = (TextView) view.findViewById(R.id.counterrating);
        commentBody = (EditText) view.findViewById(R.id.commentBody);
        commentBody.addTextChangedListener(this);

        ratingComment.addTextChangedListener(this);
        ImageView postCard = (ImageView) view.findViewById(R.id.postCard);

        cardPlayerName = (TextView) view.findViewById(R.id.cardPlayerName);

        cardImage = (ImageView) view.findViewById(R.id.cardType);

        postCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                postCard();
            }

        });
    }

    private void initFanRantLiveScoresAdapter() {
        FanRantDatabase db = new FanRantDatabase(context);

        FanRantFixtureModel[] fixtureModels = db.getFixtures();

        if (fixtureModels != null) {
            adapter = new FanRantLiveScoresAdapter(context, fixtureModels) {
                @Override
                public View.OnClickListener getFireGameView(final int groupPosition, final int childPosition) {
                    View.OnClickListener listener = new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            final FanRantFixtureModel model = (FanRantFixtureModel) adapter.getChild(groupPosition, childPosition);

                            context.showProgessBar();
                            context.getServiceManager().stopService(FanRantService.FIXTURES);


                            currentFixtureId = model.getId();

                            initLiveGame(model.getId(), true);

                            context.invalidateOptionsMenu();


                        }
                    };
                    return listener;
                }
            };

            if (noGames.getVisibility() == View.VISIBLE) {
                noGames.setVisibility(View.GONE);
            }
            listView.setAdapter(adapter);
            for (int i = 0; i < listView.getExpandableListAdapter().getGroupCount(); i++) {
                listView.expandGroup(i);
            }

        } else {
            noGames.setVisibility(View.VISIBLE);

        }
    }

    private void initLiveGame(String fixid, boolean animate) {
        FanRantDatabase db = new FanRantDatabase(context);

        final FanRantFixtureModel model = db.getFixture(fixid);
        Bundle b = new Bundle();
        Handler ahandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                initLiveGame(model);
                gameListView.onRefreshComplete();
            }
        };
        b.putString("fixture_id", model.getId());

        context.getServiceManager().addAndStartService(FanRantService.GAMEUPDATES, FanRantGameUpdatesService.class, new GameUpdatesReceiver(ahandler), b);
        if (animate) {
            vFlip.setInAnimation(context, R.anim.in_from_right);
            vFlip.setOutAnimation(context, R.anim.out_to_left);
            vFlip.showNext();
        } else {
            vFlip.setDisplayedChild(1);
        }
        score(model.getScoreline());
        liveGameView = true;
        context.getSupportActionBar().hide();
        if (scoreTableLayout.getHeight() == 0) {
            int size = 90;
            DisplayMetrics metrics = new DisplayMetrics();
            context.getWindowManager().getDefaultDisplay().getMetrics(metrics);

            switch (metrics.densityDpi) {
                case DisplayMetrics.DENSITY_LOW:
                    size = 60;
                    break;
                case DisplayMetrics.DENSITY_MEDIUM:
                    size = 90;
                    break;
                case DisplayMetrics.DENSITY_HIGH:
                    size = 90;
                    break;
                case DisplayMetrics.DENSITY_XHIGH:
                    size = 120;
                    break;
                case DisplayMetrics.DENSITY_XXHIGH:
                    size = 90;
                    break;
            }
            if (animate) {
                DropDownAnim dropDownAnim = new DropDownAnim(scoreTableLayout, size, true);
                dropDownAnim.setDuration(800);
                view.startAnimation(dropDownAnim);
            } else {
                DropDownAnim dropDownAnim = new DropDownAnim(scoreTableLayout, size, true);
                dropDownAnim.setDuration(100);
                view.startAnimation(dropDownAnim);
            }
        }
        initLiveGame(model);
    }

    private void initLiveGame(final FanRantFixtureModel model) {

        FanRantDatabase db = new FanRantDatabase(context);
        FanRantLiveGameAdapter liveGameAdapter = new FanRantLiveGameAdapter(context, R.id.livegameupdates, db.getGameUpdatesForFixture(model.getId())) {
            @Override
            public View.OnClickListener getShareAction(final int childPosition) {

                View.OnClickListener listener = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        FanRantGameUpdateModel model = (FanRantGameUpdateModel) getItem(childPosition);
                        StringBuilder sb = new StringBuilder();
                        FanRantDialog.newInstance(context, model.getComment(), false, DialogType.SHARE).show(context.getSupportFragmentManager(), "share");
                    }
                };
                return listener;
            }
        };
        gameListView.setAdapter(liveGameAdapter);

        String mtime = "";
        if (model.getMatchtime() == null) {
            mtime = "FT";
        } else if (model.getMatchtime().equals("0min")) {
            mtime = model.getKickoff().substring(11, 16);
        } else {
            mtime = model.getMatchtime();
        }
        liveScoreLine.setText(model.getHome_team_name() + " vs " + model.getAway_team_name() + "(" + mtime + ")");

    }

    private void resetWall() {
//        context.getSupportActionBar().show();
        context.setTitle("Live Scores");
        currentFixtureId = null;
        liveGameArrayAdapter = null;
        liveGameView = false;
        vFlip.setInAnimation(context, R.anim.in_from_left);
        vFlip.setOutAnimation(context, R.anim.out_to_right);

        vFlip.showPrevious();
    }

    private void postFrant() {
        context.showProgessBar();
        postController.performFrantPost(currentFrant, currentFixtureId,
                rating.getRating(), ratingComment.getText().toString(),
                currentPlayerId);
        commentBody.setText("");
        frantViewFlipper.setDisplayedChild(0);
        context.invalidateOptionsMenu();

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.context = (FanRantMainActivity) activity;
        if (vFlip != null) {
            vFlip.setDisplayedChild(currentView);
        }
//        app = (FanRantApplication) context.getApplication();
        latestScoreTimer.run();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        FanRantMenuItem.MenuItemIndex menuItemIndex = FanRantMenuItem.fromOrd(item.getItemId());
        switch (menuItemIndex) {
            case ITEM_INDEX_0:

                context.getSupportActionBar().show();
                commentBody.setText("");
                frantViewFlipper.setDisplayedChild(0);
                context.invalidateOptionsMenu();
                break;
            case ITEM_INDEX_1:
                frant(currentFixtureId);
                break;

            case ITEM_INDEX_2:
                Intent intent = new Intent();
                intent.putExtra("fixtureId", currentFixtureId);
                intent.setClassName("com.fanrant", "com.fanrant.app.view.FanRantFrantChooserActivity");
                startActivityForResult(intent, 1);
                context.overridePendingTransition(R.anim.slidein, 0);
                break;
            case ITEM_INDEX_3:
                postCard();
                break;
            case ITEM_INDEX_4:
                postFrant();
                break;

        }

        InputMethodManager imm = (InputMethodManager) context.getSystemService(
                Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(commentBody.getWindowToken(), 0);
        return super.onOptionsItemSelected(item);
    }

    private void postCard() {
        postController.performPostCard(currentFixtureId, currentPlayerId, (currentFrant == Frants.YELLOWCARD));
        frantViewFlipper.setDisplayedChild(0);
        context.invalidateOptionsMenu();
    }

    private void frant(String currentFixtureId) {
        Intent intent = new Intent();
        intent.putExtra("fixtureId", currentFixtureId);
        intent.setClassName("com.fanrant", "com.fanrant.app.view.FanRantFrantChooserActivity");
        startActivityForResult(intent, 1);
        context.overridePendingTransition(R.anim.slidein, 0);

    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        context.setTitle("Live Scores");
        if (liveGameView) {


            context.showLiveGameScoreLine();
            menu.clear();


            switch (frantViewFlipper.getDisplayedChild()) {
                case 0:
                    context.getSupportActionBar().hide();
                    break;
                case 1:
                    context.getSupportActionBar().show();
                    List<FanRantMenuItem> menuItems = new ArrayList<FanRantMenuItem>();
                    menuItems.add(new FanRantMenuItem("Cancel", FanRantMenuItem.MenuItemIndex.ITEM_INDEX_0));

                    menuItems.add(new FanRantMenuItem("Post", FanRantMenuItem.MenuItemIndex.ITEM_INDEX_4));
                    for (FanRantMenuItem menuItem : menuItems) {
                        menu.add(0, menuItem.getId().ordinal(), menuItem.getId().ordinal(), menuItem.getName()).setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
                    }
                    break;

                case 2:
                    context.getSupportActionBar().show();
                    menuItems = new ArrayList<FanRantMenuItem>();
                    menuItems.add(new FanRantMenuItem("Cancel", FanRantMenuItem.MenuItemIndex.ITEM_INDEX_0));

                    menuItems.add(new FanRantMenuItem("Post", FanRantMenuItem.MenuItemIndex.ITEM_INDEX_3));
                    for (FanRantMenuItem menuItem : menuItems) {
                        menu.add(0, menuItem.getId().ordinal(), menuItem.getId().ordinal(), menuItem.getName()).setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
                    }
                    break;
                case 3:
                    menu.clear();
                    menuItems = new ArrayList<FanRantMenuItem>();
                    menuItems.add(new FanRantMenuItem("Cancel", FanRantMenuItem.MenuItemIndex.ITEM_INDEX_0));

                    menuItems.add(new FanRantMenuItem("Post", FanRantMenuItem.MenuItemIndex.ITEM_INDEX_3));
                    for (FanRantMenuItem menuItem : menuItems) {
                        menu.add(0, menuItem.getId().ordinal(), menuItem.getId().ordinal(), menuItem.getName()).setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
                    }
                    break;
            }
        } else {
            menu.clear();
        }
        super.onPrepareOptionsMenu(menu);

    }

    @Override
    public void score(String score) {
        if (!score.equals("")) { //0-0
            scoreTextView.setText(score);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (Frants.fromOrd(resultCode)) {
            case COMMENT:
                frantViewFlipper.setDisplayedChild(3);
                frantCommit.setImageResource(R.drawable.frant_comment);
                currentFrant = Frants.COMMENT;
                context.invalidateOptionsMenu();
//                playerTextView.setText("Rate " + api.getStringExtra("playerName"));
                break;
            case YELLOWCARD:
                frantViewFlipper.setDisplayedChild(2);
                cardPlayerName.setText(data.getStringExtra("playerName"));
                currentPlayerId = data.getIntExtra("playerId", 0);
                cardImage.setImageResource(R.drawable.frant_yellow);
                currentFrant = Frants.YELLOWCARD;
//                yellowCard = true;

                context.invalidateOptionsMenu();
                break;
            case REDCARD:

                frantViewFlipper.setDisplayedChild(2);
                cardPlayerName.setText(data.getStringExtra("playerName"));
                currentPlayerId = data.getIntExtra("playerId", 0);
                cardImage.setImageResource(R.drawable.frant_red);
                currentFrant = Frants.REDCARD;

                context.invalidateOptionsMenu();
//                yellowCard = false;
                break;
            case GOAL:
                frantViewFlipper.setDisplayedChild(1);
                frantCommit.setImageResource(R.drawable.frant_goal);
                playerTextView.setText("Rate " + data.getStringExtra("playerName"));
                currentFrant = Frants.GOAL;
                currentPlayerId = data.getIntExtra("playerId", 0);

                context.invalidateOptionsMenu();
                break;
            case PENALTY:
                frantViewFlipper.setDisplayedChild(1);
                frantCommit.setImageResource(R.drawable.frant_penalty);
                playerTextView.setText("Rate " + data.getStringExtra("playerName"));
                currentFrant = Frants.PENALTY;
                currentPlayerId = data.getIntExtra("playerId", 0);

                context.invalidateOptionsMenu();
                break;
            case DIVE:
                frantViewFlipper.setDisplayedChild(1);
                frantCommit.setImageResource(R.drawable.frant_dive);
                playerTextView.setText("Rate " + data.getStringExtra("playerName"));
                currentFrant = Frants.DIVE;
                currentPlayerId = data.getIntExtra("playerId", 0);

                context.invalidateOptionsMenu();
                break;
            case ONFIRE:
                frantViewFlipper.setDisplayedChild(1);
                frantCommit.setImageResource(R.drawable.frant_fire);
                playerTextView.setText("Rate " + data.getStringExtra("playerName"));
                currentFrant = Frants.ONFIRE;
                currentPlayerId = data.getIntExtra("playerId", 0);

                context.invalidateOptionsMenu();
                break;
            case WHATADONKEY:
                frantViewFlipper.setDisplayedChild(1);
                frantCommit.setImageResource(R.drawable.frant_donkey);
                playerTextView.setText("Rate " + data.getStringExtra("playerName"));
                currentFrant = Frants.WHATADONKEY;
                currentPlayerId = data.getIntExtra("playerId", 0);

                context.invalidateOptionsMenu();
                break;
            case PLAYACTOR:
                frantViewFlipper.setDisplayedChild(1);
                frantCommit.setImageResource(R.drawable.frant_actor);
                playerTextView.setText("Rate " + data.getStringExtra("playerName"));
                currentFrant = Frants.PLAYACTOR;
                currentPlayerId = data.getIntExtra("playerId", 0);

                context.invalidateOptionsMenu();
                break;
            case CHOPPER:
                frantViewFlipper.setDisplayedChild(1);
                frantCommit.setImageResource(R.drawable.frant_chopper);
                playerTextView.setText("Rate " + data.getStringExtra("playerName"));
                currentFrant = Frants.CHOPPER;
                currentPlayerId = data.getIntExtra("playerId", 0);

                context.invalidateOptionsMenu();
                break;
            case OFFSIDE:
                frantViewFlipper.setDisplayedChild(1);
                frantCommit.setImageResource(R.drawable.frant_offside);
                playerTextView.setText("Rate " + data.getStringExtra("playerName"));
                currentFrant = Frants.OFFSIDE;
                currentPlayerId = data.getIntExtra("playerId", 0);

                context.invalidateOptionsMenu();
                break;
            case THEREFSBLIND:
                frantViewFlipper.setDisplayedChild(1);
                frantCommit.setImageResource(R.drawable.frant_blind);
                playerTextView.setText("Rate " + data.getStringExtra("officialName"));
                currentPlayerId = data.getIntExtra("officialId", 0);
                currentFrant = Frants.THEREFSBLIND;
                context.invalidateOptionsMenu();
                break;
            case CANCELLED:
                //do nothing
                break;
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onDetach() {

        super.onDetach();
    }

    @Override
    public void onResume() {
        context.showProgessBar();
        if (getArguments() != null) {
            if (getArguments().containsKey("fixtureId")) {
                currentFixtureId = getArguments().getString("fixtureId");

                context.setTabArguments("Tab4",null);
            }
        }

        if (currentFixtureId == null) {
            vFlip.setDisplayedChild(0);
        }
        super.onResume();
    }

    private void displayToast(int postsuccess) {
        Toast.makeText(context, postsuccess, 2000).show();

    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (frantViewFlipper.getDisplayedChild() == 1) {
            int countTotal = ratingComment.getText().length();
            String txt = (150 - countTotal) + " Characters Left";
            ratingCommentCounter.setText(txt);
        } else {
            int countTotal = commentBody.getText().length();
            String txt = (150 - countTotal) + " Characters Left";
            commentCounter.setText(txt);
        }
    }

    @Override
    public void afterTextChanged(Editable s) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    class FixtureUpdatesReceiver extends ResultReceiver {

        private Handler handler;

        FixtureUpdatesReceiver(Handler handler) {
            super(handler);
            this.handler = handler;
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            handler.sendEmptyMessage(10);
        }
    }

    class GameUpdatesReceiver extends ResultReceiver {

        private Handler handler;

        GameUpdatesReceiver(Handler handler) {
            super(handler);
            this.handler = handler;
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            handler.sendEmptyMessage(10);
        }


    }


    Handler twitterAuthHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (msg.what == ApplicationMessages.TWITTERAUTH) {
                eventManager.fire(new TwitterEnableEvent());
                initSocialMedia();
            }
        }
    };

    private void initSocialMedia() {
        if (context.isPostToFacebookChecked()) {
            postToFacebook.setBackgroundResource(R.drawable.fb_on);
            postController.setPostToFacebook(true);
        }
        postToFacebook.setPublishPermissions(Arrays.asList("publish_stream"));
        if (postController.isPostToTwitter()) {
            postToTwitter.setBackgroundResource(R.drawable.twitter_on);
            postController.setPostToTwitter(true);
        }
    }

    public void facebookEnabled(@Observes FanRantFacebookEnableEvent facebookEnableEvent) {
        initSocialMedia();
    }

    public void twitterEnabled(@Observes TwitterEnableEvent twitterEnableEvent) {
        postController.setPostToTwitter(true);
        initSocialMedia();
    }

    public void facebookShared(@Observes FanRantFacebookPostedEvent fanRantFacebookPostedEvent) {
        displayToast(R.string.facebookshared);
    }

}
