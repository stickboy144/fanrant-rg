package com.fanrant.app.view;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.Button;
import com.fanrant.R;
import com.fanrant.app.events.TeamSelectedEvent;
import com.fanrant.app.modules.IPreferences;
import com.fanrant.app.modules.PreferenceConstants;
import com.fanrant.app.view.dialogs.FanRantDialog;
import com.fanrant.app.view.dialogs.FanRantTeamsListDialog;
import com.google.inject.Inject;
import roboguice.event.EventManager;
import roboguice.event.Observes;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * User: Antwork
 * Date: 17/11/2012
0 * Time: 16:31
 * To change this template use File | Settings | File Templates.
 */
@ContentView(R.layout.startup)
public class FanRantWelcomeActivity extends FanRantActivity implements FanRantDialog.FanRantDialogListener {

    @Inject
    EventManager eventManager;

    @Inject IPreferences preferences;

    @InjectView(R.id.pickteam)
    Button selectTeamButton;

    @InjectView(R.id.login)
    Button loginButton;

     FanRantTeamsListDialog tlf;


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(R.string.whosteam);

        tlf = new FanRantTeamsListDialog(eventManager);
        selectTeamButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                tlf.show(getSupportFragmentManager(), "teams list");
            }

        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClassName("com.fanrant", "com.fanrant.app.view.FanRantLoginActivity");

                startActivity(intent);
                overridePendingTransition(R.anim.slidein, 0);
                finish();
            }
        });

//        if(preferences.getEnvironment() == Environments.DEV) {
//            initGuestModeWithTeamId(new TeamSelectedEvent("7", comp)); //aston villa
//        }
    }


    public void initGuestModeWithTeamId(@Observes TeamSelectedEvent teamSelectedEvent) {
        HashMap<String,String> map = new HashMap<String, String>();
        map.put(PreferenceConstants.UNREGTEAMID, teamSelectedEvent.getTeamid());
        map.put(PreferenceConstants.UNREGCOMPID, teamSelectedEvent.getCompid());
        map.put(PreferenceConstants.UNREGUSER, "true");
        map.put(PreferenceConstants.UID, Settings.Secure.getString(getContentResolver(),
                Settings.Secure.ANDROID_ID));
        preferences.addPref(map);


        Intent intent = new Intent();
        intent.setClassName("com.fanrant", "com.fanrant.app.view.FanRantMainActivity");
        intent.putExtra("guest", true);
        startActivity(intent);
        overridePendingTransition(R.anim.slidein, 0);
        finish();


    }
}

