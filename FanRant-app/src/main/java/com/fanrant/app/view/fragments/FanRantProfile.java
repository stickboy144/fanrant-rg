package com.fanrant.app.view.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.fanrant.FanRantApp;
import com.fanrant.R;
import com.fanrant.app.Environments;
import com.fanrant.app.controller.UserController;
import com.fanrant.app.events.*;
import com.fanrant.app.model.FanRantUserModel;
import com.fanrant.app.model.IFanRantUserModel;
import com.fanrant.app.modules.IPreferences;
import com.fanrant.app.modules.api.FanRantAPIParams;
import com.fanrant.app.modules.api.FanRantAsyncTask;
import com.fanrant.app.modules.api.MockAsyncTask;
import com.fanrant.app.modules.api.responders.FanRantUserResponse;
import com.fanrant.app.modules.db.FanRantDatabase;
import com.fanrant.app.modules.twitter.ITwitterUtils;
import com.fanrant.app.modules.util.*;
import com.fanrant.app.view.FanRantMainActivity;
import com.fanrant.app.view.dialogs.FanRantDialog;
import com.fanrant.app.view.dialogs.FanRantTeamsListDialog;
import com.fanrant.app.view.dialogs.FanRantTwitterOauthDialog;
import com.fanrant.app.view.menu.FanRantMenuItem;
import com.fanrant.app.view.menu.MenuFragment;
import com.github.rtyley.android.sherlock.roboguice.fragment.RoboSherlockFragment;
import com.google.inject.Inject;
import com.novoda.imageloader.core.ImageManager;
import com.novoda.imageloader.core.model.ImageTagFactory;
import oauth.signpost.OAuthConsumer;
import oauth.signpost.OAuthProvider;
import roboguice.RoboGuice;
import roboguice.event.EventManager;
import roboguice.event.Observes;
import roboguice.inject.InjectView;
import roboguice.util.RoboAsyncTask;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.fanrant.app.view.menu.FanRantMenuItem.MenuItemIndex;

/**
 * Created with IntelliJ IDEA.
 * User: Antwork
 * Date: 30/10/2012
 * Time: 19:00
 * To change this template use File | Settings | File Templates.
 */
public class FanRantProfile extends RoboSherlockFragment {

    @Inject
    IPreferences preferences;
    @Inject
    IFanRantUserModel userModel;
    @Inject
    EventManager eventManager;

    @Inject
    ITwitterUtils twitterUtils;

    @Inject
    UserController userController;

    private FanRantMainActivity context;

    private CustomNamedHandler imageSelectedHandler = new CustomNamedHandler(Handlers.AVATARCHOSEN) {
        @Override
        public void handleMessage(Message msg) {

            if (msg.arg1 == 1) {
                Bitmap newBitmap = BitmapFactory.decodeFile(userModel.getAvatar().toString());
                Bitmap oldBitmap = ((BitmapDrawable) avatar.getDrawable()).getBitmap();
                avatar.setImageDrawable(null); //this should help
                oldBitmap.recycle();
                oldBitmap = null;
                avatar.setImageBitmap(newBitmap);
                avatar.invalidate();
            }
            super.handleMessage(msg);
        }
    };

    private ImageView avatar;
    private ArrayAdapter<String> adapter;
    private EditText email, pass, pass2;
    private boolean avatarChanged = false;
    private CacheStore cache = CacheStore.getInstance();
    private FanRantTeamsListDialog tlf;
    @InjectView(R.id.pickteam)
    Button selectTeamButton;
    private TeamSelectedEvent teamSelected;
    private CheckBox facebook;
    private CheckBox twitter;
    private Button upload;
    private ImageManager imageManager;
    private ImageTagFactory imageTagFactory;


    @Override
    public void onCreate(Bundle savedInstanceState) {
/**
 * setup roboguice for this fragment
 */
        RoboGuice.getInjector(context).injectMembers(this);

        setHasOptionsMenu(true);
        context.setWindowTitle("Profile");
        context.showProgessBar();

        imageManager = FanRantApp.getImageManager();

        imageTagFactory = ImageTagFactory.newInstance(context, R.drawable.profile);

        imageTagFactory.setErrorImageId(R.drawable.profile);
        imageTagFactory.setSaveThumbnail(true);


        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (container == null) {
            return null;
        }

        View view = inflater.inflate(R.layout.profile, null);
        avatar = (ImageView) view.findViewById(R.id.avatar);

        setImageTag(avatar, userModel.getAvatar());
        loadImage(avatar);

//        Bitmap img = cache.getCacheFile(userModel.getAvatar());
//        if (img == null) {
////            AvatarLoaderTask imageLoader = new AvatarLoaderTask(avatar);
////            imageLoader.execute(userModel.avatar.toString());
//        } else {
//            avatar.setImageBitmap(img);
//        }


        selectTeamButton = (Button) view.findViewById(R.id.pickteam);
        upload = (Button) view.findViewById(R.id.uploadavatar);
        upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchChooser(v);
            }
        });

        email = (EditText) view.findViewById(R.id.regEmail);
        email.setText(userModel.getEmail_address());
        pass = (EditText) view.findViewById(R.id.regPass);
        pass.setText(userModel.getPassword());
        pass2 = (EditText) view.findViewById(R.id.regConfPass);
        pass2.setText(userModel.getPassword());

        FragmentManager fm = context.getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        Fragment mFragment = fm.findFragmentByTag("f1");
        if (mFragment == null) {

            List<FanRantMenuItem> menuItems = new ArrayList<FanRantMenuItem>();
            FanRantMenuItem refreshMenuItem = new FanRantMenuItem("Logout", MenuItemIndex.ITEM_INDEX_0);
            FanRantMenuItem refreshMenuItem2;
            if (!userModel.isRegistered()) {
                refreshMenuItem2 = new FanRantMenuItem("Register", MenuItemIndex.ITEM_INDEX_5);
            } else {
                refreshMenuItem2 = new FanRantMenuItem("Save", MenuItemIndex.ITEM_INDEX_1);

            }
            menuItems.add(refreshMenuItem);
            menuItems.add(refreshMenuItem2);
            mFragment = new MenuFragment(menuItems);
            ft.add(mFragment, "f1");
        }
        ft.commit();

        twitter = (CheckBox) view.findViewById(R.id.sharetw);

        twitter.setChecked(twitterUtils.isTwitterPrefsSet());

        twitter.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    twitterUtils.loginAndAuthenicate(loginHandler, new OAuthRequestTokenTask(context,twitterUtils.getConsumer(),twitterUtils.getProvider(), loginHandler));
                } else {
                    if (Boolean.valueOf(context.getPreference("twitterEnabled"))) {
                        context.deletePref("twitterEnabled");
                    }
                }
            }
        });

        facebook = (CheckBox) view.findViewById(R.id.sharefb);

        facebook.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {

                    String fbid = context.getPreference("facebook_id");
                    String fbtoken = context.getPreference("facebook_access_token");

                    if (fbid != null && fbtoken != null) {
                        buttonView.setChecked(true);
                    }

                }
            }
        });


        return view;
    }


    private void setImageTag(ImageView view, String url) {
        view.setTag(imageTagFactory.build(url, context));
    }

    private void loadImage(ImageView view) {
        imageManager.getLoader().load(view);
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        tlf = new FanRantTeamsListDialog(eventManager);
        selectTeamButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                tlf.show(context.getSupportFragmentManager(), "teams list");
            }

        });

        teamSelected = new TeamSelectedEvent(String.valueOf(userModel.getTeam_id()), userModel.getCompetition_id(), userModel.getTeamName());

        eventManager.fire(teamSelected);

        if (!userModel.isRegistered()) {
            email.setEnabled(false);
            pass.setEnabled(false);
            pass2.setEnabled(false);
            twitter.setEnabled(false);
            facebook.setEnabled(false);
            upload.setEnabled(false);
            TextView regneed = (TextView) view.findViewById(R.id.regneed);
            regneed.setVisibility(View.VISIBLE);
            selectTeamButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    FanRantDialog.newInstance(context, context.getString(R.string.regnow), true, DialogType.REGNOW).show(context.getSupportFragmentManager(), "Register now");

                }
            });
        }
        super.onViewCreated(view, savedInstanceState);    //To change body of overridden methods use File | Settings | File Templates.
    }

    @Override
    public void onAttach(Activity activity) {
        context = (FanRantMainActivity) activity;

        super.onAttach(activity);
    }

    private static final int ACTIVITY_SELECT_IMAGE = 1;


    public void launchChooser(View v) {
        Intent i = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, ACTIVITY_SELECT_IMAGE);

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == ACTIVITY_SELECT_IMAGE && resultCode == Activity.RESULT_OK) {
            Uri selectedImage = data.getData();
            Bitmap bitmap = null;
            try {
                bitmap = ImageUtils.getCorrectlyOrientedImage(context, selectedImage);
            } catch (IOException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = context.getContentResolver().query(selectedImage, filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String filePath = cursor.getString(columnIndex);
            cursor.close();

            try {
                FileOutputStream out = new FileOutputStream(filePath);
                bitmap.compress(Bitmap.CompressFormat.PNG, 90, out);
            } catch (Exception e) {
                e.printStackTrace();
            }


            userModel.setAvatar(filePath);

            avatar.setImageDrawable(null); //this should help
            avatar.setImageBitmap(bitmap);
            avatar.invalidate();
            avatarChanged = true;
        } else if (resultCode == ApplicationMessages.TWITTERAUTH) {
            twitter.setEnabled(twitterUtils.isTwitterPrefsSet());
        } else if (resultCode == ApplicationMessages.TWITTERTIMEOUT) {
            context.showFanRantDialog(R.string.twittertimedout, false);
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    private void displayToast(int postsuccess) {
        Toast.makeText(context, postsuccess, 2000).show();

    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        context.setTitle("Profile");
        context.hideProgessBar();
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        FanRantMenuItem.MenuItemIndex menuItemIndex = FanRantMenuItem.fromOrd(item.getItemId());

        switch (menuItemIndex) {


            case ITEM_INDEX_0:
                preferences.clear();
                Intent intent = new Intent();
                intent.setClassName("com.fanrant", "com.fanrant.app.view.FanRantWelcomeActivity");
                startActivity(intent);
                context.overridePendingTransition(R.anim.slidein, 0);
                context.finish();

                break;
            case ITEM_INDEX_1:
                HashMap<String, Object> postData = new HashMap<String, Object>();
                postData.put("method", "updateProfile");
                postData.put("email", email.getText().toString());
                postData.put("username", userModel.getUsername());
                postData.put("password", userModel.getPassword());
                postData.put("password1", pass.getText().toString());
                postData.put("password2", pass2.getText().toString());
                if (avatarChanged) {
                    postData.put("avatar", userModel.getAvatar());
                }

                postData.put("team_id", String.valueOf(teamSelected.getTeamid()));

                context.showProgessBar();


                Handler handler = new CustomNamedHandler(Handlers.USERUPDATEHANDLER) {
                    @Override
                    public void handleMessage(Message msg) {
                        if (msg.what == ApplicationMessages.ASYNCTASKSUCESS.ordinal()) {
                            displayToast(R.string.profileupdated);
                            FanRantUserModel[] userModels =
                                    (FanRantUserModel[]) msg.obj;
                            userModel.populate(userModels[0]);
                            userModel.setPassword(pass.getText().toString());
                            userController.saveUserModelAsPref(userModel);
                            context.hideProgessBar();
                            FanRantDatabase db = new FanRantDatabase(context);
                            db.truncateWall();
                        } else if (msg.what == ApplicationMessages.ASYNCTASKFAILED.ordinal()) {
                            eventManager.fire(new FanRantErrorEvent((Exception) msg.obj));
                        }
                        context.hideProgessBar();
                        super.handleMessage(msg);
                    }
                };
                if (!pass2.getText().toString().equals(pass.getText().toString())) {
                    context.showFanRantDialog(R.string.passwordmismatch, false);
                } else {
                    FanRantAPIParams params = new FanRantAPIParams(FanRantAPIParams.ApiService.USER, FanRantUserResponse.class, postData, preferences); //TODO: inject prefs
                    RoboAsyncTask t = preferences.getEnvironment() == Environments.TESTING ?
                            new MockAsyncTask(context, params, handler) : new FanRantAsyncTask(context, params, handler);
                    t.execute();
                }
                break;
            case ITEM_INDEX_2:
                break;
            case ITEM_INDEX_3:
                break;
            case ITEM_INDEX_4:
                break;
            case ITEM_INDEX_5:
                Intent intent2 = new Intent();
                intent2.setClassName("com.fanrant", "com.fanrant.app.view.FanRantRegisterActivity");
                context.startActivity(intent2);
                context.overridePendingTransition(R.anim.slidein, 0);

                break;
        }

        return super.onOptionsItemSelected(item);
    }

    public void teamSelected(@Observes TeamSelectedEvent teamSelectedEvent) {
        selectTeamButton.setText("My Team: " + teamSelectedEvent.getTeamname());
        teamSelected = teamSelectedEvent;

    }

    public class OAuthRequestTokenTask extends AsyncTask<Void, Void, Void> {

        final String TAG = getClass().getName();
        private OAuthProvider provider;
        private Handler loginHandler;
        private OAuthConsumer consumer;

        /**
         *
         * We pass the OAuth consumer and provider.
         *
         * @param    context
         * 			Required to be able to start the intent to launch the browser.
         * @param    consumer
         * @param    provider
 * 			The OAuthProvider object
         * @param loginHandler
         */
        public OAuthRequestTokenTask(Context context, OAuthConsumer consumer, OAuthProvider provider, Handler loginHandler) {
            this.consumer = consumer;
            this.provider = provider;
            this.loginHandler = loginHandler;
        }


        /**
         *
         * Retrieve the OAuth Request Token and present a browser to the user to authorize the token.
         *
         */
        @Override
        protected Void doInBackground(Void... params) {

            try {
                Log.i(TAG, "Retrieving request token from Google servers");
            } catch (Exception e) {
                Log.e(TAG, "Error during OAUth retrieve request token", e);
            }

            return null;
        }

    }

    public void showOAuthDialog(@Observes FanRantTwitterAuthEvent fanRantTwitterAuthEvent) {
        FragmentTransaction ft = context.getSupportFragmentManager().beginTransaction();
        ft.addToBackStack(null);
        // Create and show the dialog.
        FanRantTwitterOauthDialog newFragment = new FanRantTwitterOauthDialog(loginHandler);
        newFragment.show(ft, "dialog");

    }
    Handler loginHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case ApplicationMessages.TWITTERAUTH:
                    twitter.setEnabled(true);
                    break;
                case ApplicationMessages.TWITTERAUTHFAILED:
                    eventManager.fire(new FanRantTwitterFailedEvent((Exception)msg.obj));
                    twitter.setEnabled(false);
                    break;
                case ApplicationMessages.TWITTERTIMEOUT:
                    eventManager.fire(new FanRantTwitterTimeOutEvent());
                    twitter.setEnabled(false);
            }
        }
    };

}