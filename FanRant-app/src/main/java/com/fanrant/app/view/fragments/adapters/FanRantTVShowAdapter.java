package com.fanrant.app.view.fragments.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;
import com.fanrant.R;
import com.fanrant.app.model.FanRantTVShowModel;
import com.fanrant.app.model.objects.ShowDay;

import java.text.SimpleDateFormat;

/**
 * Created with IntelliJ IDEA.
 * User: Antwork
 * Date: 25/11/2012
 * Time: 14:26
 * To change this template use File | Settings | File Templates.
 */
public class FanRantTVShowAdapter extends BaseExpandableListAdapter {
    private final ShowDay[] showDays;
    String[][] arrChildelements;
    String[] arrGroup;
    private LayoutInflater inflater;
    private final Context context;

    public FanRantTVShowAdapter(Context context, ShowDay[] showDays) {
        this.context = context;
        this.showDays = showDays;

    }

    public Object getChild(int groupPosition, int childPosition) {

        return showDays[groupPosition].getShows().get(childPosition);
    }

    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }

    public int getChildrenCount(int groupPosition) {

        return showDays[groupPosition].getShows().size();
    }

    public Object getGroup(int groupPosition) {
        return showDays[groupPosition].getShows();
    }

    public int getGroupCount() {
        return showDays.length;
    }

    public long getGroupId(int groupPosition) {
        return 0;
    }

    public boolean hasStableIds() {
        return false;
    }

    public boolean isChildSelectable(int groupPosition, int childPosition) {

        return true;
    }

    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.day_row, null);
        }

        ViewContainer viewContainer = new ViewContainer(convertView);

        viewContainer.getShowDay().setText(showDays[groupPosition].getStart().toString());

        viewContainer.getShowDay().setTypeface(null, Typeface.BOLD);
        return convertView;
    }

    public View getChildView(int groupPosition, int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.show_row, null);
        }

        ShowContainer viewContainer = new ShowContainer(convertView);

        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm");

        FanRantTVShowModel show = (FanRantTVShowModel) showDays[groupPosition].getShows().get(childPosition);
            viewContainer.getName().setText(show.getShow());
        viewContainer.getInfo().setText("On " + show.getChannel() + " @ " + show.getStart().split(" ")[1]);
        convertView.setOnClickListener(getFirePlayerView(groupPosition, childPosition));
        return convertView;
    }

    public View.OnClickListener getFirePlayerView(int groupPosition, int childPosition) {
        return null;  //To change body of created methods use File | Settings | File Templates.
    }

    private class ViewContainer {
        private View row;
        private TextView showDay;

        private ViewContainer(View row) {
            this.row = row;
        }

        public TextView getShowDay() {
            if (showDay == null) {
                showDay = (TextView) row.findViewById(R.id.showday);
            }
            return showDay;
        }
    }


    private class ShowContainer {
        private View row;

        private TextView name;
        private TextView info;


        private ShowContainer(View row) {
            this.row = row;
        }

        public TextView getName() {
            if (name == null) {
                name = (TextView) row.findViewById(R.id.showname);
            }
            return name;
        }

        public TextView getInfo() {
            if(info == null) {
                info = (TextView) row.findViewById(R.id.showinfo);
            }
            return info;
        }
    }
}
