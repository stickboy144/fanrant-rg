package com.fanrant.app.view;

import android.content.Intent;
import android.os.Bundle;
import com.fanrant.R;
import com.fanrant.app.Environments;
import com.fanrant.app.controller.UserController;
import com.fanrant.app.events.*;
import com.fanrant.app.model.FanRantUserModel;
import com.fanrant.app.model.IFanRantUserModel;
import com.fanrant.app.modules.PreferenceConstants;
import com.fanrant.app.modules.db.DatabaseManager;
import com.fanrant.app.modules.db.FanRantDBOpenHelper;
import com.google.inject.Inject;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.JsonSerializer;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializerProvider;
import roboguice.activity.event.OnStartEvent;
import roboguice.event.EventManager;
import roboguice.event.Observes;
import roboguice.inject.ContentView;

import java.io.IOException;
import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.`
 * User: Antwork
 * Date: 17/03/2013
 * Time: 16:49
 * To change this template use File | Settings | File Templates.
 */
@ContentView(R.layout.preload)
public class FanRantPreLoaderActivity extends FanRantActivity {

    /**
     * see UserController.preLoad()) @Observes OnStartEvent4
     */
    @Inject
    EventManager eventManager;

    @Inject
    UserController userController;

    @Inject
    IFanRantUserModel userModel;

    @Inject
    protected FanRantEventListeners fanRantEventListeners;
    private FanRantDBOpenHelper databaseHelper;

    /**
     * receives events from the UserController
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    protected void onStart() {
        if (preferences.getEnvironment() != Environments.TESTING) {
            super.onStart();
        }
        userController.init();
    }

    /**
     * called by UserController.startWelcomeMode();
     *
     * @param welcomeEvent
     */
    protected void initWelcomeScreen(@Observes WelcomeEvent welcomeEvent) {
        Intent intent = new Intent();
        intent.setClassName("com.fanrant", FanRantWelcomeActivity.class.getName());
        startActivity(intent);
        overridePendingTransition(R.anim.slidein, 0);
        finish();

    }

    /**
     * called by UserController.startLoginMode();
     *
     * @param loginEvent
     */
    protected void initUserMode(@Observes LoggedInEvent loginEvent) {
        userModel.populate((FanRantUserModel) loginEvent.getModel());
        userModel.setLoggedIn(true);
        userModel.setRegistered(true);
        userController.saveUserModelAsPref(userModel);
        Intent intent = new Intent();

        intent.setClassName("com.fanrant", FanRantMainActivity.class.getName());
        startActivity(intent);
        overridePendingTransition(R.anim.slidein, 0);
        finish();

    }

    /**
     * called by UserController.startGuestMode();
     *
     * @param guestModeEvent
     */

    protected void initGuestMode(@Observes GuestModeEvent guestModeEvent) {
        userModel.populate((FanRantUserModel) guestModeEvent.getModel());
        userController.saveUserModelAsPref(userModel);
        Intent intent = new Intent();
        intent.setClassName("com.fanrant", "com.fanrant.app.view.FanRantMainActivity");
        startActivity(intent);
        finish();

    }

    void fireOnStart() {
        eventManager.fire(new OnStartEvent());
    }




}