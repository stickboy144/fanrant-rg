package com.fanrant.app.view;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TabWidget;
import android.widget.TextView;
import com.facebook.*;
import com.facebook.model.GraphUser;
import com.fanrant.R;
import com.fanrant.app.Environments;
import com.fanrant.app.controller.UserController;
import com.fanrant.app.events.FanRantFacebookEnableEvent;
import com.fanrant.app.events.GuestModeEvent;
import com.fanrant.app.events.TwitterEnableEvent;
import com.fanrant.app.model.FanRantUserModel;
import com.fanrant.app.model.FanRantWallStatusModel;
import com.fanrant.app.model.IFanRantUserModel;
import com.fanrant.app.modules.IPreferences;
import com.fanrant.app.modules.api.FanRantAPIParams;
import com.fanrant.app.modules.api.FanRantAsyncTask;
import com.fanrant.app.modules.api.MockAsyncTask;
import com.fanrant.app.modules.api.responders.FanRantWallStatusResponse;
import com.fanrant.app.modules.services.FanRantServiceManager;
import com.fanrant.app.modules.services.IFanRantServiceManager;
import com.fanrant.app.modules.twitter.TwitterConstants;
import com.fanrant.app.modules.util.ApplicationMessages;
import com.fanrant.app.modules.util.CustomTimeNamedHandler;
import com.fanrant.app.modules.util.Timers;
import com.fanrant.app.view.dialogs.FanRantDialog;
import com.fanrant.app.view.dialogs.FanRantTwitterOauthDialog;
import com.fanrant.app.view.fragments.*;
import com.google.inject.Inject;
import oauth.signpost.OAuthConsumer;
import oauth.signpost.OAuthProvider;
import roboguice.event.EventManager;
import roboguice.event.Observes;
import roboguice.inject.ContentView;
import roboguice.util.RoboAsyncTask;

import java.util.HashMap;
import java.util.Map;

//import com.facebook.model.GraphUser;

@ContentView(R.layout.main)
public class FanRantMainActivity extends FanRantActivity implements TabHost.OnTabChangeListener, FanRantDialog.FanRantDialogListener {
    public static final String TAG = "Fan Rant Log::main";
    private static final int ACTIVITY_SELECT_IMAGE = 1;
    public TabInfo mLastTab = null;
    TabHost mTabHost;
    @Inject
    IPreferences preferences;
    @Inject
    UserController userController;
    @Inject
    IFanRantUserModel userModel;
    @Inject
    EventManager eventManager;
    IFanRantServiceManager serviceManager;

    private HashMap mapTabInfo = new HashMap();
    private boolean postToFacebookChecked;
    private TabWidget tabWidget;
    private UiLifecycleHelper uiHelper; //facebook
    private Session.StatusCallback callback = new Session.StatusCallback() {
        @Override
        public void call(Session session, SessionState state, Exception exception) {
            onSessionStateChange(session, state, exception);
        }
    };
    private boolean postToTwitterChecked;
    private FanRantWallStatusModel wallState;


    private CustomTimeNamedHandler wallStateTimer = new CustomTimeNamedHandler(Timers.HOMEWALLSTATE) {
        @Override
        public Runnable getTask() {
            Runnable task = new Runnable() {
                @Override
                public void run() {
                    checkWallState();
                }
            };
            return task;
        }
    };

    /**
     * @param activity
     * @param tabHost
     * @param tabSpec
     */
    private static void addTab(FanRantMainActivity activity, TabHost tabHost, TabHost.TabSpec tabSpec, TabInfo tabInfo) {
        // Attach a Tab view factory to the spec
        View v = LayoutInflater.from(tabHost.getContext()).inflate(R.layout.fanranttab, tabHost.getTabWidget(), false);
        TextView title = (TextView) v.findViewById(R.id.title);
        title.setText(tabInfo.name);
        ImageView icon = (ImageView) v.findViewById(R.id.icon);
        icon.setImageResource(tabInfo.image);

        tabSpec.setContent(activity.new TabFactory(activity));
        tabSpec.setIndicator(v);
        String tag = tabSpec.getTag();

        // Check to see if we already have a fragment for this tab, probably
        // from a previously saved state.  If so, deactivate it, because our
        // initial state is that a tab isn't shown.
        tabInfo.fragment = activity.getSupportFragmentManager().findFragmentByTag(tag);
        if (tabInfo.fragment != null && !tabInfo.fragment.isDetached()) {
            FragmentTransaction ft = activity.getSupportFragmentManager().beginTransaction();
            ft.detach(tabInfo.fragment);
            ft.commit();
            activity.getSupportFragmentManager().executePendingTransactions();
        }
        tabHost.addTab(tabSpec);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState); //do this after setting contentview

        wallState = new FanRantWallStatusModel();
        wallState.setStatus(4);
//        wallState.setStatus(1);
//        wallState.setFixture_id(653366);
        boolean newGuest = getIntent().getBooleanExtra("guest", false);

        serviceManager = new FanRantServiceManager(this);

        super.setServiceManager(serviceManager);
        if (newGuest) {
            /**
             ~             * init app for a new guest call the userController to get the team info to
             * setup the default guest userModel
             */
            FanRantDialog progressDialog = showProgessBar();
            userController.startGuestMode();
        } else {
            init(null);

        }

        uiHelper = new UiLifecycleHelper(this, callback);
        uiHelper.onCreate(savedInstanceState);


    }

    public void loadAsGuest(@Observes GuestModeEvent guestModeEvent) {
        userModel.populate((FanRantUserModel) guestModeEvent.getModel());
        userController.saveUserModelAsPref(userModel);
        init(null);
    }

    public void init(Bundle savedInstanceState) {
        initialiseTabHost(savedInstanceState);

        if (savedInstanceState != null) {
            mTabHost.setCurrentTabByTag(savedInstanceState.getString("tab")); //set the tab as per the saved state
        }
        tabWidget = (TabWidget) findViewById(android.R.id.tabs);
        checkWallState();
    }

    private void checkWallState() {
        Handler loginHandler = new Handler() {
            @Override
            public void handleMessage(Message message) {
                if (message.what == ApplicationMessages.ASYNCTASKSUCESS.ordinal()) {
                    FanRantWallStatusModel[] models = (FanRantWallStatusModel[]) message.obj;
                    setWallState(models[0]);
                }
            }
        };
        Map<String, String> postData = new HashMap<String, String>();
        postData.put("method", "getStatus");
        postData.put("team_id", String.valueOf(userModel.getTeam_id()));
        postData.put("competition_id", userModel.getCompetition_id());

        FanRantAPIParams params = new FanRantAPIParams(FanRantAPIParams.ApiService.FIXTURE, FanRantWallStatusResponse.class, postData, preferences);

        RoboAsyncTask t = preferences.getEnvironment() == Environments.TESTING ?
                new MockAsyncTask(this, params, loginHandler) : new FanRantAsyncTask(this, params, loginHandler);

        t.execute();
    }

    private void setWallState(FanRantWallStatusModel model) {
        wallState = model;
    }

    public FanRantWallStatusModel.WallState getWallState() {
        return wallState.getState();
    }

    public int getLiveFixture() {
        return wallState.getFixture_id();
    }

    public void launchChooser(View v) {
        Intent i = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, ACTIVITY_SELECT_IMAGE);

    }

    @Override
    public void onDialogPositiveClick(String error) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialog) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public boolean isPostToTwitterChecked() {
        return postToTwitterChecked;
    }

    public void setPostToTwitterChecked(boolean postToTwitterChecked) {
        this.postToTwitterChecked = postToTwitterChecked;
    }

    @Override
    public void onStop() {
        super.onStop();

    }

    private void initialiseTabHost(Bundle args) {
        mTabHost = (TabHost) findViewById(android.R.id.tabhost);
        mTabHost.setup();
        TabInfo tabInfo = null;
        FanRantMainActivity.addTab(this, this.mTabHost, this.mTabHost.newTabSpec("Tab1").setIndicator("Tab 1"), (tabInfo = new TabInfo("Tab1", "Home", FanRantHome.class, R.drawable.home, args, 0)));
        mapTabInfo.put(tabInfo.tag, tabInfo);
        FanRantMainActivity.addTab(this, this.mTabHost, this.mTabHost.newTabSpec("Tab2").setIndicator("Tab 2"), (tabInfo = new TabInfo("Tab2", "Fixtures", FanRantFixtures.class, R.drawable.fixtures, args, 1)));
        mapTabInfo.put(tabInfo.tag, tabInfo);
        FanRantMainActivity.addTab(this, this.mTabHost, this.mTabHost.newTabSpec("Tab3").setIndicator("Tab 3"), (tabInfo = new TabInfo("Tab3", "TV Schedule", FanRantTVSchedule.class, R.drawable.tv, args, 2)));
        mapTabInfo.put(tabInfo.tag, tabInfo);
        FanRantMainActivity.addTab(this, this.mTabHost, this.mTabHost.newTabSpec("Tab4").setIndicator("Tab 4"), (tabInfo = new TabInfo("Tab4", "Live Scores", FanRantLiveScores.class, R.drawable.live, args, 3)));
        this.mapTabInfo.put(tabInfo.tag, tabInfo);
        FanRantMainActivity.addTab(this, this.mTabHost, this.mTabHost.newTabSpec("Tab5").setIndicator("Tab 5"), (tabInfo = new TabInfo("Tab5", "Profile", FanRantProfile.class, R.drawable.profile, args, 4)));
        this.mapTabInfo.put(tabInfo.tag, tabInfo);
        this.onTabChanged("Tab1");
        mTabHost.setOnTabChangedListener(this);
    }

    public void onTabChanged(String tag) {

//        mLastTab.fragment.setArguments(null);

        TabInfo newTab = (TabInfo) mapTabInfo.get((Object) tag);

        if (mLastTab != newTab) {
            FragmentTransaction ft = this.getSupportFragmentManager().beginTransaction();
            if (mLastTab != null) {
                if (mLastTab.fragment != null) {
                    mLastTab.fragment.onSaveInstanceState(new Bundle());
                    ft.detach(mLastTab.fragment);
                }
            }
            if (newTab != null) {
                if (newTab.fragment == null) {
                    newTab.fragment = Fragment.instantiate(this,
                            newTab.clss.getName(), newTab.args);
                    newTab.fragment.setRetainInstance(true);

                    ft.add(R.id.realtabcontent, newTab.fragment, newTab.tag);
                } else {
                    ft.attach(newTab.fragment);
                }
            }

            if (mLastTab != null) {
                mTabHost.getTabWidget().getChildAt(mLastTab.index).setBackgroundResource(0);
            }
            mLastTab = newTab;
            mLastTab.fragment.setRetainInstance(true);
            ft.commit();
            this.getSupportFragmentManager().executePendingTransactions();
            invalidateOptionsMenu();

            int tab = mTabHost.getCurrentTab();
            mTabHost.getTabWidget().getChildAt(tab).setBackgroundColor(R.color.trans);
            mTabHost.getTabWidget().getChildAt(tab).setBackgroundResource(R.drawable.tabbarcontainer);

        }
    }

    public void setTabArguments(String tag, String fixture) {
        TabInfo newTab = (TabInfo) mapTabInfo.get((Object) tag);
        Bundle b = new Bundle();
        b.putString("fixtureId", fixture);
        if (newTab.fragment == null) {
            newTab.fragment = Fragment.instantiate(this,
                    newTab.clss.getName(), newTab.args);
            newTab.fragment.setRetainInstance(true);

            newTab.fragment.setArguments(b);
            FragmentTransaction ft = this.getSupportFragmentManager().beginTransaction();

            ft.add(R.id.realtabcontent, newTab.fragment, newTab.tag);
            ft.commit();
        }

    }


    @Override
    public void onResume() {
        super.onResume();
        wallStateTimer.start(3000);
        uiHelper.onResume();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == 998) {
            eventManager.fire(new TwitterEnableEvent());
        } else {
            uiHelper.onActivityResult(requestCode, resultCode, data);
        }

    }

    @Override
    public void onPause() {
        super.onPause();
        hideProgessBar(); //force hide pbar
        getServiceManager().stopAllServices();
        wallStateTimer.stop();
        uiHelper.onPause();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        hideProgessBar(); //force hide pbar
        getServiceManager().stopAllServices();
        wallStateTimer.stop();
        wallStateTimer = null;

        uiHelper.onDestroy();
    }

    private void onSessionStateChange(final Session session, SessionState state, Exception exception) {
        if (state.isOpened()) {

            debug("Facebook Logged in...");
            // make request to the /me API
            Request.executeMeRequestAsync(session, new Request.GraphUserCallback() {

                // callback after Graph API response with user object
                @Override
                public void onCompleted(GraphUser user, Response response) {
                    if (user != null) {
                        HashMap<String, String> pref = new HashMap<String, String>();
                        pref.put("facebook_id", user.getId());
                        pref.put("facebook_access_token", session.getAccessToken());

                        userModel.setFacebookId(Long.parseLong(user.getId()));
                        userModel.setFacebookAccessToken(session.getAccessToken());

                        preferences.addPref(pref);

                        postToFacebookChecked = true;
                        eventManager.fire(new FanRantFacebookEnableEvent());


                    }
                }
            });
        } else if (state.isClosed()) {
            debug("Facebook Logged out...");
            postToFacebookChecked = false;
            clearFacebokSharedPref();


        }
    }

    public boolean isPostToFacebookChecked() {
        return postToFacebookChecked;
    }

    public void setPostToFacebookChecked(boolean postToFacebookChecked) {
        this.postToFacebookChecked = postToFacebookChecked;
    }

    class TabFactory implements TabHost.TabContentFactory {

        private final Context mContext;

        /**
         * @param context
         */
        public TabFactory(Context context) {
            mContext = context;
        }

        /**
         * (non-Javadoc)
         *
         * @see android.widget.TabHost.TabContentFactory#createTabContent(java.lang.String)
         */
        public View createTabContent(String tag) {
            View v = new View(mContext);
            v.setMinimumWidth(0);
            v.setMinimumHeight(0);
            return v;
        }

    }

    class TabInfo {
        private int index;
        private String tag;
        private String name;
        protected Class clss;
        private int image;
        protected Bundle args;
        protected Fragment fragment;

        TabInfo(String tag, String name, Class clazz, int image, Bundle args, int index) {
            this.tag = tag;
            this.name = name;
            this.clss = clazz;
            this.image = image;
            this.args = args;
            this.index = index;
        }

    }

    public IFanRantServiceManager getServiceManager() {
        return serviceManager;
    }


    public void switchTab(int i) {
        mTabHost.setCurrentTab(i);
    }

}

