package com.fanrant.app.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.ImageView;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.fanrant.R;
import com.fanrant.app.model.FanRantFixtureModel;
import com.fanrant.app.model.FanRantOfficialsModel;
import com.fanrant.app.model.FanRantPlayerModel;
import com.fanrant.app.model.objects.GamePlayers;
import com.fanrant.app.modules.db.FanRantDatabase;
import com.fanrant.app.modules.util.DialogType;
import com.fanrant.app.modules.util.Frants;
import com.fanrant.app.view.dialogs.FanRantDialog;
import com.fanrant.app.view.menu.FanRantMenuItem;
import com.fanrant.app.view.dialogs.adapters.FanRantPlayerAdapter;

/**
 * Created with IntelliJ IDEA.
 * User: anthonygrimmitt
 * Date: 27/04/2013
 * Time: 15:57
 * To change this template use File | Settings | File Templates.
 */

/**
 * Created with IntelliJ IDEA.
 * User: Antwork
 * Date: 07/12/2012
 * Time: 16:50
 * To change this template use File | Settings | File Templates.
 */
public class FanRantFrantChooserActivity extends FanRantActivity implements FanRantDialog.FanRantDialogListener {
    private String fixtureId;
    private FanRantPlayerAdapter adapter;
    private FanRantDialog dialog;
    private Frants currentFrant;
    private boolean firstView = true;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.frant);
        fixtureId = getIntent().getExtras().getString("fixtureId");

        setTitle("Choose Frant");

        setupFrant(R.id.frantYellow, Frants.YELLOWCARD);
        setupFrant(R.id.frant_red, Frants.REDCARD);
        setupFrant(R.id.frantgoal, Frants.GOAL);
        setupFrant(R.id.frantpenalty, Frants.PENALTY);
        setupFrant(R.id.frantdive, Frants.DIVE);
        setupFrant(R.id.frantfire, Frants.ONFIRE);
        setupFrant(R.id.frantdonkey, Frants.WHATADONKEY);
        setupFrant(R.id.frantactor, Frants.PLAYACTOR);
        setupFrant(R.id.frantchopper, Frants.CHOPPER);
        setupFrant(R.id.frantoffside, Frants.OFFSIDE);
        setupFrant(R.id.frantblind, Frants.THEREFSBLIND);


        ImageView comment = (ImageView) findViewById(R.id.frantComment);
        comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(Frants.COMMENT.ordinal());
                finish();
            }
        });

        FanRantDatabase db = new FanRantDatabase(this);

        FanRantFixtureModel model = db.getFixture(fixtureId);

        GamePlayers[] gamePlayers = new GamePlayers[3];
        gamePlayers[0] = new GamePlayers("Officials", model.getOfficials());
        gamePlayers[1] = new GamePlayers(model.getHome_team_name(), model.getHome_team_players());
        gamePlayers[2] = new GamePlayers(model.getAway_team_name(), model.getAway_team_players());

        adapter = new FanRantPlayerAdapter(this, gamePlayers) {
            @Override
            public View.OnClickListener getFirePlayerView(int groupPosition, int childPosition) {
                Object o = adapter.getChild(groupPosition, childPosition);
                Intent intent = new Intent();
                if (o instanceof FanRantOfficialsModel) {
                        intent.putExtra("officialName", ((FanRantOfficialsModel) o).getLast_name());
                    intent.putExtra("officialId", Integer.parseInt(((FanRantOfficialsModel) o).getId()));
                } else {
                    FanRantPlayerModel gamePlayer = (FanRantPlayerModel) o;
                    intent.putExtra("playerName", gamePlayer.getNumber() + " - " + gamePlayer.getLast_name());
                    intent.putExtra("playerId", Integer.parseInt(gamePlayer.getId()));
                }
                setResult(currentFrant.ordinal(), intent);

                finish();
                return super.getFirePlayerView(groupPosition, childPosition);
            }
        };


        dialog = new FanRantDialog(this, null, false, DialogType.PLAYERS) {
            @Override
            public FanRantPlayerAdapter getAdapter() {
                setAdapter(adapter);
                return adapter;
            }

        };


    }

    public void setupFrant(int res, final Frants frant) {

        ImageView imageFrant = (ImageView) findViewById(res);
        registerForContextMenu(imageFrant);
        imageFrant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentFrant = frant;
                dialog.show(getSupportFragmentManager(), "gvyhgbv");
            }
        });

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
//        super.onSaveInstanceState(outState);    //To change body of overridden methods use File | Settings | File Templates.
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.context_menu, menu);
        super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        FanRantMenuItem.MenuItemIndex menuItemIndex = FanRantMenuItem.fromOrd(item.getItemId());
        switch (menuItemIndex) {
            case ITEM_INDEX_0:
                setResult(Frants.CANCELLED.ordinal());
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, FanRantMenuItem.MenuItemIndex.ITEM_INDEX_0.ordinal(),
                FanRantMenuItem.MenuItemIndex.ITEM_INDEX_0.ordinal(),
                "Cancel").setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onDialogPositiveClick(String error) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialog) {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}