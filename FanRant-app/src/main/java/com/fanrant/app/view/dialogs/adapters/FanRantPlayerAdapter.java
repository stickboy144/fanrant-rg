package com.fanrant.app.view.dialogs.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;
import com.fanrant.R;
import com.fanrant.app.model.FanRantModel;
import com.fanrant.app.model.FanRantOfficialsModel;
import com.fanrant.app.model.objects.GamePlayers;
import com.fanrant.app.model.FanRantPlayerModel;
import org.apache.commons.lang3.StringEscapeUtils;

/**
 * Created with IntelliJ IDEA.
 * User: Antwork
 * Date: 25/11/2012
 * Time: 14:26
 * To change this template use File | Settings | File Templates.
 */
public class FanRantPlayerAdapter extends BaseExpandableListAdapter {
    private final GamePlayers[] players;
    String[][] arrChildelements;
    String[] arrGroup;
    private LayoutInflater inflater;
    private final Context context;

    public FanRantPlayerAdapter(Context context, GamePlayers[] gamePlayers) {
        this.context = context;
        this.players = gamePlayers;

    }

    public Object getChild(int groupPosition, int childPosition) {

        return players[groupPosition].getPlayers().toArray()[childPosition];
    }

    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }

    public int getChildrenCount(int groupPosition) {

        return players[groupPosition].getPlayers().toArray().length;
    }

    public Object getGroup(int groupPosition) {
        return players[groupPosition].getType();
    }

    public int getGroupCount() {
        return players.length;
    }

    public long getGroupId(int groupPosition) {
        return 0;
    }

    public boolean hasStableIds() {
        return false;
    }

    public boolean isChildSelectable(int groupPosition, int childPosition) {

        return true;
    }

    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.team_row, null);
        }

        ViewContainer viewContainer = new ViewContainer(convertView);

        viewContainer.getTeamName().setText(players[groupPosition].getType());

        viewContainer.getTeamName().setTypeface(null, Typeface.BOLD);
        return convertView;
    }

    public View getChildView(int groupPosition, int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.player_row, null);
        }

        PlayerContainer viewContainer = new PlayerContainer(convertView);


       Object o = players[groupPosition].getPlayers().toArray()[childPosition];
        if (o instanceof FanRantOfficialsModel) {
            FanRantOfficialsModel model = (FanRantOfficialsModel)o;
            viewContainer.getName().setText(StringEscapeUtils.unescapeJava(model.getFirst_name()) + " " + StringEscapeUtils.unescapeJava(model.getLast_name()));
        } else {
            FanRantPlayerModel model = (FanRantPlayerModel)o;
            viewContainer.getName().setText(model.getNumber() + " - " + StringEscapeUtils.unescapeJava(model.getFirst_name()) + " " + StringEscapeUtils.unescapeJava(model.getLast_name()));
            //referee
        }
//        convertView.setOnClickListener(getFireTeamView(groupPosition, childPosition));
        return convertView;
    }

    public View.OnClickListener getFirePlayerView(int groupPosition, int childPosition) {
        return null;  //To change body of created methods use File | Settings | File Templates.
    }

    private class ViewContainer {
        private View row;
        private TextView teamName;

        private ViewContainer(View row) {
            this.row = row;
        }

        public TextView getTeamName() {
            if (teamName == null) {
                teamName = (TextView) row.findViewById(R.id.teamrowname);
            }
            return teamName;
        }
    }


    private class PlayerContainer {
        private View row;

        private TextView name;


        private PlayerContainer(View row) {
            this.row = row;
        }

        public TextView getName() {
            if (name == null) {
                name = (TextView) row.findViewById(R.id.playername);
            }
            return name;
        }
    }
}
