package com.fanrant.app.view.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.*;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.*;
import com.fanrant.R;
import com.fanrant.app.events.*;
import com.fanrant.app.model.FanRantModel;
import com.fanrant.app.model.IFanRantUserModel;
import com.fanrant.app.modules.IPreferences;
import com.fanrant.app.modules.api.FanRantAPIParams;
import com.fanrant.app.modules.api.FanRantAsyncTask;
import com.fanrant.app.modules.api.responders.FanRantPostFrantResponse;
import com.fanrant.app.modules.twitter.ITwitterUtils;
import com.fanrant.app.modules.util.ApplicationMessages;
import com.fanrant.app.modules.util.CommentType;
import com.fanrant.app.modules.util.DialogType;
import com.fanrant.app.view.FanRantActivity;

import com.fanrant.app.view.dialogs.adapters.FanRantDivisionTeamAdapter;
import com.fanrant.app.view.dialogs.adapters.FanRantPlayerAdapter;
import com.google.inject.Inject;
import roboguice.event.EventManager;
import roboguice.event.Observes;
import roboguice.fragment.RoboDialogFragment;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: Antwork
 * Date: 13/10/2012
 * Time: 17:42
 * To change this template use File | Settings | File Templates.
 */
public class FanRantDialog extends RoboDialogFragment implements TextWatcher, LocationListener {

    @Inject
    ContentResolver resolver;

    @Inject
    EventManager eventManager;

    @Inject
    ITwitterUtils twitterUtils;

    @Inject
    IFanRantUserModel userModel;


    @Inject
    IPreferences preferences;

    private FanRantActivity activity;
    private String mMessage;
    private boolean mCancelable;

    private DialogType dialogType;
    private TextView tweetCount;
    private EditText tweetText;
    private LocationManager locationManager;
    private Location currentLocation;
    private TextView address;
    private ListView listView;

    private FanRantDivisionTeamAdapter teamAdapter;
    private TextView progessTextView;

    private static final List<String> PERMISSIONS = Arrays.asList("publish_actions");
    private static final String PENDING_PUBLISH_KEY = "pendingPublishReauthorization";
    private boolean pendingPublishReauthorization = false;
    private ImageView tw;

    public FanRantDivisionTeamAdapter getTeamAdapter() {
        return teamAdapter;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);
    }

    public void setTeamAdapter(FanRantDivisionTeamAdapter teamAdapter) {
        this.teamAdapter = teamAdapter;
    }

    public void setAdapter(FanRantPlayerAdapter adapter) {
        this.adapter = adapter;
    }

    private FanRantPlayerAdapter adapter;

    private ExpandableListView expandableListView;


    public FanRantPlayerAdapter getAdapter() {
        return adapter;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        updateTwitterCount();
    }

    private void updateTwitterCount() {
        int countTotal = tweetText.getText().length();
        String txt = String.valueOf(150 - countTotal);
        tweetCount.setText(txt);
    }


    @Override
    public void afterTextChanged(Editable s) {
        //To change body of implemented methods use File | Settings | File Templates.
    }


    public interface FanRantDialogListener {
        public void onDialogPositiveClick(String error);

        public void onDialogNegativeClick(DialogFragment dialog);
    }

    static FanRantDialogListener mDialogListener;


    public FanRantDialog(FanRantActivity activity, String mMessage, boolean mCancelable, DialogType dialogType) {
        this.activity = activity;
        this.mMessage = mMessage;
        this.mCancelable = mCancelable;
        this.dialogType = dialogType;
    }

    public FanRantDialog() {
    }


    public static FanRantDialog newInstance(FanRantActivity activity, String message, boolean cancelable, DialogType dialogType) {
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events with it
            mDialogListener = (FanRantDialogListener) activity;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.getLocalClassName()     + " must implement FanRantDialogListener");
        }
        FanRantDialog frag = new FanRantDialog();
        frag.mMessage = message;
        frag.dialogType = dialogType;
        frag.mCancelable = cancelable;
        frag.activity = activity;

        return frag;
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        switch (dialogType) {

            case ERROR:
                builder.setCancelable(mCancelable);
                builder.setMessage(mMessage)
                        .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                mDialogListener.onDialogPositiveClick(mMessage);
                            }
                        });
                break;
            case WARN:
                builder.setCancelable(mCancelable);
                builder.setMessage(mMessage)
                        .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dismiss();
                            }
                        });

            case COMMENTTYPE:
                View view = getActivity().getLayoutInflater().inflate(R.layout.commenttypeselector, null);
                Button clubButton = (Button) view.findViewById(R.id.clubbutton);
                clubButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        eventManager.fire(new FanRantCommentViewEvent(CommentType.CLUB));
                        dismiss();
                    }
                });



                Button divisonButton = (Button) view.findViewById(R.id.divisionbutton);
                divisonButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        eventManager.fire(new FanRantCommentViewEvent(CommentType.DIVISON));
                        dismiss();
                    }
                });


                builder.setView(view);
                builder.setTitle(R.string.postto);

                break;
            case PLAYERS:
                view = getActivity().getLayoutInflater().inflate(R.layout.playerpicker, null);
                expandableListView = (ExpandableListView) view.findViewById(R.id.playersList);
                expandableListView.setAdapter(getAdapter());
                expandableListView.expandGroup(0);
                expandableListView.expandGroup(1);

                expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
                    @Override
                    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                        getAdapter().getFirePlayerView(groupPosition, childPosition);
                        return false;
                    }
                });


                builder.setView(view);
                builder.setTitle(R.string.pickplayer);
                break;
            case TEAMS:
                view = getActivity().getLayoutInflater().inflate(R.layout.containerteampicker, null);

                builder.setView(view);
                builder.setTitle(R.string.pickteam);


                break;
            case SHARE:
                view = getActivity().getLayoutInflater().inflate(R.layout.sharedialog, null);

                ImageView fb = (ImageView) view.findViewById(R.id.sharefb);


                fb.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        publishStory(mMessage);
                        dismiss();

                    }
                });

                tw = (ImageView) view.findViewById(R.id.sharetw);
                if (twitterUtils.isTwitterPrefsSet()) {
                    twitterUtils.authenticate(twitterAuthHandler);
                    tw.setImageResource(R.drawable.twitter_on);
                    tw.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            DialogFragment dialogFragment = new FanRantDialog().newInstance(activity, mMessage, false, DialogType.TWEET);
                            dialogFragment.show(activity.getSupportFragmentManager(), "Error Message");
                            dismiss();
                        }
                    });

                } else {
                    tw.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            Intent intent = new Intent();
                            intent.setClassName("com.fanrant", "com.fanrant.app.view.FanRantTwitterLoginActivity");
                            activity.startActivityForResult(intent, ApplicationMessages.TWITTERAUTH);

                            activity.overridePendingTransition(R.anim.slidein, 0);
                        }
                    });
                }


                Button cancel = (Button) view.findViewById(R.id.cancel);
                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dismiss();
                    }
                });
                builder.setView(view);

                break;

            case TWEET:
                view = getActivity().getLayoutInflater().inflate(R.layout.tweetdialog, null);
                Button cancel2 = (Button) view.findViewById(R.id.cancel);
                cancel2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dismiss();
                    }
                });
                tweetCount = (TextView) view.findViewById(R.id.tweetcount);

                address = (TextView) view.findViewById(R.id.address);
                ImageView location = (ImageView) view.findViewById(R.id.location);
                tweetText = (EditText) view.findViewById(R.id.tweetText);
                tweetText.setText(mMessage);
                tweetText.addTextChangedListener(this);
                updateTwitterCount();
                Button tweet = (Button) view.findViewById(R.id.tweetbutton);
                tweet.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            Handler handler = new Handler(){
                                @Override
                                public void handleMessage(Message msg) {
                                    if(msg.what == ApplicationMessages.TWEETED) {
                                        eventManager.fire(new FanRantCommentPostedEvent());
                                    }
                                    super.handleMessage(msg);    //To change body of overridden methods use File | Settings | File Templates.
                                }
                            };
                            twitterUtils.sendTweet(tweetText.getText().toString(), handler);
                        } catch (Exception e) {
                            e.printStackTrace();
                            eventManager.fire(new FanRantErrorEvent(e));
                        }
                        dismiss();

                    }
                });


                builder.setView(view);
                break;

            case WARNING:
                break;
            case PROGRESS:
//                builder = new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.AlertDialogCustom));
//                view = getActivity().getLayoutInflater().inflate(R.layout.progress, null);
                Dialog dialog = new Dialog(getActivity(), R.style.AntDialog);
//                ContextThemeWrapper ctw = new ContextThemeWrapper(getActivity(), R.style.AntDialog);
//                builder= new AlertDialog.Builder( ctw );
//                LayoutInflater inflater = (LayoutInflater) ctw.getSystemService(getActivity().LAYOUT_INFLATER_SERVICE);
                dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL, WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL);
                view = getActivity().getLayoutInflater().inflate(R.layout.progress, null);

                dialog.setContentView(view);
                return dialog;
            case REGNOW:
                builder.setCancelable(mCancelable);
                builder.setMessage(mMessage)
                        .setPositiveButton("Register", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent intent2 = new Intent();
                                intent2.setClassName("com.fanrant", "com.fanrant.app.view.FanRantRegisterActivity");
                                activity.startActivity(intent2);
                                activity.overridePendingTransition(R.anim.slidein, 0);

                            }
                        });
                break;

        }

        // Create the AlertDialog object and return it
        return builder.create();
    }


    public ExpandableListView.OnChildClickListener clickListner() {
        return null;  //To change body of created methods use File | Settings | File Templates.
    }

    public void expandGroup(int index) {
        expandableListView.expandGroup(index);
    }

    public void expandAndShow(FragmentManager manager, String tag) {

    }

    @Override
    public void onLocationChanged(Location loc) {
    }

    @Override
    public void onProviderDisabled(String provider) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onProviderEnabled(String provider) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onStatusChanged(String provider, int status,
                                Bundle extras) {
        // TODO Auto-generated method stub
    }

    private void fireTeamSelected(int id) {

    }

    private boolean isSubsetOf(Collection<String> subset, Collection<String> superset) {
        for (String string : subset) {
            if (!superset.contains(string)) {
                return false;
            }
        }
        return true;
    }
    private void publishStory(String msg) {
        Map<String, String> postData = new HashMap<String, String>();
        postData.put("method", "postSocialComment");
        postData.put("at", userModel.getFacebookAccessToken());
        postData.put("fid", String.valueOf(userModel.getFacebookId()));
        postData.put("comment", msg);

        Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                if (msg.what == ApplicationMessages.ASYNCTASKSUCESS.ordinal()) {
                    eventManager.fire(new FanRantFacebookPostedEvent());
                } else if (msg.what == ApplicationMessages.ASYNCTASKFAILED.ordinal()) {
                    eventManager.fire(new FanRantErrorEvent((Exception) msg.obj));
                }
                super.handleMessage(msg);
            }
        };

        FanRantAPIParams params = new FanRantAPIParams(FanRantAPIParams.ApiService.POST, FanRantPostFrantResponse.class, postData, preferences); //TODO: inject prefs
        new FanRantAsyncTask<FanRantModel>(activity, params, handler).execute();


    }

 class LocationServiceUnavailableException extends Exception {

        public LocationServiceUnavailableException() {
            super("No Location Service");
        }

        public LocationServiceUnavailableException(String s) {

        }
    }



    Handler twitterAuthHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (msg.what == ApplicationMessages.TWITTERAUTH) {
                eventManager.fire(new TwitterEnableEvent());
//                initSocialMedia();
            }
        }
    };


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);    //To change body of overridden methods use File | Settings | File Templates.
    }

    public void twitterEnabled(@Observes TwitterEnableEvent twitterEnableEvent) {

        if (twitterUtils.isTwitterPrefsSet()) {
            twitterUtils.authenticate(twitterAuthHandler);
            tw.setImageResource(R.drawable.twitter_on);
            tw.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DialogFragment dialogFragment = new FanRantDialog().newInstance(activity, mMessage, false, DialogType.TWEET);
                    dialogFragment.show(activity.getSupportFragmentManager(), "Error Message");
                    dismiss();
                }
            });


        }


    }
}

