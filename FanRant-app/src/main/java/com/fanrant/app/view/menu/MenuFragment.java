package com.fanrant.app.view.menu;

import android.os.Bundle;
import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.fanrant.app.view.menu.FanRantMenuItem;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Antwork
 * Date: 18/11/2012
 * Time: 21:54
 * To change this template use File | Settings | File Templates.
 */

public class MenuFragment extends SherlockFragment {

    private List<FanRantMenuItem> options;

    public MenuFragment(List<FanRantMenuItem> options) {
        this.options = options;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        for (FanRantMenuItem menuItem : options) {
            MenuItem item = menu.add(0, menuItem.getId().ordinal(), menuItem.getId().ordinal(), menuItem.getName());
            item.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
            if(menuItem.getDrawable() != 0) {
                item.setIcon(menuItem.getDrawable());
            }
        }
    }

}

