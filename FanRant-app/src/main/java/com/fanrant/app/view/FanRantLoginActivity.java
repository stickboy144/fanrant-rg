package com.fanrant.app.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.fanrant.R;
import com.fanrant.app.controller.UserController;
import com.fanrant.app.events.LoggedInEvent;
import com.fanrant.app.model.FanRantUserModel;
import com.fanrant.app.model.IFanRantUserModel;
import com.fanrant.app.modules.IPreferences;
import com.fanrant.app.view.dialogs.FanRantDialog;
import com.google.inject.Inject;
import roboguice.event.EventManager;
import roboguice.event.Observes;
import roboguice.inject.ContentView;
import roboguice.inject.InjectExtra;
import roboguice.inject.InjectView;

//import com.fanrant.old.FanRantApplication;

/**
 * Created with IntelliJ IDEA.
 * User: Antwork
 * Date: 10/10/2012
 * Time: 21:37
 * To change this template use File | Settings | File Templates.
 */
@ContentView(R.layout.login)
public class FanRantLoginActivity extends FanRantActivity implements FanRantDialog.FanRantDialogListener {

    @InjectExtra(value = "error", optional = true)
    String error = null;

    @Inject
    IPreferences preferences;
    @Inject
    IFanRantUserModel userModel;

    @Inject
    UserController userController;

    @Inject
    EventManager eventManager;

    @InjectView(R.id.errorText) TextView errorText;
    @InjectView(R.id.login) ImageView loginBtn;
    @InjectView(R.id.register) ImageView registerBtn;
    @InjectView(R.id.username) EditText username;
    @InjectView(R.id.password) EditText password;


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(error != null) {
            errorText.setText(error);
        }

        String usernamepref = preferences.getPref("username");
        String pass = preferences.getPref("password");
        if (usernamepref != null || pass != null) {
            username.setText(usernamepref);
            password.setText(pass);
        }

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (username.getText().length() < 1 || password.getText().length() < 1) {
                    showFanRantDialog(R.string.enterlogindetails, false);
                } else {
                    userController.login(username.getText().toString(), password.getText().toString());
                }
            }
        });


        registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                debug("new user going to register");
                Intent intent = new Intent();
                intent.setClassName("com.fanrant", "com.fanrant.app.view.FanRantRegisterActivity");

                startActivity(intent);
                overridePendingTransition(R.anim.slidein, 0);


            }
        });

    }

    /**
     * called by UserController.startLoginMode();
     * @param loginEvent
     */
    protected void initUserMode(@Observes LoggedInEvent loginEvent) {
        userModel.populate((FanRantUserModel) loginEvent.getModel());
        userModel.setLoggedIn(true);
        userModel.setRegistered(true);
        userModel.setPassword(password.getText().toString());
        userController.saveUserModelAsPref(userModel);


        Intent intent = new Intent();

        intent.setClassName("com.fanrant", FanRantMainActivity.class.getName());
        startActivity(intent);
        overridePendingTransition(R.anim.slidein, 0);
        finish();

    }

    @Override
    public void onDialogPositiveClick(String dialog) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void onDialogNegativeClick(android.support.v4.app.DialogFragment dialog) {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}