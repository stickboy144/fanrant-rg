package com.fanrant.app.view;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.util.Log;
import com.fanrant.app.modules.IPreferences;
import com.fanrant.app.modules.twitter.TwitterConstants;
import com.fanrant.app.modules.twitter.TwitterUtils;
import com.fanrant.app.modules.util.ApplicationMessages;
import com.google.inject.Inject;
import oauth.signpost.OAuth;
import oauth.signpost.OAuthConsumer;
import oauth.signpost.OAuthProvider;
import oauth.signpost.commonshttp.CommonsHttpOAuthConsumer;
import oauth.signpost.commonshttp.CommonsHttpOAuthProvider;

import java.util.HashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * Created with IntelliJ IDEA.
     * User: Antwork
 * Date: 21/02/2013
 * Time: 20:19
 * To change this template use File | Settings | File Templates.
 */
public class FanRantTwitterLoginActivity extends FanRantActivity {

    @Inject
    IPreferences Preferences;

    final String TAG = getClass().getName();

    private OAuthConsumer consumer;
    private OAuthProvider provider;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            this.consumer = new CommonsHttpOAuthConsumer(TwitterConstants.CONSUMER_KEY, TwitterConstants.CONSUMER_SECRET);
            this.provider = new CommonsHttpOAuthProvider(TwitterConstants.REQUEST_URL, TwitterConstants.ACCESS_URL, TwitterConstants.AUTHORIZE_URL);
        } catch (Exception e) {
            Log.e(TAG, "Error creating consumer / provider", e);
        }

        Log.i(TAG, "Starting task to retrieve request token.");

//        final TwitterUtils.OAuthRequestTokenTask requestTokenTask = new TwitterUtils.OAuthRequestTokenTask(this, consumer, provider);

//        Thread thread = new Thread(new Runnable() {
//            @Override
//            public void run() {
//                try {
////                    requestTokenTask.get(30, TimeUnit.SECONDS);
//                } catch (InterruptedException e) {
//                    setResult(ApplicationMessages.TWITTERTIMEOUT);
//                } catch (ExecutionException e) {
//                    setResult(ApplicationMessages.TWITTERAUTHFAILED);
//                } catch (TimeoutException e) {
//                    setResult(ApplicationMessages.TWITTERAUTHFAILED);
//                } finally {
//                    finish();
//                }
//            }
//        });
//        thread.start();

    }

    /**
     * Called when the OAuthRequestTokenTask finishes (user has authorized the request token).
     * The callback URL will be intercepted here.
     */
    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        final Uri uri = intent.getData();
        if (uri != null && uri.getScheme().equals(TwitterConstants.OAUTH_CALLBACK_SCHEME)) {
            Log.i(TAG, "Callback received : " + uri);
            Log.i(TAG, "Retrieving Access Token");
            Handler handler = new Handler() {
                @Override
                public void handleMessage(Message msg) {
                    if(msg.what == ApplicationMessages.ASYNCTASKSUCESS.ordinal()) {
                        finish();
                    }
                    finish();
                }
            };
            new RetrieveAccessTokenTask(this, consumer, provider, prefs,handler).execute(uri);
            setResult(ApplicationMessages.TWITTERAUTH);
            finish();
        }
    }


    public class RetrieveAccessTokenTask extends AsyncTask<Uri, Void, Void> {

        private Context context;
        private OAuthProvider provider;
        private OAuthConsumer consumer;
        private SharedPreferences prefs;
        private Handler handler;

        public RetrieveAccessTokenTask(Context context, OAuthConsumer consumer, OAuthProvider provider, SharedPreferences prefs, Handler handler) {
            this.context = context;
            this.consumer = consumer;
            this.provider = provider;
            this.prefs = prefs;
            this.handler = handler;
        }


        /**
         * Retrieve the oauth_verifier, and store the oauth and oauth_token_secret
         * for future API calls.
         */
        @Override
        protected Void doInBackground(Uri... params) {
            final Uri uri = params[0];
            final String oauth_verifier = uri.getQueryParameter(OAuth.OAUTH_VERIFIER);

            try {
                provider.retrieveAccessToken(consumer, oauth_verifier);

                HashMap<String, String> map = new HashMap<String, String>();
                map.put(OAuth.OAUTH_TOKEN, consumer.getToken());
                map.put(OAuth.OAUTH_TOKEN_SECRET, consumer.getTokenSecret());


                map.put("twitterEnabled", "true");
                preferences.addPref(map);

                String token = preferences.getPref(OAuth.OAUTH_TOKEN);
                String secret = preferences.getPref(OAuth.OAUTH_TOKEN_SECRET);

                consumer.setTokenWithSecret(token, secret);


                Log.i(TAG, "OAuth - Access Token Retrieved");

                handler.sendEmptyMessage(ApplicationMessages.ASYNCTASKSUCESS.ordinal());


            } catch (Exception e) {
                Log.e(TAG, "OAuth - Access Token Retrieval Error", e);
            }

            return null;
        }


    }
}

