package com.fanrant.app.view.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.fanrant.R;
import com.fanrant.app.view.dialogs.FanRantDialog;

/**
 * Created with IntelliJ IDEA.
 * User: Antwork
 * Date: 18/11/2012
 * Time: 16:10
 * To change this template use File | Settings | File Templates.
 */
public class FanRantCommentTypeSelector extends FanRantDialog {

    private Activity activity;

    public FanRantCommentTypeSelector() {
//        super();
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        return super.onCreateDialog(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.commenttypeselector,container);

        return view;
    }
}
