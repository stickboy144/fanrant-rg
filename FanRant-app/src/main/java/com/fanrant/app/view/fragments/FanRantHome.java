package com.fanrant.app.view.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.ResultReceiver;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.LoaderManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.fanrant.R;
import com.fanrant.app.controller.PostController;
import com.fanrant.app.events.*;
import com.fanrant.app.model.FanRantDataModel;
import com.fanrant.app.model.FanRantWallStatusModel;
import com.fanrant.app.model.IFanRantUserModel;
import com.fanrant.app.modules.db.FanRantDatabase;
import com.fanrant.app.modules.services.FanRantHomeWallService;
import com.fanrant.app.modules.services.FanRantService;
import com.fanrant.app.modules.twitter.ITwitterUtils;
import com.fanrant.app.modules.util.*;
import com.fanrant.app.view.FanRantMainActivity;
import com.fanrant.app.view.dialogs.FanRantCommentTypeSelector;
import com.fanrant.app.view.fragments.adapters.FanRantDataAdapter;
import com.fanrant.app.view.menu.FanRantMenuItem;
import com.github.rtyley.android.sherlock.roboguice.fragment.RoboSherlockFragment;
import com.google.inject.Inject;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import roboguice.RoboGuice;

import java.util.*;

import com.facebook.widget.LoginButton;
import roboguice.event.EventManager;
import roboguice.event.Observes;
import roboguice.inject.InjectView;

/**
 * Created with IntelliJ IDEA.
 * User: Antwork
 * Date: 30/10/2012
 * Time: 19:00
 * To change this template use File | Settings | File Templates.
 */
public class FanRantHome extends RoboSherlockFragment implements TextWatcher {

    public static final int LOADER_ID = 1;

    @Inject
    ITwitterUtils twitterUtils;

    @Inject
    IFanRantUserModel userModel;

    @Inject
    EventManager eventManager;

    @Inject
    PostController postController;

    @InjectView(R.id.posttofb)
    LoginButton postToFacebook;
    @InjectView(R.id.posttotw)
    ImageView postToTwitter;
    @InjectView(R.id.commentFlipper)
    ViewFlipper vFlip;
    @InjectView(R.id.commentBody)
    EditText commentBody;
    @InjectView(R.id.counter)
    TextView commentCounter;
    @InjectView(R.id.latest)
    PullToRefreshListView listView;


    private LoaderManager.LoaderCallbacks<Cursor> mCallbacks;

    private FanRantMainActivity context;
    private FanRantDataAdapter dataAdapter;


    private CustomNamedHandler dataLoadingHandler = new CustomNamedHandler(Handlers.DATALOADING) {
        @Override
        public void handleMessage(Message msg) {
            if (msg.arg1 > 0) {
                initHomeWall();
            }
            context.hideProgessBar();
            listView.onRefreshComplete();
        }
    };

    private void initHomeWall() {

        switch (context.getWallState()) {

            case LIVEGAME:

                context.invalidateOptionsMenu();
                break;
            case COUNTRYGAME:
                break;
            case DIVISIONGAME:
                break;
            case NOGAMES:
                break;
        }

        FanRantDatabase db = new FanRantDatabase(context);
        dataAdapter = new FanRantDataAdapter(context, R.id.latest, db.getWall(),context.getLayoutInflater());
        listView.setAdapter(dataAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                showFeedItemBrowser(position);
            }
        });

        listView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<ListView>() {
            @Override
            public void onRefresh(PullToRefreshBase<ListView> refreshView) {
                context.getServiceManager().restartService(FanRantService.HOMEWALL);

            }
        });

    }


    private LoaderManager loaderManager;
    private Intent homeWallDataService;


    private CommentType curentCommentType = CommentType.CLUB; // default
    private boolean commentView = false;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        /**
         * setup roboguice for this fragment
         */
        RoboGuice.getInjector(context).injectMembers(this);

        /**
         * Start the data service to automatically update the feed
         *
         */

        WallDataReceiver wallDataReceiver = new WallDataReceiver(dataLoadingHandler);

        context.getServiceManager().addService(FanRantService.HOMEWALL, FanRantHomeWallService.class, wallDataReceiver)
                .startService(FanRantService.HOMEWALL);
        /**
         *  sets window title
         */
        context.setTitle("Latest comments");

        setHasOptionsMenu(true); //allow ABS menu control
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (container == null) {
            return null;
        }

        LinearLayout view = (LinearLayout) inflater.inflate(R.layout.hometab, container, false);

        return view;

    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        /**
         * viewflipper used to container both latest view and comment view
         */
        vFlip.setInAnimation(context, R.anim.in_from_left);
        vFlip.setOutAnimation(context, R.anim.out_to_right);

        /**
         * setup comment ui component
         */
        commentBody.addTextChangedListener(this);

        ImageView commentBtn = (ImageView) view.findViewById(R.id.postcomment);
        commentBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                postController.performPost(curentCommentType, context.isPostToFacebookChecked(), commentBody.getText().toString());
            }
        });


        initSocialMedia();

        initHomeWall();


        postToTwitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (twitterUtils.isTwitterPrefsSet()) {
                    twitterUtils.authenticate(twitterAuthHandler);
                } else {
                    Intent intent = new Intent();
                    intent.setClassName("com.fanrant", "com.fanrant.app.view.FanRantTwitterLoginActivity");
                    startActivityForResult(intent,ApplicationMessages.TWITTERAUTH);

                    context.overridePendingTransition(R.anim.slidein, 0);
                }
            }
        });

    }

    private void showFeedItemBrowser(int position) {
        FanRantDataModel model = dataAdapter.getData()[position - 1];

        if (model.getType() != FeedType.COMMENTS) {
            Intent intent = new Intent();
            intent.setClassName("com.fanrant", "com.fanrant.app.view.FanRantBrowserActivity");
            intent.putExtra("url", model.getLink());
            context.startActivity(intent);
            context.overridePendingTransition(R.anim.slidein, 0);
        }
    }

    private void initSocialMedia() {
        if (context.isPostToFacebookChecked()) {
            postToFacebook.setBackgroundResource(R.drawable.fb_on);
            postController.setPostToFacebook(true);
        }
       postToFacebook.setPublishPermissions(Arrays.asList("publish_stream"));
        if (postController.isPostToTwitter()) {
            postToTwitter.setBackgroundResource(R.drawable.twitter_on);
            postController.setPostToTwitter(true);
        }
    }

    /**
     * displaying warning that posting is in progress
     * fire from PostController
     *
     * @param postInProgressEvent
     */
    public void postInProgess(@Observes FanRantPostInProgressEvent postInProgressEvent) {
        context.showFanRantDialog(R.id.postinprogress, false);
    }


    /**
     * fired from PostController
     *
     * @param commentMadeEvent
     */
    public void finishComment(@Observes FanRantCommentPostedEvent commentMadeEvent) {
        vFlip.showPrevious();
        commentView = false;
        context.setTitle(0);
        context.invalidateOptionsMenu();
    }

    /**
     * Fire from FanRantCommentTypeSelector
     *
     at com.fanrant.app.view.fragments.FanRantHome.finishComment(FanRantHome.j
     * @param commentViewEvent
     */
    public void launchCommentView(@Observes FanRantCommentViewEvent commentViewEvent) {
        curentCommentType = commentViewEvent.getType();
        commentView = true;
        context.invalidateOptionsMenu();
        vFlip.showNext();
    }

    public void facebookEnabled(@Observes FanRantFacebookEnableEvent facebookEnableEvent) {
        initSocialMedia();
    }

    public void twitterEnabled(@Observes TwitterEnableEvent twitterEnableEvent) {
        postController.setPostToTwitter(true);
        initSocialMedia();
    }


    /**
     * this receivers notifications from the FanRantHomeWallService that new data has been added to wall
     */

    class WallDataReceiver extends ResultReceiver {

        private Handler handler;

        public WallDataReceiver(Handler handler) {
            super(handler);
            this.handler = handler;
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            switch (resultCode) {
                case -1:
                    eventManager.fire(new FanRantErrorEvent(new Exception(resultData.getString("error"))));
                    break;
                case 1:
                    Message message = new Message();
                    message.arg1 = 1;
                    handler.sendMessage(message);
                    break;

        }
            super.onReceiveResult(resultCode, resultData);
        }
    }

    /**
     * Menu
     */

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        FanRantMenuItem.MenuItemIndex menuItemIndex = FanRantMenuItem.fromOrd(item.getItemId());
        switch (menuItemIndex) {
            case ITEM_INDEX_0:
           commentView = false;
                context.invalidateOptionsMenu();
                eventManager.fire(new FanRantCommentPostedEvent());
                break;
            case ITEM_INDEX_1:
                if(commentBody.getText().toString().equals(""))  {
                    context.showFanRantDialog(R.string.emptycomment,false);

                } else {
                     postController.performPost(curentCommentType, context.isPostToFacebookChecked(), commentBody.getText().toString());
                }
                break;
            case ITEM_INDEX_2:
                DialogFragment dialogFragment = new FanRantCommentTypeSelector().newInstance(context, null, false, DialogType.COMMENTTYPE);
                dialogFragment.show(context.getSupportFragmentManager(), null);
                break;
            case ITEM_INDEX_3:
                context.setTabArguments("Tab4", String.valueOf(context.getLiveFixture()));
                context.switchTab(3);
                break;

        }

        InputMethodManager imm = (InputMethodManager) context.getSystemService(
                Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(commentBody.getWindowToken(), 0);

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        context.setTitle("Latest comments");
        if (commentView) {
            menu.clear();
            List<FanRantMenuItem> menuItems = new ArrayList<FanRantMenuItem>();
            menuItems.add(new FanRantMenuItem("Cancel", FanRantMenuItem.MenuItemIndex.ITEM_INDEX_0));
            menuItems.add(new FanRantMenuItem("Post", FanRantMenuItem.MenuItemIndex.ITEM_INDEX_1));
            for (FanRantMenuItem menuItem : menuItems) {
                menu.add(0, menuItem.getId().ordinal(), menuItem.getId().ordinal(), menuItem.getName()).setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
            }
        } else {
            menu.clear();
            FanRantMenuItem menuItem = new FanRantMenuItem("comment", FanRantMenuItem.MenuItemIndex.ITEM_INDEX_2);
            menu.add(0, menuItem.getId().ordinal(), menuItem.getId().ordinal(), menuItem.getName())
                    .setIcon(R.drawable.comment)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

            if(context.getWallState() == FanRantWallStatusModel.WallState.LIVEGAME) {
                FanRantMenuItem menuItem2 = new FanRantMenuItem("live game", FanRantMenuItem.MenuItemIndex.ITEM_INDEX_3);
                menu.add(0, menuItem2.getId().ordinal(), menuItem2.getId().ordinal(), menuItem2.getName())
                        .setIcon(R.drawable.ball)
                        .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

            }

        }

    }

    /**
     * text limiting functions
     */

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        int countTotal = commentBody.getText().length();
        String txt = (140 - countTotal) + " Characters Left";
        commentCounter.setText(txt);
    }

    @Override
    public void afterTextChanged(Editable s) {
    }

    /**
     * Boilerplate methods
     *
     * @param activity
     */

    @Override
    public void onAttach(Activity activity) {
        context = (FanRantMainActivity) activity;
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        context.getServiceManager().stopService(FanRantService.HOMEWALL);
        super.onDetach();    //To change body of overridden methods use File | Settings | File Templates.
    }

    @Override
    public void onStop() {
        context.getServiceManager().stopService(FanRantService.HOMEWALL);
        super.onStop();
    }

    @Override
    public void onPause() {
        context.getServiceManager().stopService(FanRantService.HOMEWALL);
        super.onPause();
    }

    @Override
    public void onResume() {
        if(!context.getSupportActionBar().isShowing()) {
            context.getSupportActionBar().show();
        }
        commentView  = false;
        vFlip.setDisplayedChild(0);

        twitterUtils.authenticate(twitterAuthHandler);
        context.getServiceManager().startService(FanRantService.HOMEWALL);
        super.onResume();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);    //To change body of overridden methods use File | Settings | File Templates.
    }

    Handler twitterAuthHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if(msg.what == ApplicationMessages.TWITTERAUTH) {
                eventManager.fire(new TwitterEnableEvent());
                initSocialMedia();
            }
        }
    };


    public void onError(@Observes FanRantErrorEvent errorEvent) {
        context.showFanRantDialog(R.string.erroroccured,errorEvent.getException().getMessage(),false);
    }
}