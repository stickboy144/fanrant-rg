package com.fanrant.app.view.dialogs.adapters;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.content.Loader;
import android.widget.SimpleCursorTreeAdapter;
import com.fanrant.app.view.FanRantActivity;
import com.fanrant.app.view.dialogs.FanRantTeamsListDialog;

import java.util.HashMap;

//import com.fanrant.app.model.FanRantTeamsByDivisionModel;

/**
 * Created with IntelliJ IDEA.
 * User: Antwork
 * Date: 25/11/2012
 * Time: 14:26
 * To change this template use File | Settings | File Templates.
 */
public class FanRantDivisionTeamAdapter extends SimpleCursorTreeAdapter  {

    private final FanRantActivity context;
    private HashMap<Integer, Integer> mGroupMap;
    private FanRantTeamsListDialog mFragment;

    public FanRantDivisionTeamAdapter(Context context, FanRantTeamsListDialog mFragment,
                                      int groupLayout, String[] groupFrom, int[] groupTo,
                                      int childLayout, String[] childFrom, int[] childTo) {

        super(context, null, groupLayout, groupFrom, groupTo, childLayout, childFrom,
                childTo);
        this.mFragment = mFragment;
        this.context = (FanRantActivity) context;


        mGroupMap = new HashMap<Integer, Integer>();

    }
    @Override
    protected Cursor getChildrenCursor(Cursor groupCursor) {
        // Given the group, we return a cursor for all the children within that group
        int groupPos = groupCursor.getPosition();
        int groupId = groupCursor.getInt(groupCursor
                .getColumnIndex("competition_id"));

        mGroupMap.put(groupId, groupPos);

        Loader loader = context.getSupportLoaderManager().getLoader(groupId);
//        if ( loader != null && !loader.isReset() ) {
//            context.getSupportLoaderManager().restartLoader(groupId, null, mFragment);
//        } else {
//            context.getSupportLoaderManager().initLoader(groupId, null, mFragment);
//        }

        return null;
    }

    public HashMap<Integer, Integer> getmGroupMap() {
        return mGroupMap;
    }

    public void setmGroupMap(HashMap<Integer, Integer> mGroupMap) {
        this.mGroupMap = mGroupMap;
    }
}


//    @Override
//    protected Cursor getChildrenCursor(Cursor groupCursor) {
//
////        Cursor value = checkDB.rawQuery("SELECT * FROM employee WHERE _id='"+ countryID +"'", null);
//
////        String test = "";
////        if(value.moveToFirst())
////            test =  value.getInt(0) + ": " + value.getString(1);
////        while(value.moveToNext()){
////            test += ";" + value.getInt(0) + ": " + value.getString(1);
////        }
//        return null;
//    }



