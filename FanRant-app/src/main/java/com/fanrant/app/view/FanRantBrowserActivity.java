package com.fanrant.app.view;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.actionbarsherlock.view.MenuItem;
import com.fanrant.R;
import com.fanrant.app.view.dialogs.FanRantDialog;
import com.fanrant.app.view.menu.FanRantMenuItem;
import com.fanrant.app.view.menu.MenuFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: anthonygrimmitt
 * Date: 13/04/2013
 * Time: 17:20
 * To change this template use File | Settings | File Templates.
 */
public class FanRantBrowserActivity extends FanRantActivity implements FanRantDialog.FanRantDialogListener{
    private Fragment mFragment;
    private WebView webView;
    private String url;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.browser);
        url = getIntent().getExtras().getString("url");
        showProgessBar();
        setTitle("Browser");

        webView = (WebView) findViewById(R.id.browser);
        webView.loadUrl(url);
        webView.setInitialScale(72);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.setBackgroundColor(0);
        webView.setBackgroundDrawable(getResources().getDrawable(R.color.trans));

        webView.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url){
                view.loadUrl(url);
                return false;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                showProgessBar();
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                hideProgessBar();
            }


        });
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        mFragment= fm.findFragmentByTag("f1");
        if (mFragment == null) {

            List<FanRantMenuItem> menuItems = new ArrayList<FanRantMenuItem>();
            FanRantMenuItem closeMenuItem = new FanRantMenuItem("close", FanRantMenuItem.MenuItemIndex.ITEM_INDEX_0);
            menuItems.add(closeMenuItem);
            FanRantMenuItem refreshMenuItem = new FanRantMenuItem("refresh", FanRantMenuItem.MenuItemIndex.ITEM_INDEX_1);
            refreshMenuItem.setDrawable(R.drawable.ic_menu_refresh_holo_light);

            menuItems.add(refreshMenuItem);
            mFragment = new MenuFragment(menuItems);
            ft.add(mFragment, "f1");
        }
        ft.commit();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        FanRantMenuItem.MenuItemIndex menuItemIndex = FanRantMenuItem.fromOrd(item.getItemId());

        switch (menuItemIndex) {

            case ITEM_INDEX_0:
                finish();
                break;
            case ITEM_INDEX_1:
                webView.loadUrl(url);
                break;
            case ITEM_INDEX_2:
                break;
            case ITEM_INDEX_3:
                break;
            case ITEM_INDEX_4:
                break;
            case ITEM_INDEX_5:
                break;
        }
        return super.onOptionsItemSelected(item);

    }
}
