package com.fanrant.app.view.fragments.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;
import com.fanrant.R;
import com.fanrant.app.model.FanRantFixtureModel;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: Antwork
 * Date: 25/11/2012
 * Time: 14:26
 * To change this template use File | Settings | File Templates.
 */
public class FanRantLiveScoresAdapter extends BaseExpandableListAdapter {
    private final FanRantFixtureModel[] fixtureModels;
    private  Object[][] fixtureArray;
    HashMap<String,List<FanRantFixtureModel>> map;
    private LayoutInflater inflater;
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");


    private final Context context;

    public FanRantLiveScoresAdapter(Context context, FanRantFixtureModel[] fixtures) {
        this.context = context;
        this.fixtureModels = fixtures;
        map = new HashMap<String, List<FanRantFixtureModel>>();
        for (FanRantFixtureModel fixtureModel : fixtures) {
            if(map.containsKey(fixtureModel.getCompetition_name())) {
                map.get(fixtureModel.getCompetition_name()).add(fixtureModel);
            } else {
                map.put(fixtureModel.getCompetition_name(),new ArrayList<FanRantFixtureModel>());
                map.get(fixtureModel.getCompetition_name()).add(fixtureModel);
            }
        }

        Set entries = map.entrySet();
        Iterator entriesIterator = entries.iterator();

        fixtureArray = new Object[entries.size()][2];
        int i = 0;
        while(entriesIterator.hasNext()){

            Map.Entry mapping = (Map.Entry) entriesIterator.next();

            fixtureArray[i][0] = mapping.getKey();
            fixtureArray[i][1] = mapping.getValue();

            i++;
        }
    }

    public Object getChild(int groupPosition, int childPosition) {
        return ((List<FanRantFixtureModel>)fixtureArray[groupPosition][1]).get(childPosition);
    }

    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }

    public int getChildrenCount(int groupPosition) {
        if(fixtureArray.length > 0) {
          return ((List<FanRantFixtureModel>)fixtureArray[groupPosition][1]).size();
        }
        return 0;
    }

    public Object getGroup(int groupPosition) {
        return fixtureArray[groupPosition];
    }

    public int getGroupCount() {
        return fixtureArray.length;
    }

    public long getGroupId(int groupPosition) {
        return 0;
    }

    public boolean hasStableIds() {
        return false;
    }

    public boolean isChildSelectable(int groupPosition, int childPosition) {

        return true;
    }

    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.team_row, null);
        }

        ViewContainer viewContainer = new ViewContainer(convertView);

        viewContainer.getTeamName().setText(fixtureArray[groupPosition][0].toString());

        viewContainer.getTeamName().setTypeface(null, Typeface.BOLD);
        return convertView;
    }

    public View getChildView(int groupPosition, int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.game_row, null);
        }

        ScoreViewContainer viewContainer = new ScoreViewContainer(convertView);

        FanRantFixtureModel model = ((List<FanRantFixtureModel>) fixtureArray[groupPosition][1]).get(childPosition);
        viewContainer.getHomeTeam().setText(model.getHome_team_name());

        viewContainer.getAwayTeam().setText(model.getAway_team_name());

        if(model.getMatchtime() == null){
            viewContainer.getTime().setText("FT");
        } else {
            if(model.getMatchtime().equals("0min")) {
                model.setMatchtime(model.getKickoff().substring(11,16));
            }

            viewContainer.getTime().setText(model.getMatchtime());
        }

        if (model.getScoreline().equals("")) {
            viewContainer.getScore().setText("0-0");
        } else {
            viewContainer.getScore().setText(model.getScoreline());
//            viewContainer.getShowDay().setText(fixtureModels[groupPosition].getGames())
        }
        convertView.setOnClickListener(getFireGameView(groupPosition, childPosition));
        return convertView;
    }

    public View.OnClickListener getFireGameView(int groupPosition, int childPosition) {
        return null;  //To change body of created methods use File | Settings | File Templates.
    }

    private class ViewContainer {
        private View row;
        private TextView teamName;

        private ViewContainer(View row) {
            this.row = row;
        }

        public TextView getTeamName() {
            if (teamName == null) {
                teamName = (TextView) row.findViewById(R.id.teamrowname);
            }
            return teamName;
        }
    }


    private class ScoreViewContainer {
        private View row;


        private TextView time;


        private TextView homeTeam;

        private TextView score;

        public TextView getTime() {
            if (time == null) {
                time = (TextView) row.findViewById(R.id.game_time);
            }
            return time;
        }

        private TextView awayTeam;

        private ScoreViewContainer(View row) {
            this.row = row;
        }

        public TextView getHomeTeam() {
            if (homeTeam == null) {
                homeTeam = (TextView) row.findViewById(R.id.hometeam);
            }
            return homeTeam;
        }

        public TextView getScore() {
            if (score == null) {
                score = (TextView) row.findViewById(R.id.gamescore);
            }
            return score;
        }

        public TextView getAwayTeam() {
            if (awayTeam == null) {
                awayTeam = (TextView) row.findViewById(R.id.awayteam);
            }
            return awayTeam;
        }
    }
}
