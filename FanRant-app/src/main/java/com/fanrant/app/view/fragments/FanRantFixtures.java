package com.fanrant.app.view.fragments;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.fanrant.R;
import com.fanrant.app.model.IFanRantUserModel;
import com.fanrant.app.view.FanRantMainActivity;
import com.fanrant.app.view.menu.FanRantMenuItem;
import com.github.rtyley.android.sherlock.roboguice.fragment.RoboSherlockFragment;
import com.google.inject.Inject;

/**
 * Created with IntelliJ IDEA.
 * User: Antwork
 * Date: 30/10/2012
 * Time: 19:00
 * To change this template use File | Settings | File Templates.
 */
public class FanRantFixtures extends RoboSherlockFragment {

    @Inject
    IFanRantUserModel userModel;

    private FanRantMainActivity context;
    private WebView webView;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        context = (FanRantMainActivity) getActivity();
//        app = (FanRantApplication) context.getApplication();

        setHasOptionsMenu(true);

        context.setTitle("Fixtures");
//        fixUrl = app.getFanRantUser().team_fixtures_url.toString();

        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (container == null) {
            return null;
        }
        LinearLayout layout = (LinearLayout) inflater.inflate(R.layout.fixtures, container, false);


        webView = (WebView) layout.findViewById(R.id.fixtureWebview);
        if (savedInstanceState != null) {
            webView.restoreState(savedInstanceState);
        } else {
            initWebView();

        }
        return layout;
    }

    private void initWebView() {
        webView.setInitialScale(72);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.setBackgroundColor(0);
        webView.setBackgroundDrawable(getResources().getDrawable(R.color.trans));

        webView.loadUrl(userModel.getTeam_fixtures_url());
        webView.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return false;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                context.showProgessBar();
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                context.hideProgessBar();
            }


        });
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        webView.restoreState(savedInstanceState);
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        FanRantMenuItem.MenuItemIndex menuItemIndex = FanRantMenuItem.fromOrd(item.getItemId());

        switch (menuItemIndex) {

            case ITEM_INDEX_0:
                webView.loadUrl(userModel.getTeam_fixtures_url());
                break;
            case ITEM_INDEX_1:
                break;
            case ITEM_INDEX_2:
                break;
            case ITEM_INDEX_3:
                break;
            case ITEM_INDEX_4:
                break;
            case ITEM_INDEX_5:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

        webView.saveState(outState);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onResume() {
        if(!context.getSupportActionBar().isShowing()) {
            context.getSupportActionBar().show();
        }
        super.onResume();
    }

    @Override
    public void onPause() {
        System.out.println("hello");
        super.onPause();
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        context.setTitle("Fixtures");
        menu.clear();
        FanRantMenuItem menuItem = new FanRantMenuItem("refresh", FanRantMenuItem.MenuItemIndex.ITEM_INDEX_0);
        menu.add(0, menuItem.getId().ordinal(), menuItem.getId().ordinal(), menuItem.getName())
                .setIcon(R.drawable.ic_menu_refresh_holo_light)
                .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

        super.onPrepareOptionsMenu(menu);
    }
}
