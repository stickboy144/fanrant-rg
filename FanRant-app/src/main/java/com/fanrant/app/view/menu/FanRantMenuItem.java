package com.fanrant.app.view.menu;

/**
 * Created with IntelliJ IDEA.
 * User: Antwork
 * Date: 24/11/2012
 * Time: 13:11
 * To change this template use File | Settings | File Templates.
 */
public class FanRantMenuItem {
    private String name;
    private MenuItemIndex id;
    private int drawable = 0;
    public enum MenuItemIndex {
        ITEM_INDEX_0,
        ITEM_INDEX_1,
        ITEM_INDEX_2,
        ITEM_INDEX_3,
        ITEM_INDEX_4,
        ITEM_INDEX_5
    }

    public FanRantMenuItem(String name, MenuItemIndex id) {
        this.name = name;
        this.id = id;
    }

    public int getDrawable() {
        return drawable;
    }

    public void setDrawable(int drawable) {
        this.drawable = drawable;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public MenuItemIndex getId() {
        return id;
    }

    public void setId(MenuItemIndex id) {
        this.id = id;
    }

    public static MenuItemIndex fromOrd(int i) {
        if (i < 0 || i >= MenuItemIndex.values().length) {
            throw new IndexOutOfBoundsException("Invalid ordinal");
        }
        return MenuItemIndex.values()[i];
    }
}
