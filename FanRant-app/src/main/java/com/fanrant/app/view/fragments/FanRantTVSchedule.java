package com.fanrant.app.view.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import com.actionbarsherlock.view.Menu;
import com.fanrant.R;
import com.fanrant.app.controller.TVScheduleController;
import com.fanrant.app.events.TVScheduleLoadedEvent;
import com.fanrant.app.model.FanRantTVShowModel;
import com.fanrant.app.model.objects.ShowDay;
import com.fanrant.app.view.FanRantMainActivity;
import com.fanrant.app.view.fragments.adapters.FanRantTVShowAdapter;
import com.github.rtyley.android.sherlock.roboguice.fragment.RoboSherlockFragment;
import com.google.inject.Inject;
import roboguice.RoboGuice;
import roboguice.event.EventManager;
import roboguice.event.Observes;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: Antwork
 * Date: 30/10/2012
 * Time: 19:00
 * To change this template use File | Settings | File Templates.
 */
public class FanRantTVSchedule extends RoboSherlockFragment {

    @Inject
    TVScheduleController scheduleController;

    @Inject
    EventManager eventManager;

    private FanRantMainActivity context;


    private ExpandableListView tvList;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        /**
         * setup roboguice for this fragment
         */
        RoboGuice.getInjector(context).injectMembers(this);

        context.setTitle("TV Schedule");

        setHasOptionsMenu(true);
        context.showProgessBar();
        scheduleController.loadTvSchedule();
        super.onCreate(savedInstanceState);
    }


    public void loadAndSortSchedule(@Observes TVScheduleLoadedEvent scheduleLoadedEvent) {

        List<ShowDay> groupDates = new ArrayList<ShowDay>();

        Date minDate = new Date(System.currentTimeMillis());
        Calendar cal = Calendar.getInstance();
        cal.setTime(minDate);
        cal.add(Calendar.HOUR, -2);
        minDate = cal.getTime();

        for (FanRantTVShowModel show : (FanRantTVShowModel[]) scheduleLoadedEvent.getModels()) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

            try {


                Date d = sdf.parse(show.getStart());

                if (minDate.before(d)) {
                    boolean found = false;
                    for (ShowDay groupDate : groupDates) {
                        if (d.equals(groupDate.getStart())) {
                            groupDate.getShows().add(show);
                            found = true;
                        }
                    }
                    if (!found) {
                        ShowDay showDays = new ShowDay(d);
                        showDays.getShows().add(show);
                        groupDates.add(showDays);
                    }
                }
            } catch (ParseException e) {
                context.handleException(e);  //To change body of catch statement use File | Settings | File Templates.
            }


        }

        FanRantTVShowAdapter adapter = new FanRantTVShowAdapter(context, (ShowDay[]) groupDates.toArray(new ShowDay[groupDates.size()]));
        tvList.setAdapter(adapter);
        for (int i = 0; i < adapter.getGroupCount(); i++) {
            tvList.expandGroup(i);
        }

    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        context.setTitle("TV Schedule");
        menu.clear();
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (container == null) {
            return null;
        }
        LinearLayout view = (LinearLayout) inflater.inflate(R.layout.tvschedule, container, false);

        tvList = (ExpandableListView) view.findViewById(R.id.tvschedule);

        return view;
    }


    @Override
    public void onAttach(Activity activity) {
        context = (FanRantMainActivity) activity;
        super.onAttach(activity);
    }

}
