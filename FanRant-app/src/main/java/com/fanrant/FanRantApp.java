package com.fanrant;

import android.app.Application;
import com.appblade.framework.AppBlade;
import com.fanrant.app.modules.FanRantStartUpModule;
import com.novoda.imageloader.core.ImageManager;
import com.novoda.imageloader.core.LoaderSettings;
import roboguice.RoboGuice;

/**
 * Created with IntelliJ IDEA.
 * User: Antwork
 * Date: 23/03/2013
 * Time: 19:11
 * To change this template use File | Settings | File Templates.
 */
public class FanRantApp extends Application {


    private static ImageManager imageManager;

    @Override
    public void onCreate() {
        RoboGuice.setBaseApplicationInjector(this, RoboGuice.DEFAULT_STAGE,
                RoboGuice.newDefaultRoboModule(this), new FanRantStartUpModule());

        LoaderSettings settings = new LoaderSettings.SettingsBuilder()
                .withDisconnectOnEveryCall(true).build(this);
        imageManager = new ImageManager(this, settings);

    String token = "3e1b50c3193fe7e2e3ace64cc9d67aaf";
    String secret = "b06064edde17492994b4e0b2085e6a83";
    String uuid = "00f47abc-5fd7-48b4-8d5b-87c9bdc5ae0c";
    String issuance = "9999999999";

    AppBlade.register(this, token, secret, uuid, issuance);
    }
    public static final ImageManager getImageManager() {
        return imageManager;
    }
}


